# Distribution (Qunatitative)

Measure of **distribution** is used to describe the symmetry of dataset relative to normal distribution

1. **Left-Right**: Skewness
2. **Top-Down**: Kurtosis

---

## Skewness

Is a number shows the **symmetry** of dataset relative to normal distribution, in **Left-Right**:

-   **Positive**
    -   Right skewness
    -   Right Tendency
    -   Large number of **Big values** relative to **mean**
    -   Mode **<** Median **<** Mean
-   **Negative**
    -   Left skewness
    -   Left Tendency
    -   Large number of **Small values** relative to **mean**
    -   Mean **>** Median **>** Mode

![Skewness](assets/distribution_measure_distribution_skewness.png)

<!--prettier-ignore-->
!!! example "Example"
    Life time of 10 batteries have mean `3.5`, median `3.48`, standard deviation `1.65`, find skewness:

    $$
    \begin{aligned}
        & b_2 = \frac{ 3 \left( 3.5 - 3.45 \right) }{ 1.65 } = 0.036 = 3\%
        \\
        & \texttt{Right Skewness}
    \end{aligned}
    $$

---

### Pearson First Coefficient (**b1**)

$$
\begin{aligned}
    & b_1 = \frac{ \left( \overline{X} - M \right) }{S}
    \\
    \\
    & \overline{X}: \texttt{Arithmetic Mean}
    \\
    & M: \texttt{Mode}
    \\
    & S: \texttt{Standard Deviation}
\end{aligned}
$$

---

### Pearson Second Coefficient (**b2**)

$$
\begin{aligned}
    & b_2 = \frac{ 3\left( \overline{X} - m \right) }{S}
    \\
    \\
    & \overline{X}: \texttt{Arithmetic Mean}
    \\
    & m: \texttt{Median}
    \\
    & S: \texttt{Standard Deviation}
\end{aligned}
$$

---

### Moment Coefficient (**g**)

$$
\begin{aligned}
    & g = \frac{m_3}{S^3}
    \\
    \\
    & m_3: \texttt{Central Moment of Order 3}
    \\
    & S: \texttt{Standard Deviation}
\end{aligned}
$$

<!--prettier-ignore-->
!!! warning "Central Moment of Order `r`"
    Is a base formula of **variance**, measure of distance from mean

    $$
    \begin{aligned}
        & m_r = \frac{ \sum\limits_{i=1}^n \left(X_i - \overline{X}\right) ^ r }{n}
    \end{aligned}
    $$

    The $\textbf{m}_2$ equals to $\textbf{S}^2$ or **variance**

---

## Kurtosis (**k**)

Is a number shows the **symmetry** of dataset relative to normal distribution, in **Top-Down**:

-   **Positive**
    -   Top kurtosis
    -   More **Central Tendency**
-   **Negative**
    -   Down kurtosis
    -   More **Dispersion**

![Kurtosis](assets/distribution_measure_distribution_kurtosis.png)

$$
\begin{aligned}
    & k = \frac{m_4}{S^4} - 3
    \\
    \\
    & m_4: \texttt{Central Moment of Order 4}
    \\
    & S: \texttt{Standard Deviation}
\end{aligned}
$$

<!--prettier-ignore-->
!!! example "Example"
    Life time of 100 batteries have central moment order 4 `9.23`, standard deviation `1.65`, find kurtosis:

    $$
    \begin{aligned}
        & k = \frac{ 9.23 }{ 1.65 } - 3 = -1.75 = -175\%
        \\
        & \texttt{More Dispersion}
    \end{aligned}
    $$

---
