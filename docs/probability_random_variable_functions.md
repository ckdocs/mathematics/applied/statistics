# Functions

There are many important functions used in probability theory for both **Discrete** and **Continous** random variables

1. **PMF**: Discrete probability
2. **PDF**: Continous probability
3. **CDF**: Probability from 0 to x
4. **PGF**: Probability generator

<!--prettier-ignore-->
!!! danger "PMF vs PDF"
    $$
    \begin{aligned}
        & P(X = a) = p_X(a)
        \\
        & P(X = a) = f_X(a).0 = 0
    \end{aligned}
    $$

---

## Probability Mass Function (PMF) (p)

**Discrete** random variable probability = **Probability Function**

Every discrete random variable has a **PMF** for finding probability of a random variable value

Also called **probability law** or **probability distribution** or **probability function** of a random variable

Gives us the probability of a value of random variable

1. Get $\{\Omega\}$ from random variable inverse $x$
2. $PMF = P(\{\Omega\})$

$$
\begin{aligned}
    & Pr(X = x) = P_X(x) = P(X = x) = p_X(x)
    \\
    \\
    & X = x :: \{w : X(w) = x\}
\end{aligned}
$$

We can also find a probability of range using summation over discrete values:

$$
\begin{aligned}
    & P(a \leq X \leq b) = \sum\limits_{x = a}^{b} p_X(x)
\end{aligned}
$$

---

### Rules

There are many important rules for every PMF

---

#### Non Negative

Probability is non negative:

$$
\begin{aligned}
    & p_X(x) \geq 0
\end{aligned}
$$

---

#### Summation

Sum of probabilities is 1:

$$
\begin{aligned}
    & \sum\limits_{x} p_X(x) = 1
\end{aligned}
$$

---

#### Range

We can split probabilities of a range to more paritions:

$$
\begin{aligned}
    & P(a \leq X \leq b) = P(a \leq X \leq b)
    \\
    \\
    & P(a \leq X \leq b) = P(X = a) + P(a \lt X \leq b)
    \\
    \\
    & P(a \leq X \leq b) = P(a \leq X \lt b) + P(X = b)
    \\
    \\
    & P(a \leq X \leq b) = P(X = a) + P(a \lt X \lt b) + P(X = b)
\end{aligned}
$$

---

### Example

-   $p_X(3) = \frac{1}{4}$
-   $p_X(4) = \frac{1}{4}$
-   $p_X(5) = \frac{1}{2}$

![PMF Example](assets/pmf_example.png)

---

## Probability Density Function (PDF) (f)

**Continous** random variable probability = **Density Function**

Every continous random variable has a **PDF** for finding probability of a random variable value range

Also called **density function** of a random variable

Gives us the probability of a value range of random variable

1. Get $\{\Omega\}$ from random variable inverse $x$
2. $PMF = P(\{\Omega\})$

$$
\begin{aligned}
    & Pr(X = x) = P_X(x) = P(X = x) = f_X(x)
    \\
    \\
    & X = x :: \{w : X(w) = x\}
\end{aligned}
$$

We can also find a probability of range using integration over continous values:

$$
\begin{aligned}
    & P(a \leq X \leq b) = \int\limits_{a}^{b} f_X(x) dx
\end{aligned}
$$

---

### Rules

There are many important rules for every PDF

---

#### Non Negative

Density is non negative:

$$
\begin{aligned}
    & f_X(x) \geq 0
\end{aligned}
$$

---

#### Summation

Sum of densities is 1:

$$
\begin{aligned}
    & \int\limits_{-\infty}^{\infty} f_X(x) = 1
\end{aligned}
$$

---

#### Range

We can split probabilities of a range to more paritions:

<!--prettier-ignore-->
!!! danger "Tip"
    But the important tip is that, density of a **point** is always **zero**

$$
\begin{aligned}
    & P(a \leq X \leq b) = P(a \leq X \leq b)
    \\
    \\
    & P(a \leq X \leq b) = P(X = a) + P(a \lt X \leq b)
    \\
    & P(a \leq X \leq b) = P(a \lt X \leq b)
    \\
    \\
    & P(a \leq X \leq b) = P(a \leq X \lt b) + P(X = b)
    \\
    & P(a \leq X \leq b) = P(a \leq X \lt b)
    \\
    \\
    & P(a \leq X \leq b) = P(X = a) + P(a \lt X \lt b) + P(X = b)
    \\
    & P(a \leq X \leq b) = P(a \lt X \lt b)
\end{aligned}
$$

---

#### Point Density

The probability in continous distributions is the **area** under the function shape, The value of density in one point is:

$$
\begin{aligned}
    & P(a \leq X \leq a + \delta) = \int_{a}^{a + \delta} f_X(x)
    \\
    \\
    & \texttt{If delta goes smaller, integral is the area:}
    \\
    \\
    & P(a \leq X \leq a + \delta) \approx f_X(a) . \delta
\end{aligned}
$$

![PDF Point](assets/pdf_point.png)

So the $P(X = a)$ is always 0:

$$
\begin{aligned}
    & P(X = a) = P(a \leq X \leq a) \approx f_X(a) . 0 = 0
    \\
    \\
    & P(X = a) = 0
\end{aligned}
$$

---

### Example

-   $P(10 \leq X \leq 15) = \int\limits_{10}^{15} f_X(x) dx = 5.\frac{1}{30} = \frac{1}{6}$

![PDF Example](assets/pdf_example.jpg)

---

## Cumulative Distribution Function (CDF) (F)

**Summation** of random variables values probabilities from **0** to **x** for both **Discrete** and **Continous** random variables

Also called **distribution function** of a random variable

$$
\begin{aligned}
    & F_X(x) = P(X \leq x)
    \\
    \\
    & F_X(x) =
    \begin{cases}
        \sum\limits_{i=-\infty}^{x} p_X(i) & \texttt{Discrete}
        \\
        \int\limits_{i=-\infty}^{x} f_X(i) di & \texttt{Continous}
    \end{cases}
\end{aligned}
$$

---

### Rules

There are many important rules for every CDF

---

#### Range

The **CDF** function is the summation of **PMF** or **PDF** functions, and we know summation of probability functions is **1**, so it will be in range from **0** to **1**

$$
\begin{aligned}
    & 0 \leq F_X(x) \leq 1
    \\
    & x \in S_X
\end{aligned}
$$

<!--prettier-ignore-->
!!! tip "Summation from 0"
    If the **min(X)** is **0** then summation from **-infinity** to **0** is equals to **0** so we will not sum them

    $$
    \begin{aligned}
        & F_X(x) =
        \begin{cases}
            \sum\limits_{i=0}^{x} p_X(i) & \texttt{Discrete}
            \\
            \int\limits_{i=0}^{x} f_X(i) di & \texttt{Continous}
        \end{cases}
    \end{aligned}
    $$

---

#### Infinity

We know that **CDF** is a summation of **Probability Functions**, and maximum values is **1**, so when we summing **x's** of **X** random variable from **-infinity** to **+infinity** the value of **CDF** will increase from **0** to **1**

$$
\begin{aligned}
    & F(-\infty) = \sum\limits_{i=-\infty}^{-\infty} p_X(x) = 0
    \\
    & F(+\infty) = \sum\limits_{i=-\infty}^{+\infty} p_X(x) = 1
\end{aligned}
$$

---

#### Ascending

The **CDF** of a random variable is always ascending and it will start from **0** in **-infinity** and will grow to **1** in **+infinity**

![PMF To CDF](assets/pmf_to_cdf_1.png)
![PMF To CDF](assets/pmf_to_cdf_2.png)

$$
\begin{aligned}
    & x_1 \leq x_2 \in S_X
    \\
    \\
    & F_X(x_1) \leq F_X(x_2)
    \\
    \\
    & \texttt{Proof:}
    \\
    & F_X(x_2) = F_X(x_1) + \sum\limits_{i = x_1}^{x_2} p_X(x)
    \\
    & F_X(x_2) \geq F_X(x_1)
\end{aligned}
$$

---

#### Continous From Right

All ascending functions are continous from right:

$$
\begin{aligned}
    & F(a^{+}) = F(a)
\end{aligned}
$$

![Continous From Right](assets/continous.jpg)

---

#### Split

We can split the $-\infty$ to $+\infty$ range using CDF's:

$$
\begin{aligned}
    & P(-\infty \leq X \leq +\infty) = 1
    \\
    & P(-\infty \leq X \leq x_0) + P(x_0 \lt X \leq +\infty) = 1
    \\
    & P(X \leq x_0) + P(X \gt x_0) = 1
    \\
    & F_X(x_0) + P(X \gt x_0) = 1
    \\
    \\
    & P(X \gt x_0) = 1 - F_X(x_0)
\end{aligned}
$$

---

### CDF to PMF

There are many important rules based on continuation from right that we can use to make a relation between **CDF** and **PMF**:

$$
\begin{aligned}
    & P(a \lt X \leq b) = F(b) - F(a)
    \\
    \\
    & P(a \leq X \lt b) = F(b^{-}) - F(a^{-})
    \\
    \\
    & P(a \leq X \leq b) = F(b) - F(a^{-})
    \\
    \\
    & P(a \lt X \lt b) = F(b^{-}) - F(a)
\end{aligned}
$$

So we can find the probability of a point (**PMF**) using these rules

$$
\begin{aligned}
    & P(X = x_0) = P(x_0 \leq X \leq x_0) = F(x_0) - F(x_0^{-})
\end{aligned}
$$

---

### CDF to PDF

We can find the **PDF** by **derivating** from it's **CDF**:

$$
\begin{aligned}
    f_X(x) = \frac{dF_X}{dx}(x)
\end{aligned}
$$

---

### Example

We have two random variables, **X** and **Y** with distribution of **Uniform** with parameter **N** in **{1,2,...,N}**, find the **PMF** of **min(X, Y)** and **max(X, Y)**:

**min(X, Y)**:

$$
\begin{aligned}
    & P(min(X, Y) = z) = ?
    \\
    \\
    & \texttt{Find CDF of min}
    \\
    \\
    & P(min(X, Y) \leq z) = 1 - P(min(X, Y) \gt z)
    \\
    & P(min(X, Y) \leq z) = 1 - P(X \gt z, Y \gt z)
    \\
    & P(min(X, Y) \leq z) = 1 - P(X \gt z).P(Y \gt z)
    \\
    \\
    & \texttt{We know the CDF of Uniform random variables}
    \\
    \\
    & P(min(X, Y) \leq z) = 1 - \left(\frac{N - z}{N + 1}\right)^2
    \\
    \\
    & \texttt{Now we can convet CDF to PMF}
    \\
    \\
    & P(min(X, Y) = z) = P(min(X, Y) \leq z) - P(min(X, Y) \leq z - 1)
    \\
    & P(min(X, Y) = z) = (1 - \left(\frac{N - z}{N + 1}\right)^2) - (1 - \left(\frac{N - (z - 1)}{N + 1}\right)^2)
\end{aligned}
$$

**max(X, Y)**:

$$
\begin{aligned}
    & P(max(X, Y) = z) = ?
    \\
    \\
    & \texttt{Find CDF of max}
    \\
    \\
    & P(max(X, Y) \leq z) = P(X \leq z, Y \leq z)
    \\
    & P(max(X, Y) \leq z) = P(X \leq z).P(Y \leq z)
    \\
    & P(max(X, Y) \leq z) = \left(\frac{z + 1}{N + 1}\right)^2
    \\
    \\
    & \texttt{Now we can convet CDF to PMF}
    \\
    \\
    & P(max(X, Y) = z) = P(max(X, Y) \leq z) - P(max(X, Y) \leq z - 1)
    \\
    & P(max(X, Y) = z) = \left(\frac{z + 1}{N + 1}\right)^2 - \left(\frac{z}{N + 1}\right)^2
\end{aligned}
$$

---

## Probability Generating Function (PGF) (G)

**Generation** of random variables values probabilities using **n-th Derivation** for **Discrete** random variables

$$
\begin{aligned}
    & G_X(z) = \Phi_X(z) = E[z^X] = \sum\limits_{i=0}^{\infty} z^i p_X(i)
\end{aligned}
$$

Now we can find i-th derivation of **G(z)** and set **z = 0** to get **P(X = i)**

---

### Rules

There are many important rules for every PGF

---

#### Addition

We can find the **PGF** of addition of variables **X** and **Y** using multiplying **PGF** of them:

$$
\begin{aligned}
    & G_{X + Y}(z) = G_X(z) . G_Y(z)
\end{aligned}
$$

---

#### Substitution

We can find the **PGF** of substitution of variables **X** and **Y** using multiplying **PGF** of them in **z** and **1/z**:

$$
\begin{aligned}
    & G_{X - Y}(z) = G_X(z) . G_Y\left(\frac{1}{z}\right)
\end{aligned}
$$

---

#### PGF to PMF

We know that **PGF** is a series of **PMF** values, so we can get all **PMF** values from **PGF**, using **derivation** n time and set **z = 0**

$$
\begin{aligned}
    & P_X(k) = \frac{G_X^{(k)}(0)}{k!}
\end{aligned}
$$

---

### Example

We have a dice, what is the **PGF** of that:

$$
\begin{aligned}
    & p_1 = \frac{1}{6}
    \\
    & p_2 = \frac{1}{6}
    \\
    & p_3 = \frac{1}{6}
    \\
    & p_4 = \frac{1}{6}
    \\
    & p_5 = \frac{1}{6}
    \\
    & p_6 = \frac{1}{6}
    \\
    \\
    & G_X(z) = E[z^X] = \sum\limits_{i=1}^{6} z^i p_X(i)
    \\
    & G_X(z) = z p_1 + z^2 p_2 + z^3 p_3 + z^4 p_4 + z^5 p_5 + z^6 p_6
    \\
    & G_X(z) = \frac{z}{6} + \frac{z^2}{6} + \frac{z^3}{6} + \frac{z^4}{6} + \frac{z^5}{6} + \frac{z^6}{6}
    \\
    \\
    & G_X^{(1)}(z) = p_1 + 2.z p_2 + 3.z^2 p_3 + 4.z^3 p_4 + 5.z^4 p_5 + 6.z^5 p_6
    \\
    & G_X^{(2)}(z) = 2.p_2 + 6.z p_3 + 12.z^2 p_4 + 20.z^3 p_5 + 30.z^4 p_6
    \\
    & G_X^{(3)}(z) = 6.p_3 + 24.z p_4 + 60.z^2 p_5 + 120.z^3 p_6
    \\
    \\
    & G_X^{(1)}(0) = p_1 = \frac{1}{6}
    \\
    & G_X^{(2)}(0) = 2.p_2 = \frac{2}{6}
    \\
    & G_X^{(3)}(0) = 6.p_3 = \frac{6}{6}
    \\
    \\
    & \texttt{Convert PGF to PMF}
    \\
    \\
    & P_X(1) = \frac{G_X^{(1)}(0)}{1!} = \frac{\frac{1}{6}}{1} = \frac{1}{6}
    \\
    & P_X(2) = \frac{G_X^{(2)}(0)}{2!} = \frac{\frac{2}{6}}{2} = \frac{1}{6}
    \\
    & P_X(3) = \frac{G_X^{(3)}(0)}{3!} = \frac{\frac{6}{6}}{6} = \frac{1}{6}
\end{aligned}
$$

---
