# Intro

There are three main concepts that we must know before learning **probability**:

1. Trial (**Begin - End**)
2. Sample Space (S) (**Record**)
3. Event (A)

---

## Random Trial

Is any procedure that can be infinitely **repeated** and has a well-defined set of **possible** outcomes, known as the **sample space**.

<!--prettier-ignore-->
!!! danger "Important Tip"
    **Trial**: A procedure (simple or compound) action, **record** from time **Begin** to **End** until ends (**trial condition**)

    * Flipping a coin
        1. `Begin Trial`
        2. **Flip coin**
        3. `End Trial`
    * Flipping a dice
        1. `Begin Trial`
        2. **Flip dice**
        3. `End Trial`
    * Getting a lamp age
        1. `Begin Trial`
        2. **Select a lamp**
        3. `End Trial`
    * Flipping a dice for 4 time
        1. `Begin Trial`
        2. **Flip dice**
        3. **Flip dice**
        4. **Flip dice**
        5. **Flip dice**
        6. `End Trial`
    * Flipping a coin until get 2 heads
        1. `Begin Trial`
        2. **Flip coin** => Head 2 ?
        3. **Flip coin** => Head 2 ?
        4. **Flip coin** => Head 2 ?
        5. ...
        6. `End Trial`
    * Space between dart and center with round of 50cm
        1. `Begin Trial`
        2. **Throw dart**
        3. `End Trial`
    * Getting three children genders
        1. `Begin Trial`
        2. **Get child 1**
        3. **Get child 2**
        4. **Get child 3**
        5. `End Trial`

<!--prettier-ignore-->
!!! example "Example"
    1. Flipping a coin
    2. Flipping a dice
    3. Getting a lamp age
    4. Flipping a coin until get 2 heads
    5. Space between dart and center with round of 50cm

### Experiment

Trying a trial for multiple times called an experiment, that we can use it result for improving our probability (**Experimental Probability**)

**Theoritical Probability** =(Experiment)=> **Experimental Probability**

<!--prettier-ignore-->
!!! example "Example"
    We toss a coin for 20 times and record 20 **outcomes** as an **experiment**

    ![Trial Experiment](assets/trial_experiment.png)

---

## Sample Space (S)

A **set** of possible outcomes of a **random trial** called sample space or $\textbf{S}$ or $\Omega$

<!--prettier-ignore-->
!!! danger "Important Tip"
    **Sample Space**: The information saved in trial **record** from **Begin** to **End** time, also called **outcomes** of our trial

    * Flipping a coin
        1. **{H, T}**
    * Flipping a dice
        1. **{1, 2, 3, 4, 5, 6}**
    * Getting a lamp age
        1. **(0, inf)**
    * Flipping a dice for 4 time
        1. **{HHHH, HHHT, ..., TTTH, TTTT}**
    * Flipping a coin until get 2 heads
        1. **{HH, THH, TTHH, ...}**
    * Space between dart and center with round of 50cm
        1. **(0, 50)**
    * Getting three children genders
        1. **{GGG, GGB, GBG, ..., BBG, BBB}**

<!--prettier-ignore-->
!!! example "Example"
    1. Flipping a coin
        - Sample Space: **{Head, Tail}**
    2. Flipping a dice
        - Sample Space: **{1, 2, 3, 4, 5, 6}**
    3. Getting a lamp age
        - Sample Space: **(0,inf)**
    4. Flipping a coin until get 2 heads
        - Sample Space: **{2, 3, 4, ...}**
    5. Space between dart and center with round of 50cm
        - Sample Space: **(0,50)**
    6. Flipping two dices:
        - Sample Space: **{11,12,13,14,15,16,21,...,65,66}**

### Types

There are two types of sample spaces based on countability

#### Discrete (Countable)

1. Finite: `Flipping a dice`
2. Infinine: `Flipping a coin to get head`

#### Continuous

1. Finine: `Dart and center gap`
2. Infinine: `Lamp age`

---

### Visualize

We can break-down our **Trial** into simple and smaller trials

<!--prettier-ignore-->
!!! example "Example"
    We have a **dice** and a **coin**, now we flip them

    1. We flip **dice**, then flip **coin**
    2. We flip **dice** and **coin** together

    ![Sample Space Visualize](assets/sample_space_visualize.jpg)

---

#### Venn Diagram

We can visualize our sample space in **Venn Diagram** (entire set **S**)

<!--prettier-ignore-->
!!! example "Example"
    We have a **dice** and a **coin**, now we flip them

    1. We flip **dice**, then flip **coin**
    2. We flip **dice** and **coin** together

    ![Sample Space Venn Diagram](assets/sample_space_venn_diagram.jpg)

---

#### Tree Diagram

We can visualize our sample space in **Tree Diagram**

<!--prettier-ignore-->
!!! example "Example"
    We have a **dice** and a **coin**, now we flip them

    1. We flip **dice**, then flip **coin**
    2. We flip **dice** and **coin** together

    ![Sample Space Tree Diagram](assets/sample_space_tree_diagram.jpg)

---

#### Frequency Distribution

We can visualize our sample space in **Frequency Distribution**

<!--prettier-ignore-->
!!! example "Example 2D"
    We have a **dice** and a **coin**, now we flip them

    1. We flip **dice**, then flip **coin**
    2. We flip **dice** and **coin** together

    Now we have **12** possible outcomes

    ![Sample Space Frequency Distribution 1](assets/sample_space_frequency_distribution_1.jpg)

<!--prettier-ignore-->
!!! example "Example 3D"
    We have a **three childs**, now we want to know their genders

    1. We get **first child gender**, then **second child gender**, then **third child gender**
    2. We get **all children genders**

    Now we have **8** possible outcomes

    ![Sample Space Frequency Distribution 2](assets/sample_space_frequency_distribution_2.jpg)

---

## Event (A)

Is a set of **outcomes** of a trial (**a subset of the sample space**) to which a probability is assigned. called event or $\textbf{A}$

<!--prettier-ignore-->
!!! example "Example"
    1. See 6 in dice flipping
        - Event: **{6}**
    2. See 2 heads in flipping 3 coins
        - Event: **{HTH, HHT, THH}**
    3. Space of dart and center less than 20cm
        - Event: **(0, 20)**
    4. Lamp age more than 200 hours
        - Event: **(200, inf)**

<!--prettier-ignore-->
!!! example "Full Example"
    -   **Experiment**: flipping a dice
    -   **Sample Space**: {1,2,3,4,5,6}
    -   **Event 1**: Getting odd
    -   **Event 2**: Getting 5

---

### Visualize

We can visualize our events in sample space using diagrams

#### Venn Diagram

Events are subset of **Sample Space** so we can use venn diagrams to show them much more readable and visualize their relations

![Event Venn Diagram](assets/event_venn_diagram.png)

---

### Types

There are many types of events based on their sizes and probabilities

#### Simple (1)

An event with only **one** sample

<!--prettier-ignore-->
!!! example "Example"
    Getting **6** in dice flip

---

#### Compound (k)

An event with **more than one** samples

<!--prettier-ignore-->
!!! example "Example"
    Getting **6 or 5 or 1** in dice flip

---

#### Certain (S)

An event equals to **sample space**

<!--prettier-ignore-->
!!! example "Example"
    Getting **Head or Tail** in coin flip

---

#### Impossible (0)

An event **no** sample

<!--prettier-ignore-->
!!! example "Example"
    Getting **5** in coin flip

---

#### Equally Likely (1/2)

An event with probability of **1/2**

<!--prettier-ignore-->
!!! example "Example"
    Getting **odd number** in dice flip

---

### Occurence

If the outcome of experiment was **ω**, and **ω ∈ E** we say E **occurs**, otherwise if **ω ∉ E** we say E **not occurs**

<!--prettier-ignore-->
!!! example "Example"
    **Event**: See odd number in dice flipping

    **Experiment Result**: 5

    E occurs!!

We have two types of events based on their occurance types

---

#### Independent

If each event is **not affected** by any other events, we called them **independent**

<!--prettier-ignore-->
!!! example "Example"
    Flipping two coins and getting **HT**

    1. We flip first coin
    2. We flip second coin (not dependent to first)

---

#### Dependent

If they can be **affected** by **previous** events.

<!--prettier-ignore-->
!!! example "Example"
    Picking two cards from a deck and get **4,8**

    1. We pick first card
    2. We pick second card (first card cannot occur! - dependent)

---

### Relations

Events are sets, so they can have relations with each other

![Event Relations](assets/event_relations.png)

---

#### Subset

If event **X** is subset of event **Y**, and occurence of **X** means the occurence of **Y**

$$
\begin{aligned}
    A \subseteq B
\end{aligned}
$$

![Subset](assets/event_subset.png)

<!--prettier-ignore-->
!!! example "Example"
    **Event 1**: Flipping a dice and and getting 4

    **Event 2**: Flipping a dice and and getting even number

    **E1** is subset of **E2**

---

#### Equal

$$
\begin{aligned}
    A = B: A \subseteq B , B \subseteq A
\end{aligned}
$$

![Equal](assets/event_equal.jpeg)

<!--prettier-ignore-->
!!! example "Example"
    **Event 1**: Flipping a dice and and getting 2 or 4 or 6

    **Event 2**: Flipping a dice and and getting even number

    **E1** is equal to **E2**

---

#### Disjoint (Mutually Exclusive)

If event **X** and event **Y** has **no intersection**, they called disjoint

![Disjoint](assets/event_disjoint.png)

<!--prettier-ignore-->
!!! example "Example"
    **Event 1**: Flipping a dice and and getting 2

    **Event 2**: Flipping a dice and and getting 5

    **E1** and **E2** are disjoint

---

#### Union

![Union](assets/event_union.png)

<!--prettier-ignore-->
!!! example "Example"
    **Event 1**: Flipping a dice and and getting 2

    **Event 2**: Flipping a dice and and getting 5

    **Event 3**: Flipping a dice and and getting 2 or 5

    **E3** is union of **E1** and **E2**

---

#### Intersection

![Intersect](assets/event_intersect.png)

<!--prettier-ignore-->
!!! example "Example"
    **Event 1**: Flipping a dice and and getting odd number

    **Event 2**: Flipping a dice and and getting 1 or 2

    **Event 3**: Flipping a dice and and getting 1

    **E3** is intersect of **E1** and **E2**

---

#### Complement

![Complement](assets/event_complement.png)

<!--prettier-ignore-->
!!! example "Example"
    **Event 1**: Flipping a dice and and getting 1 or 2

    **Event 2**: Flipping a dice and and getting 3 or 4 or 5 or 6

    **E2** is complement of **E1**

---

#### Subtraction

![Subtract](assets/event_subtract.png)

<!--prettier-ignore-->
!!! example "Example"
    **Event 1**: Flipping a dice and and getting 1 or 2 or 3

    **Event 2**: Flipping a dice and and getting 3 or 4 or 5 or 6

    **Event 3**: Flipping a dice and and getting 1 or 2

    **E3** is **E1** subtracts **E2**

---

#### De Morgan’s Law

A useful law that can simplify calculating probabilities using converting **Unions** to **Intersections** and reverse

$$
\begin{aligned}
    & (A \cup B)^{c} = A^{c} \cap B^{c}
    \\
    & (A \cap B)^{c} = A^{c} \cup B^{c}
\end{aligned}
$$

---
