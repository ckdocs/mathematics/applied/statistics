# Dispersion (Quantitative)

Measure of **spread** or **dipersion** is used to describe the variablility in a sample or population

1. **Qualitative** DataSet: `Compare Distribution Table`
2. **Quantitative** DataSet: `Measure of Dispersion`

<!--prettier-ignore-->
!!! tip "Measure of Dispersion"
    Is a number, show the distance of values, if it goes **bigger** `distance` is **bigger**, if it goes **smaller** `distance` is **smaller**

    ---

    **Example**:

    $$
    \begin{aligned}
    & \textbf{DataSet-1}: 1,20,40,60,80,100
    \\
    & \textbf{Range}: 100 - 1 = 99 && Big-distance
    \\
    \\
    & \textbf{DataSet-2}: 1,2,3,4,5,6
    \\
    & \textbf{Range}: 6 - 1 = 5 && Small-distance
    \end{aligned}
    $$

![Dispersion](assets/distribution_measure_dispersion.png)

## Range (**R**)

The distance of max value and min value in dataset called **Range** or $\textbf{R}$

1. **Sort** DataSet
2. Subtitute the **first** and **last** data

$$
\begin{aligned}
    & R = X_{(n)} - X_{(1)}
\end{aligned}
$$

<!--prettier-ignore-->
!!! example "Example"
    $$
    \begin{aligned}
        & \textbf{DataSet}: 1,2,3,4,7,4,3,2,10
        \\
        & \textbf{Sorted}: 1,2,2,3,3,4,4,7,10
        \\
        \\
        & \textbf{R} = 10 - 1 = 9
    \end{aligned}
    $$

<!--prettier-ignore-->
!!! danger "Problem of range"
    Range measure, can only use **max** and **min** values **not** the entire dataset and **central tendency**

    ---

    **Example**:

    $$
    \begin{aligned}
        & \textbf{DataSet-1}: 1,2,3,100
        \\
        & \textbf{R} = 100 - 1 = 99
        \\
        \\
        & \textbf{DataSet-2}: 1,2,3,4
        \\
        & \textbf{R} = 4 - 1 = 3
    \end{aligned}
    $$

---

## Interquartile Range (**Q** - **IQR**)

The distance of $\textbf{q}_1$ value and $\textbf{q}_3$ value in dataset called **Interquartile Range** or $\textbf{Q}$

1. **Sort** DataSet
2. Subtitute the **3th quartile** and **1th quartile** value

$$
\begin{aligned}
    & Q = IQR = q_{3} - q_{1}
\end{aligned}
$$

<!--prettier-ignore-->
!!! example "Example"
    $$
    \begin{aligned}
        & \textbf{DataSet}: 10,12,6,4,9,3,7,8,5,1,11,2
        \\
        & \textbf{Sorted}: 1,2,3 | 4,5,6 | 7,8,9 | 10,11,12
        \\
        & \textbf{q}_1: \frac{3+4}{2} = 3.5
        \\
        &
        \textbf{q}_3: \frac{9+10}{2} = 9.5
        \\
        \\
        & \textbf{Q} = 9.5 - 3.5 = 6
    \end{aligned}
    $$

---

## Variance (**S2**)

The mean of distances of values from their mean called **Variance** or $\textbf{S}^2$ or $\sigma^2$ or $\textbf{Var(X)}$

1. Find **Arithmetic Mean**
2. Find distance of **values** and **mean** power two
3. Mean **distances**

$$
\begin{aligned}
    & Var(X) = S^2 = \sigma^2 = \frac{ \sum\limits_{i=1}^n \left(X_i - \overline{X}\right) ^ 2 }{n}
\end{aligned}
$$

<!--prettier-ignore-->
!!! example "Example"
    $$
    \begin{aligned}
        & \textbf{DataSet}: 1,2,3,4,5
        \\
        & \overline{\textbf{X}}: \frac{1 + 2 + 3 + 4 + 5}{5} = 3
        \\
        \\
        & \textbf{Var(X)} = \frac{ \left(1 - 3\right)^2 + \left(2 - 3\right)^2 + \left(3 - 3\right)^2 + \left(4 - 3\right)^2 + \left(5 - 3\right)^2 }{5} = 2
    \end{aligned}
    $$

<!--prettier-ignore-->
!!! warning "Unit of Measurement"
    The unit of measurement of variance is unit of datas **power two**, for this problem we introduce new measure called **Standard Deviation**

    ---

    **Example**:

    * Data Unit: **cm**, **kg**
    * Variance Unit: **cm^2**, **kg^2**
    * Standard Deviation Unit: **cm**, **kg**

---

## Standard Deviation (**S**)

Sqrt of variance called **Standard Deviation** or $\textbf{S}$ or $\sigma$ or $\textbf{SD(X)}$

1. **Sqrt** of the **Variance**

$$
\begin{aligned}
    & SD(X) = S = \sigma = \sqrt{ Var(X) }
\end{aligned}
$$

<!--prettier-ignore-->
!!! example "Example"
    $$
    \begin{aligned}
        & \textbf{DataSet}: 1,2,3,4,5
        \\
        & \overline{\textbf{X}}: \frac{1 + 2 + 3 + 4 + 5}{5} = 3
        \\
        \\
        & \textbf{Var(X)} = \frac{ \left(1 - 3\right)^2 + \left(2 - 3\right)^2 + \left(3 - 3\right)^2 + \left(4 - 3\right)^2 + \left(5 - 3\right)^2 }{5} = 2
        \\
        & \textbf{SD(X)} = \sqrt{2} = 1.42
    \end{aligned}
    $$

---
