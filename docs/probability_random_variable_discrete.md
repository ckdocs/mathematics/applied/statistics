# Discrete Random Variable

There are many families of discrete random variable distributions based on their PMF distribution shape

1. **Bernouli**: True/False
2. **Uniform**: Equally Likly
3. **Binomial**: 2 Variables
4. **Multinomial**: k Variables
5. **Geometric**: Repeat Inf Times
6. **Hyper-Geometric**: Without Replace
7. **Poisson**: Approximate
8. **Negative-Binomial**: R-th Success

<!--prettier-ignore-->
!!! tip "p and q"
    **p** is the probability of occurance and **q** is the probability of not occurance:

    $$
    \begin{aligned}
        & q = 1 - p
    \end{aligned}
    $$

---

## Bernouli Random Variables (True/False)

A random variable that gives us **0** or **1** (**True** or **False**) by probability of **p**

$$
\begin{aligned}
    & X \sim Bernouli(p)
    \\
    \\
    & X(x)= \begin{cases}
                1 & Condition(x)
                \\
                0 & !Condition(x)
            \end{cases}
\end{aligned}
$$

<!--prettier-ignore-->
!!! example "Example"
    1. Coin example
        * **Trial**: Flipping a coin
        * **Sample Space**: {H, T}
        * **X**: Getting head
            * $P_X(0) = p(0) = \frac{1}{2}$
            * $P_X(1) = p(1) = \frac{1}{2}$
    2. Dice example
        * **Trial**: Tossing a dice
        * **Sample Space**: {1, 2, 3, 4, 5, 6}
        * **X**: Getting 5
            * $P_X(0) = p(0) = \frac{5}{6}$
            * $P_X(1) = p(1) = \frac{1}{6}$
        * **Y**: Getting odd number
            * $P_Y(0) = p(0) = \frac{3}{6}$
            * $P_Y(1) = p(1) = \frac{3}{6}$

    ---

    Flipping a dice and getting 2 or 5:

    $$
    \begin{aligned}
        &   \begin{cases}
                1 \rightarrow False
                \\
                2 \rightarrow \textbf{True}
                \\
                3 \rightarrow False
                \\
                4 \rightarrow False
                \\
                5 \rightarrow \textbf{True}
                \\
                6 \rightarrow False
            \end{cases}
        \\
        & P_X(1) = \frac{2}{6}
        \\
        & P_X(0) = \frac{4}{6}
    \end{aligned}
    $$

---

### PMF

$$
\begin{aligned}
    & P(X = x) = p(x) = \begin{cases}
                            1 - p & x = 0
                            \\
                            p & x = 1
                        \end{cases}
    \\
    & x \in \{0, 1\}
\end{aligned}
$$

<!--prettier-ignore-->
!!! tip "Plot"
    Bernouli plot have two columns with values of **p** and **1 - p**

    ![Bernouli PMF](assets/bernouli_pmf.gif)

---

### CDF

$$
\begin{aligned}
    & F(x) = P(X \leq x) =  \begin{cases}
                                1 - p & x = 0
                                \\
                                1 & x = 1
                            \end{cases}
    \\
    & x \in \{0, 1\}
\end{aligned}
$$

---

### MGF

$$
\begin{aligned}
    & M(t) = q + pe^t
\end{aligned}
$$

---

### Expectation

$$
\begin{aligned}
    & E(X) = M^{(1)}(0)
    \\
    & E(X) = pe^t
    \\
    & E(X) = pe^0
    \\
    \\
    & E(X) = p
\end{aligned}
$$

---

### Variance

$$
\begin{aligned}
    & Var(X) = E(X^2) - E(X)^2
    \\
    & Var(X) = M^{(2)}(0) - (M^{(1)}(0))^2
    \\
    & Var(X) = pe^t - (pe^t)^2
    \\
    & Var(X) = pe^0 - (pe^0)^2
    \\
    & Var(X) = p - p^2
    \\
    & Var(X) = p(1 - p)
    \\
    \\
    & Var(X) = pq
\end{aligned}
$$

---

## Uniform Random Variables (Equally Likly)

A random variable that gives us **a** to **b** by probability of **likly hood**

$$
\begin{aligned}
    & X \sim Unif(a, b)
    \\
    & n = b - a
    \\
    \\
    & X(x)= \begin{cases}
                a & x_1
                \\
                a+1 & x_2
                \\
                a+2 & x_3
                \\
                \dots
                \\
                b & x_n
            \end{cases}
\end{aligned}
$$

<!--prettier-ignore-->
!!! example "Example"
    1. Dice example
        * **Trial**: Tossing a dice
        * **Sample Space**: {1, 2, 3, 4, 5, 6}
        * **X**: Getting dice value
            * $P_X(1) = \frac{1}{6}$
            * $P_X(2) = \frac{1}{6}$
            * $P_X(3) = \frac{1}{6}$
            * $P_X(4) = \frac{1}{6}$
            * $P_X(5) = \frac{1}{6}$
            * $P_X(6) = \frac{1}{6}$

    ---

    Flipping a dice:

    $$
    \begin{aligned}
        &   \begin{cases}
                1 \rightarrow 1
                \\
                2 \rightarrow 2
                \\
                3 \rightarrow 3
                \\
                4 \rightarrow 4
                \\
                5 \rightarrow 5
                \\
                6 \rightarrow 6
            \end{cases}
        \\
        & P_X(1) = \frac{1}{6}
        \\
        & P_X(2) = \frac{1}{6}
        \\
        & P_X(3) = \frac{1}{6}
        \\
        & P_X(4) = \frac{1}{6}
        \\
        & P_X(5) = \frac{1}{6}
        \\
        & P_X(6) = \frac{1}{6}
    \end{aligned}
    $$

---

### PMF

$$
\begin{aligned}
    & P(X = x) = p(x) = \frac{1}{n}
    \\
    & x \in \{a, a+1, \dots, b\}
\end{aligned}
$$

<!--prettier-ignore-->
!!! tip "Plot"
    Uniform plot have n columns with values of **1/n**

    ![Uniform PMF](assets/uniform_pmf.png)

---

### CDF

$$
\begin{aligned}
    & F(x) = P(X \leq x) = \frac{x - a + 1}{n}
    \\
    & x \in \{a, a+1, \dots, b\}
\end{aligned}
$$

<!--prettier-ignore-->
!!! tip "Plot"
    Uniform plot have n columns with values of **1/n**, **2/n**, ..., **n/n**

    ![Uniform CDF](assets/uniform_cdf.png)

---

### MGF

$$
\begin{aligned}
    & M(t) = \frac{e^{bt} - e^{at}}{(b - a)t}
\end{aligned}
$$

---

### Expectation

$$
\begin{aligned}
    & E(X) = \frac{a + b}{2}
\end{aligned}
$$

---

### Variance

$$
\begin{aligned}
    & Var(X) = \frac{(b - a).(b - a + 2)}{12}
\end{aligned}
$$

---

## Binomial Random Variables (2 Variables)

We repeat a trial for **N** times and find the number of a specific outcome

$$
\begin{aligned}
    & X \sim B(n, p)
    \\
    & Y \sim B(n, p)
    \\
    \\
    & X(k)= \texttt{Number of Heads in k}
    \\
    & Y(k) = \texttt{Number of Tails in k}
\end{aligned}
$$

<!--prettier-ignore-->
!!! example "Example"
    1. Coin example
        * **Trial**: Flipping a coin 3 times
        * **Sample Space**: {HHH, HHT, HTH, HTT, ..., TTT}
        * **X**: Number of heads
        * **Y**: Number of tails
            * $p = \frac{1}{2}, n = 3$
            * $P_X(0) = P_{X,Y}(0,3) = \frac{1}{8}$
            * $P_X(1) = P_{X,Y}(1,2) = \frac{3}{8}$
            * $P_X(2) = P_{X,Y}(2,1) = \frac{3}{8}$
            * $P_X(3) = P_{X,Y}(3,0) = \frac{1}{8}$

    ---

    Flipping a coin 3 times and getting number of heads:

    $$
    \begin{aligned}
        &   \begin{cases}
                HHH \rightarrow 3
                \\
                HHT \rightarrow 2
                \\
                HTH \rightarrow 2
                \\
                HTT \rightarrow 1
                \\
                THH \rightarrow 2
                \\
                THT \rightarrow 1
                \\
                TTH \rightarrow 1
                \\
                TTT \rightarrow 0
            \end{cases}
        \\
        & P_X(0) = \frac{1}{8}
        \\
        & P_X(1) = \frac{3}{8}
        \\
        & P_X(2) = \frac{3}{8}
        \\
        & P_X(3) = \frac{1}{8}
    \end{aligned}
    $$

    ![Binomial Example](assets/binomial_example.png)

---

### PMF

$$
\begin{aligned}
    & P(X = x) = p(x) = {n \choose x} p^{x} . (1 - p)^{n-x}
    \\
    & P(X = x, Y = y) = P_{X, Y}(x, y) = {n \choose x} p^{x} . (1 - p)^{y = n-x}
    \\
    & x \in \{0, 1, \dots, n\}
\end{aligned}
$$

<!--prettier-ignore-->
!!! tip "Plot"
    Binomial plot is something like this

    ![Binomial PMF](assets/binomial_pmf.png)
    ![Binomial](assets/binomial.png)

---

### CDF

$$
\begin{aligned}
    & F(x) = P(X \leq x) = \sum\limits_{i=0}^{x} {n \choose x} p^{x} . (1 - p)^{n-x}
    \\
    & x \in \{0, 1, \dots, n\}
\end{aligned}
$$

<!--prettier-ignore-->
!!! tip "Plot"
    Binomial plot is something like this

    ![Binomial CDF](assets/binomial_cdf.png)

---

### MGF

$$
\begin{aligned}
    & M(t) = (q + pe^t)^n
\end{aligned}
$$

---

### Expectation

$$
\begin{aligned}
    & E(X) = M^{(1)}(0)
    \\
    & E(X) = n.(pe^t).(q + pe^t)^{n-1}
    \\
    & E(X) = n.(pe^0).(q + pe^0)^{n-1}
    \\
    & E(X) = n.(p).(q + p)^{n-1}
    \\
    & E(X) = n.(p).(1)^{n-1}
    \\
    \\
    & E(X) = np
\end{aligned}
$$

---

### Variance

$$
\begin{aligned}
    & Var(X) = npq
\end{aligned}
$$

---

## Multinomial Random Variables (k Variables)

Generalized version of binomial distribution

We repeat a trial for **N** times and find the number of a specific outcome

$$
\begin{aligned}
    & X_1(k)= \texttt{Number of X1's in k}
    \\
    & X_2(k)= \texttt{Number of X2's in k}
    \\
    & X_3(k)= \texttt{Number of X3's in k}
    \\
    & \dots
    \\
    & X_m(k)= \texttt{Number of Xm's in k}
\end{aligned}
$$

<!--prettier-ignore-->
!!! example "Example"
    1. Dice example
        * **Trial**: Toss a dice for 10 times
        * **Sample Space**: {111111, 111112, 111113, ..., 666666}
        * **X1**: Number of 1's
        * **X2**: Number of 2's
        * **X3**: Number of 3's
        * **X4**: Number of 4's
        * **X5**: Number of 5's
        * **X6**: Number of 6's

    ---

    Flipping a dice 10 times and getting number of 1's and 2's and 3's and 4's and 5's and 6's:

    $$
    \begin{aligned}
        &   \begin{cases}
                & X_1(122643) = 1
                \\
                & X_2(122643) = 2
                \\
                & X_3(122643) = 1
                \\
                & X_4(122643) = 1
                \\
                & X_5(122643) = 0
                \\
                & X_6(122643) = 1
            \end{cases}
        \\
        & P_{X_1,X_2,X_3,X_4,X_5,X_6}(1,2,1,1,0,1) = \frac{6!}{1!.2!.1!.1!.0!.1!} . \frac{1}{6}^1 . \frac{1}{6}^2 . \frac{1}{6}^1 . \frac{1}{6}^1 . \frac{1}{6}^0 . \frac{1}{6}^1
    \end{aligned}
    $$

---

### PMF

$$
\begin{aligned}
    & P(X_1 = x_1, X_2 = x_2, X_3 = x_3, \dots, X_m = x_m) = {n \choose x_1, x_2, x_3, \dots, x_m} p_1^{x_1} p_2^{x_2} p_3^{x_3} \dots p_m^{x_m}
    \\
    & x_1 + x_2 + x_3 + \dots + x_m = n
    \\
    \\
    & {n \choose x_1, x_2, x_3, \dots, x_m} = \frac{n!}{x_1! x_2! x_3! \dots x_m!}
\end{aligned}
$$

<!--prettier-ignore-->
!!! tip "Plot"
    Multinomial plot is N-Dimentional

    ![Multinomial PMF](assets/multinomial_pmf.png)

---

### CDF

$$
\begin{aligned}
    & F(X_1 \leq x_1, X_2 \leq x_2, X_3 \leq x_3, \dots, X_m \leq x_m) = \sum\limits_{i_1 = 0}^{x_1} \sum\limits_{i_2 = 0}^{x_2} \sum\limits_{i_3 = 0}^{x_3} \dots \sum\limits_{i_m = 0}^{x_m} p(i_1, i_2, i_3, \dots, i_m)
    \\
    & x_1 + x_2 + x_3 + \dots + x_m = n
\end{aligned}
$$

<!--prettier-ignore-->
!!! tip "Plot"
    Multinomial plot is N-Dimentional

    ![Multinomial CDF](assets/multinomial_cdf.png)

---

### MGF

$$
\begin{aligned}
    M(t_1, t_2, \dots, t_m) = \left( \sum\limits_{i = 1}^{m} p_i e^{t_i} \right) ^ n
\end{aligned}
$$

---

### Expectation

$$
\begin{aligned}
    & E(X_i) = np_i
\end{aligned}
$$

---

### Variance

$$
\begin{aligned}
    & Var(X_i) = np_i(1 - p_i)
\end{aligned}
$$

---

## Geometric Random Variables (Repeat Inf Times)

A random variable that gives us number of trials, runs to win

$$
\begin{aligned}
    & \texttt{Sample Space} = \texttt{Set of infinite sequence of Head and Tail}
    \\
    & \{HHTTTHTH\dots, HHHH\dots, \dots\}
    \\
    \\
    & X \sim Geom(p)
    \\
    \\
    & X(x) = \texttt{Number of tosses to first Head}
\end{aligned}
$$

<!--prettier-ignore-->
!!! example "Example"
    1. Coin example
        * **Trial**: Flipping a coin until get first head
        * **Sample Space**: {H, TH, TTH, TTTH, ...}
        * **X**: Number of tosses
            * $P_X(1) = \frac{1}{2} . \left(1 - \frac{1}{2}\right)^0 = \frac{1}{2}$
            * $P_X(2) = \frac{1}{2} . \left(1 - \frac{1}{2}\right)^1 = \frac{1}{4}$
            * $P_X(3) = \frac{1}{2} . \left(1 - \frac{1}{2}\right)^2 = \frac{1}{8}$
    2. Dice example
        * **Trial**: Tossing a dice until get first 6
        * **Sample Space**: {1, 2, 3, 4, 5, 6, 16, 26, 36, ...}
        * **X**: Number of tosses
            * $P_X(1) = \frac{1}{6} . \left(1 - \frac{1}{6}\right)^0 = \frac{1}{6}$
            * $P_X(2) = \frac{1}{6} . \left(1 - \frac{1}{6}\right)^1 = \frac{5}{36}$
            * $P_X(3) = \frac{1}{6} . \left(1 - \frac{1}{6}\right)^2 = \frac{25}{216}$

    ---

    Flipping a coin and until get a Head:

    $$
    \begin{aligned}
        &   \begin{cases}
                H \rightarrow 1
                \\
                TH \rightarrow 2
                \\
                TTH \rightarrow 3
                \\
                TTTH \rightarrow 4
                \\
                TTTTH \rightarrow 5
                \\
                \dots
            \end{cases}
        \\
        & P_X(1) = \frac{1}{2}
        \\
        & P_X(0) = \frac{1}{4}
    \end{aligned}
    $$

---

### PMF

$$
\begin{aligned}
    & P(X = 1) = P(H) = p
    \\
    & P(X = 2) = P(TH) = (1-p).p
    \\
    & P(X = 3) = P(TTH) = (1-p).(1-p).p
    \\
    & P(X = 4) = P(TTTH) = (1-p).(1-p).(1-p).p
    \\
    & \dots
    \\
    & P(X = x) = p(x) = p . (1 - p)^{x-1}
    \\
    & x \in \{1, 2, \dots, \infty\}
\end{aligned}
$$

<!--prettier-ignore-->
!!! tip "Plot"
    Geometric plot have infinite number of columns

    ![Geometric PMF](assets/geometric_pmf.png)

    ![Geometric Plot](assets/geometric_plot.png)

---

### CDF

$$
\begin{aligned}
    & F(x) = P(X \leq x) = 1 - (1 - p)^x
    \\
    & x \in \{1, 2, \dots, \infty\}
\end{aligned}
$$

<!--prettier-ignore-->
!!! tip "Plot"
    Geometric plot have infinite number of columns

    ![Geometric CDF](assets/geometric_cdf.png)

---

### MGF

$$
\begin{aligned}
    & M(t) = \frac{pe^t}{1 - (1 - p)e^t}
\end{aligned}
$$

---

### Expectation

$$
\begin{aligned}
    & E(X) = \frac{1}{p}
\end{aligned}
$$

---

### Variance

$$
\begin{aligned}
    & Var(X) = \frac{1 - p}{p^2}
\end{aligned}
$$

---

## Hyper-Geometric Random Variables (Without Replace)

Is non-replacement version of binomial

$$
\begin{aligned}
    & X \sim HyperGeom(N, K, n)
    \\
    & Y \sim HyperGeom(N, K, n)
    \\
    \\
    & N: \texttt{Bag size}
    \\
    & K: \texttt{Our target objects size in bag}
    \\
    & n: \texttt{Total picks}
    \\
    \\
    & X(k)= \texttt{Number of Heads in k}
    \\
    & Y(k) = \texttt{Number of Tails in k}
\end{aligned}
$$

<!--prettier-ignore-->
!!! example "Example"
    1. Balls example
        * **Trial**: Pick 5 balls from a bag with 6 red and 14 yellow
        * **Sample Space**: {RRRRR, RRRRY, RRRYR, ..., YYYYY}
        * **X**: Number of reds
        * **Y**: Number of yellows

    ---

    Pick 5 balls from bag and get number of red's and blue's:

    $$
    \begin{aligned}
        &   \begin{cases}
                & X(RRRYR) = 4
                \\
                & Y(RRRYR) = 1
            \end{cases}
        \\
        & P_{X,Y}(4,1) = \frac{{6 \choose 4}.{14 \choose 1}}{{20 \choose 5}}
    \end{aligned}
    $$

<!--prettier-ignore-->
!!! danger "Hyper-Geometric vs Binomial"
    **Binomial**: With-Replacement
    **Hyper-Geometric**: Without-Replacement

    ---

    Pick 5 balls from bag and expect 4 red balls:

    * **Binomial**: ${5 \choose 4} \frac{6}{20}^{4} . (1 - \frac{6}{20})^{1}$
    * **Hyper-Geometric**: $\frac{{6 \choose 4}.{14 \choose 1}}{{20 \choose 5}}$

---

### PMF

$$
\begin{aligned}
    & P(X = x) = p(x) = \frac{{K \choose x} . {N-K \choose n-x}}{{N \choose n}}
    \\
    \\
    & N: \texttt{Bag has 20 balls}
    \\
    & K: \texttt{Bag has 6 red balls}
    \\
    & n: \texttt{We want select 5 balls}
    \\
    & x: \texttt{Number of red balls we picked}
    \\
    \\
    & x \in \{0, 1, \dots, min(n, K)\}
\end{aligned}
$$

<!--prettier-ignore-->
!!! tip "Plot"
    Hyper-Geometric plot is something like this

    ![Hyper-Geometric PMF](assets/hyper_geometric_pmf.png)

---

### CDF

$$
\begin{aligned}
    & F(x) = P(X \leq x) = \sum\limits_{i=0}^{x} \frac{{K \choose i} . {N-K \choose n-i}}{{N \choose n}}
    \\
    & x \in \{0, 1, \dots, min(n, K)\}
\end{aligned}
$$

<!--prettier-ignore-->
!!! tip "Plot"
    Hyper-Geometric plot is something like this

    ![Hyper-Geometric CDF](assets/hyper_geometric_cdf.png)

---

### Expectation

$$
\begin{aligned}
    & E(X) = n.\frac{K}{N}
\end{aligned}
$$

---

### Variance

$$
\begin{aligned}
    & E(X) = n.\frac{K}{N}.\frac{(N - K)}{N}.\frac{N - n}{N - 1}
\end{aligned}
$$

---

## Poisson Random Variables (Approximate)

We want to guess the number of events in a period of time

$$
\begin{aligned}
    & X \sim Pois(\lambda)
    \\
    \\
    & \lambda: \texttt{Mean number of events}
    \\
    \\
    & X(x)= \texttt{Number of events in period of time}
\end{aligned}
$$

<!--prettier-ignore-->
!!! example "Example"
    1. Accident example
        * **Trial**: Probability of have 7 car accident in two days a city of mean 4 accident per day 
        * **Sample Space**: {City1, City2, ...}
        * **X**: Number of accidents
    
    ---

    Probability of have 7 car accident in two days a city of mean 4 accident:

    $$
    \begin{aligned}
        & \lambda: Mean: 4*2 = \texttt{8 accident per 2 day}
        \\
        & P(X = 7) = \frac{8^7.e^{-8}}{8!} = 0.017448336
    \end{aligned}
    $$

<!--prettier-ignore-->
!!! danger "Poisson - Binomial"
    We can use **poisson** distribution to estimate **binomial** distribution in simple way to compute

---

### PMF

$$
\begin{aligned}
    & P(X = x) = p(x) = \frac{\lambda^x . e^{-\lambda}}{x!}
\end{aligned}
$$

<!--prettier-ignore-->
!!! tip "Plot"
    Poisson plot is something like this

    ![Poisson PMF](assets/poisson_pmf.png)

---

### CDF

$$
\begin{aligned}
    & F(x) = P(X \leq x) = e^{-\lambda} \sum\limits_{i=0}^{k} \frac{\lambda^i}{i!}
\end{aligned}
$$

<!--prettier-ignore-->
!!! tip "Plot"
    Poisson plot is something like this

    ![Poisson CDF](assets/poisson_cdf.png)

---

### MGF

$$
\begin{aligned}
    & M(t) = exp(\lambda (e^t - 1))
\end{aligned}
$$

---

### Expectation

$$
\begin{aligned}
    & E(X) = M^{(1)}(0)
    \\
    & E(X) = \lambda . e^t . exp(\lambda (e^t - 1))
    \\
    & E(X) = \lambda . e^0 . exp(\lambda (e^0 - 1))
    \\
    & E(X) = \lambda . 1 . exp(0)
    \\
    \\
    & E(X) = \lambda
\end{aligned}
$$

---

### Variance

$$
\begin{aligned}
    & Var(X) = \lambda
\end{aligned}
$$

---

## Negative-Binomial Random Variables (R-th Success)

Is the generalized version of geometric distribution, probability of r-th success be in n-th toss

1. Replace r Heads and x-r Tails: $p^{r}.(1-p)^{x-r}$
2. Combinate Heads and Tails before last Head: ${n-1 \choose r-1}$

$$
\begin{aligned}
    & \texttt{Sample Space} = \texttt{Set of infinite sequence of Head and Tail}
    \\
    & \{HHTTTHTH\dots, HHHH\dots, \dots\}
    \\
    \\
    & X \sim NB(r, p)
    \\
    \\
    & r: \texttt{Number of Heads to stop}
    \\
    & p: \texttt{Head probability}
    \\
    \\
    & X(x) = \texttt{Number of tosses to nth Head}
\end{aligned}
$$

<!--prettier-ignore-->
!!! example "Example"
    1. Coin example
        * **Trial**: Flipping a coin until get third Head
        * **Sample Space**: {HHH, THHHH, TTHHHH, THTHTH, ...}
        * **X**: Number of tosses

    ---

    Flipping a coin and until get three Heads:

    $$
    \begin{aligned}
        &   \begin{cases}
                HHH \rightarrow 1
                \\
                HHTH \rightarrow 4
                \\
                HTHH \rightarrow 4
                \\
                THHH \rightarrow 4
                \\
                HTHTH \rightarrow 5
                \\
                \dots
            \end{cases}
        \\
        & P_X(5) = {4 \choose 2} . \frac{1}{2}^{3} . \frac{1}{2}^{2}
    \end{aligned}
    $$

---

### PMF

$$
\begin{aligned}
    & P(X = x) = {x-1 \choose r-1}.p^{r}.(1-p)^{x-r}
    \\
    & x \in \{r, r+1, \dots\}
\end{aligned}
$$

<!--prettier-ignore-->
!!! tip "Plot"
    Negative Binomial plot have infinite number of columns

    ![Negative Binomial PMF](assets/negative_binomial_pmf.svg)

---

### CDF

$$
\begin{aligned}
    & F(x) = P(X \leq x) = \sum\limits_{i=r}^{x} p_i
\end{aligned}
$$

<!--prettier-ignore-->
!!! tip "Plot"
    Negative Binomial plot have infinite number of columns

    ![Negative Binomial CDF](assets/negative_binomial_cdf.png)

---

### MGF

$$
\begin{aligned}
    & M(t) = \left( \frac{1 - p}{1 - pe^t} \right) ^ r
\end{aligned}
$$

---

### Expectation

$$
\begin{aligned}
    & E(X) = \frac{pr}{1 - p}
\end{aligned}
$$

---

### Variance

$$
\begin{aligned}
    & Var(x) = \frac{pr}{(1 - p)^2}
\end{aligned}
$$

---
