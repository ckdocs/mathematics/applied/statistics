# Types

There are many different types of probability

---

## Models

There are three basic probability models

For more information see [this video](https://www.youtube.com/watch?v=SrEmzdOT65s)

We have a sample **Probability Distribution Table** and we want to learn these models using this distribution:

| XY       | Probability    |
| -------- | -------------- |
| $x_1y_1$ | $\frac{4}{32}$ |
| $x_1y_2$ | $\frac{3}{32}$ |
| $x_1y_3$ | $\frac{9}{32}$ |
| $x_2y_1$ | $\frac{2}{32}$ |
| $x_2y_2$ | $\frac{6}{32}$ |
| $x_2y_3$ | $\frac{0}{32}$ |
| $x_3y_1$ | $\frac{1}{32}$ |
| $x_3y_2$ | $\frac{3}{32}$ |
| $x_3y_3$ | $\frac{0}{32}$ |
| $x_4y_1$ | $\frac{1}{32}$ |
| $x_4y_2$ | $\frac{3}{32}$ |
| $x_4y_3$ | $\frac{0}{32}$ |

Also we can show this distribution in table

![Pribability Distribution](assets/probability_distribution.jpg)

1. We have two **Random Variables** in our trials: `X`, `Y`
2. Sample Space of **X** is $\{x_1, x_2, x_3, x_4\}$
3. Sample Space of **Y** is $\{y_1, y_2, y_3\}$

---

### Joint (Cell)

The probability of **two or more** events occuring at the same time

Every probability cell that depends on **All Random Variables** called **Joint Variable**

$$
\begin{aligned}
    P_{X, Y}(x, y) = P(x, y)
\end{aligned}
$$

<!--prettier-ignore-->
!!! tip "Summation"
    Summation of joint probabilities over **all** variables is **1**:

    $$
    \begin{aligned}
        & \sum\limits_{x} P_{X} (x) = 1
        \\
        & \sum\limits_{x} \sum\limits_{y} P_{X,Y} (x, y) = 1
        \\
        & \sum\limits_{x} \sum\limits_{y} \sum\limits_{z} P_{X,Y,Z} (x, y, z) = 1
        \\
        & \dots
    \end{aligned}
    $$

    ---

    Summation of joint probabilities over **some** variables is **Marginal Probability**:

    $$
    \begin{aligned}
        & P_X(x) = \sum\limits_{y} P_{X,Y} (x, y)
        \\
        & P_Y(y) = \sum\limits_{x} P_{X,Y} (x, y)
        \\
        & \dots
    \end{aligned}
    $$

<!--prettier-ignore-->
!!! danger "PMF vs PDF"
    We can also use **Integral** instead of **Summation** for **PDF's**

<!--prettier-ignore-->
!!! example "Example"
    $$
    \begin{aligned}
        & P_{X, Y}(x_2, y_2) = \frac{6}{32}
        \\
        & P_{X, Y}(x_4, y_1) = \frac{1}{32}
    \end{aligned}
    $$

---

### Marginal (Row - Col)

Every probability row/column that depends on **Not All Random Variables** called **Marginal Variable**

$$
\begin{aligned}
    & P_{X}(x) = \sum_{j} P(x, y_j)
    \\
    & P_{Y}(y) = \sum_{i} P(x_i, y)
\end{aligned}
$$

<!--prettier-ignore-->
!!! danger "PMF vs PDF"
    We can also use **Integral** instead of **Summation** for **PDF's**

<!--prettier-ignore-->
!!! example "Example"
    $$
    \begin{aligned}
        & P_{X}(x_1) = \frac{4}{32} + \frac{3}{32} + \frac{9}{32} = \frac{16}{32}
        \\
        & P_{Y}(y_2) = \frac{3}{32} + \frac{6}{32} + \frac{3}{32} + \frac{3}{32} = \frac{15}{32}
    \end{aligned}
    $$

<!--prettier-ignore-->
!!! danger "Tip - 3D"
    This rule is true in problems with more than two random variable:
    
    $$
    \begin{aligned}
        & \texttt{Marginal Probabilities :}
        \\
        \\
        & P_{X}(x) = \sum_{k} \sum_{j} P(x, y_j, z_k)
        \\
        & P_{Y}(y) = \sum_{k} \sum_{i} P(x_i, y, z_k)
        \\
        & P_{Y}(z) = \sum_{j} \sum_{i} P(x_i, y_j, z)
        \\
        \\
        & P_{X,Y}(x,y) = \sum_{k} P(x, y, z_k)
        \\
        & P_{X,Z}(x,z) = \sum_{j} P(x, y_j, z)
        \\
        & P_{Y,Z}(y,z) = \sum_{i} P(x_i, y, z)
        \\
        \\
        \\
        & \texttt{Joint Probability :}
        \\
        \\
        & P_{X,Y,Z}(x,y,z) = P(x, y, z)
    \end{aligned}
    $$

---

### Conditional (Marginal / Marginal)

Probability of an event that we given another probability occurs before

Every **Event** (A) that we know another **Event** (B) occurs before

$$
\begin{aligned}
    & P(A | B) = P_{A | B}(a) = \frac{P(A, B)}{P(B)} && P(B) > 0
    \\
    & P(B | A) = P_{B | A}(b) = \frac{P(B, A)}{P(A)} && P(A) > 0
\end{aligned}
$$

We can also have conditional variable for **One Event** depends on **Multiple Events**:

$$
\begin{aligned}
    & P (A | B) = P_{A | B}(a) = \frac{P(A, B)}{P(B)}
    \\
    & P (A | B, C) = P_{A | B, C}(a) = \frac{P(A, B, C)}{P(B, C)}
    \\
    & P (A | B, C, D) = P_{A | B, C, D}(a) = \frac{P(A, B, C, D)}{P(B, C, D)}
    \\
    & \dots
\end{aligned}
$$

We can also have conditional variable for **Multiple Event** depends on **Multiple Events**:

$$
\begin{aligned}
    & P (A, B | C) = P_{A, B | C}(a, b) = \frac{P(A, B, C)}{P(C)}
    \\
    & P (A, B | C, D) = P_{A, B | C, D}(a, b) = \frac{P(A, B, C, D)}{P(C, D)}
    \\
    & P (A, B, C | D) = P_{A, B, C | D}(a, b, c) = \frac{P(A, B, C, D)}{P(D)}
    \\
    & \dots
\end{aligned}
$$

<!--prettier-ignore-->
!!! tip "Tip"
    The **Comma** sign, in conditional probabilities is equals to **And**:

    $$
    \begin{aligned}
        & P(A, B, C) = P(A \cap B \cap C)
    \end{aligned}
    $$

<!--prettier-ignore-->
!!! danger "Tip"
    The summation of a conditional random variable is **1**:

    $$
    \begin{aligned}
        & \sum\limits_{x} p_{X | A}(x) = 1
        \\
        & \int\limits_{-\infty}^{\infty} f_{X | A}(x) = 1
    \end{aligned}
    $$

    ![Conditional Summation](assets/conditional_summation.png)

<!--prettier-ignore-->
!!! warning "Discrete Random Variable"
    We can write the conditional probability for discrete random variables:

    $$
    \begin{aligned}
        p_{X | A}(x) =
        \begin{cases}
            \frac{p_X(x)}{P(A)} & x \in A
            \\
            \\
            0 & x \not\in A
        \end{cases}
        \\
        \\
        p_{X | Y}(x | y) = \frac{p_{X, Y}(x, y)}{p_Y(y)}
    \end{aligned}
    $$

<!--prettier-ignore-->
!!! warning "Continous Random Variable"
    We can write the conditional probability for continous random variables:

    $$
    \begin{aligned}
        f_{X | A}(x) =
        \begin{cases}
            \frac{f_X(x)}{P(A)} & x \in A
            \\
            \\
            0 & x \not\in A
        \end{cases}
        \\
        \\
        f_{X | Y}(x | y) = \frac{f_{X, Y}(x, y)}{f_Y(y)}
    \end{aligned}
    $$

<!--prettier-ignore-->
!!! example "Example"
    $$
    \begin{aligned}
        & P(y_2 | x_1) = \frac{\frac{3}{32}}{\frac{16}{32}} = \frac{3}{16}
        \\
        & P(x_4 | y_1) = \frac{\frac{1}{32}}{\frac{8}{32}} = \frac{1}{8}
    \end{aligned}
    $$

![Conditional Probability](assets/conditional_probability.png)

![Conditional Probability 1](assets/conditional_probability_1.png)

![Conditional Probability 2](assets/conditional_probability_2.png)

---

#### Intersect Rule (Multiplication Rule)

We can change the conditional formula fractal style to multiplication to find the intersection formula:

$$
\begin{aligned}
    & P(A | B) = \frac{{P(A, B)}}{P(B)}
    \\
    & P(A, B) = P(B).P(A | B)
    \\
    \\
    & P(A | B, C) = \frac{{P(A, B, C)}}{P(B, C)}
    \\
    & P(A, B, C) = P(B, C).P(A | B, C)
    \\
    & P(A, B, C) = P(C).P(B | C).P(A | B, C)
    \\
    \\
    & P(A, B | C) = \frac{{P(A, B, C)}}{P(C)}
    \\
    & P(A, B, C) = P(C).P(A, B | C)
\end{aligned}
$$

---

#### Union Rule

Using set theory we can find events union formula:

$$
\begin{aligned}
    P(A \cup B) =
    & [P(A) + P(B)] -
    \\
    & [P(A \cap B)]
    \\
    \\
    P(A \cup B \cup C) =
    & [P(A) + P(B) + P(C)] -
    \\
    & [P(A \cap B) + P(A \cap C) + P(B \cap C)] +
    \\
    & [P(A \cap B \cap C)]
    \\
    \\
    \dots
\end{aligned}
$$

---

#### Conditional Expectation

We know the expected value formula:

$$
\begin{aligned}
    & E[X] =
    \begin{cases}
        \sum\limits_{x} x.p_X(x) & Discrete
        \\
        \\
        \int\limits x.f_X(x) dx & Continous
    \end{cases}
    \\
    \\
    & E[g(X)] =
    \begin{cases}
        \sum\limits_{x} g(x).p_X(x) & Discrete
        \\
        \\
        \int\limits g(x).f_X(x) dx & Continous
    \end{cases}
\end{aligned}
$$

Now we can compute the conditional expectation formula:

$$
\begin{aligned}
    & E[X | A] =
    \begin{cases}
        \sum\limits_{x} x.p_{X | A}(x) & Discrete
        \\
        \\
        \int\limits x.f_{X | A}(x) dx & Continous
    \end{cases}
    \\
    \\
    & E[g(X) | A] =
    \begin{cases}
        \sum\limits_{x} g(x).p_{X | A}(x) & Discrete
        \\
        \\
        \int\limits g(x).f_{X | A}(x) dx & Continous
    \end{cases}
\end{aligned}
$$

---

#### Total Probability

We can partition our sample space into $\textbf{A}_1, \textbf{A}_2, \dots, \textbf{A}_n$, then we can find a probability by splitting it using these partitions

$$
\begin{aligned}
    P(B) & = P(B \cap S)
    \\
    & = P(B \cap (A_1 \cup A_2 \cup A_3))
    \\
    & = P((B \cap A_1) \cup (B \cap A_2) \cup (B \cap A_3))
    \\
    & = P(A_1 \cap B) + P(A_2 \cap B) + P(A_3 \cap B)
    \\
    & = P(A_1).P(B | A_1) + P(A_2).P(B | A_2) + P(A_3).P(B | A_3)
\end{aligned}
$$

![Total Probability](assets/total_probability.png)

<!--prettier-ignore-->
!!! warning "PMF"
    We can use the **Total Probability** formula for **PMF**:

    $$
    \begin{aligned}
        & p_X(x) = P(A_1) p_{X | A_1}(x) + \dots + P(A_n) p_{X | A_n}(x)
    \end{aligned}
    $$

<!--prettier-ignore-->
!!! warning "CDF"
    We can use the **Total Probability** formula for **CDF**:

    $$
    \begin{aligned}
        & F_X(x) = P(X \leq x)
        \\
        & F_X(x) = P(A_1) P(X \leq x | A_1) + \dots + P(A_n) P(X \leq x | A_n)
        \\
        \\
        & F_X(x) = P(A_1) F_{X | A_1}(x) + \dots + P(A_n) F_{X | A_n}(x)
    \end{aligned}
    $$

<!--prettier-ignore-->
!!! warning "PDF"
    We can use the **Total Probability** formula for **PDF** using derivating from **CDF**:

    $$
    \begin{aligned}
        & f_X(x) = P(A_1) f_{X | A_1}(x) + \dots + P(A_n) f_{X | A_n}(x)
    \end{aligned}
    $$

<!--prettier-ignore-->
!!! warning "Expectation"
    We can use the **Total Probability** formula for **Expectation**:

    $$
    \begin{aligned}
        & E[X] = P(A_1) E[X | A_1] + \dots + P(A_n) E[X | A_n]
    \end{aligned}
    $$

<!--prettier-ignore-->
!!! example "Example"
    Bill goes to the supermarket, with probability $\frac{1}{3}$ at time uniformly distributed between 0 and 2 from now; or with probability $\frac{2}{3}$, later in the day at a time uniformly distributed between 6 and 8 hours from now, find the total probability:

    ![Total Probability Example](assets/total_probability_example.png)

    $$
    \begin{aligned}
        & f_X(x) = P(A_1) f_{X | A_1}(x) + P(A_2) f_{X | A_2}(x)
        \\
        & f_X(x) = \frac{1}{3} . f_{X | A_1}(x) + \frac{2}{3} . f_{X | A_2}(x)
        \\
        & f_X(x) = \frac{1}{3} . \frac{1}{2 - 0} + \frac{2}{3} . \frac{1}{8 - 6}
        \\
        & f_X(x) = \frac{1}{3} . \frac{1}{2} + \frac{2}{3} . \frac{1}{2}
        \\
        & f_X(x) = \frac{1}{6} + \frac{1}{3}
        \\
        & f_X(x) = \frac{1}{2}
        \\
        \\
        \\
        & E[X] = P(A_1) E[X | A_1] + P(A_2) E[X | A_2]
        \\
        & E[X] = \frac{1}{3} . \frac{a + b}{2} + \frac{2}{3} . \frac{a + b}{2}
        \\
        & E[X] = \frac{1}{3} . \frac{1}{2} + \frac{2}{3} . \frac{7}{2}
        \\
        & E[X] = \frac{1}{6} + \frac{7}{3}
        \\
        & E[X] = \frac{15}{6}
        \\
        & E[X] = \frac{5}{2}
    \end{aligned}
    $$

---

#### Mixed Distributions

We can use **Total Probability** for both **Discrete** and **Continous** random variables, sometimes one of our variables is **Discrete** and another is **Continous**, this total probability distribution called **Mixed Distribution**

Example:

$$
\begin{aligned}
    & X =
    \begin{cases}
        Unif(0, 2) & p: \frac{1}{2}
        \\
        \\
        1 & p: \frac{1}{2}
    \end{cases}
\end{aligned}
$$

![Mixed Distribution](assets/mixed_distribution_1.png)

Find the total **CDF** of this mixed distribution:

$$
\begin{aligned}
    & F_X(x) = P(A_1) F_{X | A_1}(x) + P(A_2) F_{X | A_2}(x)
    \\
    & F_X(x) = \frac{1}{2} . F_{X | A_1}(x) + \frac{1}{2} . F_{X | A_2}(x)
\end{aligned}
$$

![Mixed Distribution](assets/mixed_distribution_2.png)

---

#### Bayes Rule

Is the **reverse** of conditional probability

**Conditional Probability**: We know $\textbf{A}$ occurs, probability of $\textbf{B} \subseteq \textbf{A}$

**Bayes Rule**: We know $\textbf{B} \subseteq \textbf{A}$ occurs, probability of partition $\textbf{A}_i$

$$
\begin{aligned}
    P(A_i | B) & = \frac{P(A_i \cap B)}{P(B)}
    \\
    & = \frac{P(A_i \cap B)}{P(A_1 \cap B) + P(A_2 \cap B) + \dots + P(A_n \cap B)}
    \\
    & = \frac{P(A_i).P(B | A_i)}{P(A_1).P(A_1 | B) + P(A_2).P(A_2 | B) + \dots + P(A_n).P(A_n | B)}
\end{aligned}
$$

![Bayes Rule](assets/bayes_rule.png)

---

## Independent / Dependent

Event **A** occured, then event **B** occured, if probability of **B** depends on **A** we say:
**Event B depends on event A**

The meaning of independent/dependent events is define using conditional probabilities

See this picture of **Probability Tree**:

![Conditional Probability](assets/conditional_probability.png)

Using **Conditional Probability** we assume:

$$
\begin{aligned}
    & P(A | B) = \frac{P(A, B)}{P(B)}
\end{aligned}
$$

If **Event B depends on A**:

$$
\begin{aligned}
    & P(A | B) = P(A | B)
\end{aligned}
$$

If **Event B not depends on A**:

$$
\begin{aligned}
    & P(A | B) = P(A)
\end{aligned}
$$

<!--prettier-ignore-->
!!! tip "Independent - Formula"
    $$
    \begin{aligned}
        & P(B | A) = P(B)
        \\
        & P(A | B) = P(A)
    \end{aligned}
    $$

<!--prettier-ignore-->
!!! example "Example"
    Are **X** and **Y** are independent ?

    ![Joint](assets/joint.png)

    ---

    Solution:

    $$
    \begin{aligned}
        & P(A \cap B) \stackrel{?}{=} P(A).P(B)
        \\
        \\
        & \texttt{Counter example:}
        \\
        & P(X = 1 \cap Y = 3) = \frac{1}{6}
        \\
        & P(X = 1) = \frac{1}{6} + \frac{1}{6} = \frac{2}{6}
        \\
        & P(Y = 3) = \frac{1}{6} + \frac{1}{6} = \frac{2}{6}
        \\
        \\
        & P(X = 1 \cap Y = 3) \neq P(X = 1).P(Y = 3)
        \\
        \\
        & \texttt{X and Y are dependent}
    \end{aligned}
    $$

---

### Complement

If we know **A** and **B** events, are independent, also **complements** of thems are independent:

1.  $\textbf{A}$
2.  $\textbf{A}^{c}$
3.  $\textbf{B}$
4.  $\textbf{B}^{c}$

---

### Multiplication Rule

Using multiplication rule we can find the intersection formula:

$$
\begin{aligned}
    & P(A, B) = P(B).P(A | B)
    \\
    & P(A, B, C) = P(C).P(B | C).P(A | B, C)
    \\
    & P(A, B, C, D) = P(D).P(C | D).P(B | C, D).P(A | B, C, D)
\end{aligned}
$$

Now if **A** and **B** will be independent this formula is somthing like this:

$$
\begin{aligned}
    & P(A, B) = P(B).P(A)
    \\
    & P(A, B, C) = P(C).P(B).P(A)
    \\
    & P(A, B, C, D) = P(D).P(C).P(B).P(A)
    \\
    & \dots
    \\
    & P(A_1 \cap A_2 \cap \dots \cap A_n) = P(A_1) . P(A_2) . \dots . P(A_n)
\end{aligned}
$$

---

### Conditional Independence

When events **A** and **B** are independent, we can assume an event **C** that **A** and **B** are depends on that, we can add this condition using $P(\dots | C)$

$$
\begin{aligned}
    & P(A \cap B) = P(A) . P(B)
    \\
    \\
    & P(A \cap B | C) = P(A | C) . P(B | C)
\end{aligned}
$$

<!--prettier-ignore-->
!!! example "Example"
    We have two coins:

    1. **Event A**: Toss Coin A: `P(H | Coin A) = 0.9`
    2. **Event B**: Toss Coin B: `P(H | Coin B) = 0.1`

    Probability of tossing coin **A** is **independent** from tossing coin **B**

    ![Conditional Independent Example 1](assets/conditional_independent_example_1.png)

    ---

    Now assume we add **event C**, randomly select a coin, then toss it

    1. **Event C**: Select a coin: `P(Coin A) = P(Coin B) = 0.5`

    ---

    **Question**:

    1. **P(Toss 11 = H)**

    $$
    \begin{aligned}
        P(H_{11}) & = P(H_{11} | A) + P(H_{11} | B)
        \\
        & = P(A).P(H_{11} \cap A) + P(B).P(H_{11} \cap B)
        \\
        & = 0.5 * 0.9 + 0.5 * 0.1
        \\
        & = 0.5
    \end{aligned}
    $$

    1. **P(Toss 11 = H | first 10 tosses are H)**

    $$
    \begin{aligned}
        P(H_{11} | H_{1\dots10}) & = \frac{P(H_{11} \cap H_{1\dots10})}{P(H_{1\dots10})}
    \end{aligned}
    $$

---

### Pairwise Independence

Sometimes we have 3 events that they are doublly **independent** but three of them are **depedent**

<!--prettier-ignore-->
!!! example "Example"
    We toss a coin for two times

    **Sample Space**: `{TT, TH, HT, HH}`

    **Event A (First H)**: `{HT, HH}`

    **Event B (Second H)**: `{TH, HH}`

    **Event C (Same)**: `{TT, HH}`

    $$
    \begin{aligned}
        & A \cap B = \{HH\}
        \\
        & A \cap C = \{HH\}
        \\
        & B \cap C = \{HH\}
        \\
        \\
        & P(A) = \frac{1}{2}
        \\
        & P(B) = \frac{1}{2}
        \\
        & P(C) = \frac{1}{2}
        \\
        \\
        & P(A \cap B) = P(A).P(B) = \frac{1}{4}
        \\
        & P(A \cap C) = P(A).P(C) = \frac{1}{4}
        \\
        & P(B \cap C) = P(B).P(C) = \frac{1}{4}
        \\
        \\
        & P(A \cap B \cap C) = \frac{1}{4}
        \\
        & P(A).P(B).P(C) = \frac{1}{2}.\frac{1}{2}.\frac{1}{2} = \frac{1}{8}
        \\
        \\
        & \texttt{A, B, C are dependent}
    \end{aligned}
    $$

---

### Independent Expectation

If **X** and **Y** are independent then:

$$
\begin{aligned}
    & E[XY] = E[X].E[Y]
\end{aligned}
$$

<!--prettier-ignore-->
!!! tip "Proof"
    $$
    \begin{aligned}
        & g(X, Y) = X.Y
        \\
        & E[XY] = E[g(X, Y)]
        \\
        & E[XY] = \sum\limits_{x} \sum\limits_{y} g(x, y).P_{X,Y}(x, y)
        \\
        & E[XY] = \sum\limits_{x} \sum\limits_{y} x.y.P_{X,Y}(x, y)
        \\
        & E[XY] = \sum\limits_{x} \sum\limits_{y} x.y.P_{X}(x).P_{Y}(y)
        \\
        & E[XY] = \left(\sum\limits_{x} x.P_X(x)\right) + \left(\sum\limits_{y} y.P_Y(y)\right)
        \\
        \\
        & E[XY] = E[X].E[Y]
    \end{aligned}
    $$

<!--prettier-ignore-->
!!! tip "Basic Rules"
    We know these rules:
    
    $$
    \begin{aligned}
        & E[aX + b] = a.E[X] + b
        \\
        & E[X + Y + Z] = E[X] + E[Y] + E[Z]
    \end{aligned}
    $$

---

### Independent Variance

If **X** and **Y** are independent then:

$$
\begin{aligned}
    & Var(X + Y) = Var(X) + Var(Y)
\end{aligned}
$$

<!--prettier-ignore-->
!!! tip "Proof"
    $$
    \begin{aligned}
        & g(X, Y) = X + Y
        \\
        & Var(X + Y) = E[(X + Y)^2]
        \\
        & Var(X + Y) = E[X^2 + 2XY + Y^2]
        \\
        & Var(X + Y) = E[X^2] + 2.E[XY] + E[Y^2]
        \\
        & Var(X + Y) = E[X^2] + 2.E[X].E[Y] + E[Y^2]
        \\
        \\
        & E[X] = E[Y] = 0
        \\
        \\
        & Var(X + Y) = E[X^2] + E[Y^2]
        \\
        \\
        & Var(X + Y) = Var(X) + Var(Y)
    \end{aligned}
    $$

<!--prettier-ignore-->
!!! tip "Basic Rules"
    We know these rules:
    
    $$
    \begin{aligned}
        & Var(aX) = a^2.Var(X)
        \\
        & Var(X + b) = Var(X)
    \end{aligned}
    $$

---
