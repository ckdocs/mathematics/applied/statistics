# Counting Principles

Counting the members of a group can compute using two basic rules:

1. **Add**: `A` or `B`
2. **Product**: `A` then `B`

![Counting Principle Example](assets/counting_principle_example.png)

---

## Add Counting Principle

The event `A` can occur in `3` ways
The event `B` can occur in `5` ways

The event `A or B` can occur in `5 + 3 = 8` ways

---

## Product Counting Principle

The event `A` can occur in `3` ways
The event `B` can occur in `5` ways

The event `A then B` can occur in `5 * 3 = 15` ways

### Tree Diagram

We can visualize our product counting using a tree

![Tree Diagram](assets/tree_diagram.gif)

---

## Permutation

Ways of ordering objects

$$
\begin{aligned}
    & _{n}P_{k} = P_{k}^{n} = \frac{n!}{(n-k)!}
\end{aligned}
$$

![Permutation 1](assets/permutation_1.png)

![Permutation 2](assets/permutation_2.png)

![Permutation 3](assets/permutation_3.png)

---

## Combination

Ways of choosing objects (devide **permutation** by **number of orders**)

$$
\begin{aligned}
    & _{n}C_{k} = C_{k}^{n} = {n \choose k} = \frac{n!}{k! (n-k)!} = \frac{P_{k}^{n}}{k!}
\end{aligned}
$$

![Combination 1](assets/combination_1.png)

![Combination 2](assets/combination_2.png)

![Combination 3](assets/combination_3.png)

---

## Partition

<!--TODO-->

---
