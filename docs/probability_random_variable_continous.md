# Continous Random Variable

There are many families of continous random variable distributions based on their PDF distribution shape

1. **Uniform**: Equally Likly
2. **Exponential**: Repeat Inf Times
3. **Normal**: Gaussian
4. **Gamma**: Gamma

<!--prettier-ignore-->
!!! tip "p and q"
    **p** is the probability of occurance and **q** is the probability of not occurance:

    $$
    \begin{aligned}
        & q = 1 - p
    \end{aligned}
    $$

---

## Uniform Random Variables (Equally Likly)

A random variable that gives us **a** to **b** by probability of **likly hood**

$$
\begin{aligned}
    & X \sim Unif(a, b)
    \\
    & n = b - a
    \\
    \\
    & X(x) \in [a, b]
\end{aligned}
$$

<!--prettier-ignore-->
!!! example "Example"
    1. Height example
        * **Trial**: Selecting a person
        * **Sample Space**: [Persons...]
        * **X**: Getting person height: [120, 200]

    ---

    * Probability of height to be in range from **150** to **200**:

    $$
    \begin{aligned}
        & P_X(150 \leq X \leq 200) = \int\limits_{150}^{200} \frac{1}{200 - 120} dx
        \\
        & P_X(150 \leq X \leq 200) = \frac{1}{80} . \int\limits_{150}^{200} dx
        \\
        & P_X(150 \leq X \leq 200) = \frac{1}{80} . (200 - 150)
        \\
        & P_X(150 \leq X \leq 200) = \frac{50}{80}
    \end{aligned}
    $$

    * Probability of height to be in range from **190** to **200**:

    $$
    \begin{aligned}
        & P_X(190 \leq X \leq 200) = \int\limits_{190}^{200} \frac{1}{200 - 120} dx
        \\
        & P_X(190 \leq X \leq 200) = \frac{1}{80} . \int\limits_{190}^{200} dx
        \\
        & P_X(190 \leq X \leq 200) = \frac{1}{80} . (200 - 190)
        \\
        & P_X(190 \leq X \leq 200) = \frac{10}{80}
    \end{aligned}
    $$

---

### PDF

$$
\begin{aligned}
    & f(x) = \frac{1}{n}
    \\
    & x \in [a, b]
\end{aligned}
$$

<!--prettier-ignore-->
!!! tip "Plot"
    Uniform plot have is a rectangle shape with density of **1/n**

    ![Uniform PDF](assets/continous_uniform_pdf.png)

---

### CDF

$$
\begin{aligned}
    & F(x) = P(X \leq x) = \frac{x - a}{n}
    \\
    & x \in [a, b]
\end{aligned}
$$

<!--prettier-ignore-->
!!! tip "Plot"
    Uniform plot have has cummulative density of **0** to **1**

    ![Uniform CDF](assets/continous_uniform_cdf.png)

---

### MGF

$$
\begin{aligned}
    & M(t) = \frac{e^{bt} - e^{at}}{(b - a)t}
\end{aligned}
$$

---

### Expectation

$$
\begin{aligned}
    & E(X) = \int\limits_{-\infty}^{\infty} x . f_X(x) dx
    \\
    & E(X) = \int\limits_{a}^{b} x . \frac{1}{b - a} dx
    \\
    & E(X) = \frac{1}{b - a} . \int\limits_{a}^{b} x . dx
    \\
    & E(X) = \frac{1}{b - a} . \frac{b^2 - a^2}{2}
    \\
    & E(X) = \frac{b^2 - a^2}{2.(b - a)}
    \\
    & E(X) = \frac{(b + a).(b - a)}{2.(b - a)}
    \\
    \\
    & E(X) = \frac{a + b}{2}
\end{aligned}
$$

---

### Variance

$$
\begin{aligned}
    & Var(X) = \frac{(b - a)^2}{12}
\end{aligned}
$$

---

## Exponential Random Variables (Wait Inf Times)

A random variable that gives us time waiting to win

$$
\begin{aligned}
    & X \sim Exp(\lambda)
    \\
    \\
    & X(x) = \texttt{Time for waiting to first Win}
\end{aligned}
$$

<!--prettier-ignore-->
!!! tip "Exponential vs Geometric"
    Continous exponential distribution is very similar to Discrete geometric distribution in shape and formula:

    ![Exponential vs Geometric](assets/exponential_geometric_1.png)
    ![Exponential vs Geometric](assets/exponential_geometric_2.png)

<!--prettier-ignore-->
!!! example "Example"
    1. Lamp example
        * **Trial**: Waiting for a lamp with $\lambda = 100$ to burn
        * **Sample Space**: [0, $\infty$]
        * **X**: Waiting time

    ---

    * Probability of time to be in range from **20** to **200**:

    $$
    \begin{aligned}
        & P_X(20 \leq X \leq 200) = \int\limits_{20}^{200} 100.e^{-100.x} dx
        \\
        & P_X(20 \leq X \leq 200) = 100 . \int\limits_{20}^{200} e^{-100.x} dx
        \\
        & P_X(20 \leq X \leq 200) = 100 . -\frac{1}{100} . (e^{-100*200} - e^{-20*200})
        \\
        & P_X(20 \leq X \leq 200) = -e^{\frac{-100*200}{-20*200}}
        \\
        & P_X(20 \leq X \leq 200) = -e^5
    \end{aligned}
    $$

    * Probability of height to be in range from **0** to **100**:

    $$
    \begin{aligned}
        & P_X(0 \leq X \leq 100) = \int\limits_{0}^{100} 100.e^{-100.x} dx
        \\
        & P_X(0 \leq X \leq 100) = 100 . \int\limits_{0}^{100} e^{-100.x} dx
        \\
        & P_X(0 \leq X \leq 100) = 100 . -\frac{1}{100} . (e^{-100*100} - e^{-20*0})
        \\
        & P_X(0 \leq X \leq 100) = -(e^{-10000} - e^0)
        \\
        & P_X(0 \leq X \leq 100) = -(e^{-10000} - 1)
        \\
        & P_X(0 \leq X \leq 100) = 1 - e^{-10000}
    \end{aligned}
    $$

---

### PDF

$$
\begin{aligned}
    & f(x) = \lambda . e^{-\lambda x}
    \\
    & x \in [0, \infty]
\end{aligned}
$$

<!--prettier-ignore-->
!!! tip "Plot"
    Exponential plot is like this

    ![Exponential PDF](assets/exponential_pdf.png)

---

### CDF

$$
\begin{aligned}
    & F(x) = P(X \leq x) = 1 - e^{-\lambda x}
    \\
    & x \in [0, \infty]
\end{aligned}
$$

<!--prettier-ignore-->
!!! tip "Plot"
    Exponential plot is like this

    ![Exponential CDF](assets/exponential_cdf.png)

---

### MGF

$$
\begin{aligned}
    & M(t) = \frac{\lambda}{\lambda - t}
\end{aligned}
$$

---

### Expectation

$$
\begin{aligned}
    & E(X) = \int\limits_{-\infty}^{\infty} x . f_X(x) dx
    \\
    & E(X) = \int\limits_{0}^{\infty} x . \lambda . e^{-\lambda x} dx
    \\
    \\
    & E(X) = \frac{1}{\lambda}
\end{aligned}
$$

---

### Variance

$$
\begin{aligned}
    & Var(X) = E[X^2] - (E[X])^2
    \\
    \\
    & Var(X) = \frac{1}{\lambda^2}
\end{aligned}
$$

---

## Normal Random Variables (Gaussian)

A random variable that generates datas with an specific **Mean** and **Variance**

$$
\begin{aligned}
    & X \sim N(\mu, \sigma^2)
    \\
    \\
    & X(x) = \texttt{Generated datas with mean and variance}
\end{aligned}
$$

---

### Standard Normal (Z Distribution)

The important type of the normal distribution is **N(0, 1)** or **Standard Normal Distribution** or **Z Distribution**, helps us to compute probability of other normal distributions

$$
\begin{aligned}
    & f(x) = \frac{1}{\sqrt{2\pi}} e^{-\frac{x^2}{2}}
    \\
    & x \in [-\infty, \infty]
\end{aligned}
$$

![Standard Normal Distribution](assets/standard_normal_distribution.png)

<!--prettier-ignore-->
!!! danger "Tip"
    The **CDF** of **Standard Normal Distribution** will shows by $\Phi(x)$ symbol

    $$
    \begin{aligned}
        F_X(x) = \Phi(x) = Z(x) = CDF(\texttt{Standard Normal Distribution})
    \end{aligned}
    $$

---

### Standardizing

Standardizing a random variable, means mapping it to another random variable with **Mean=0** and **Variance=1**, this tool will help us to deeply understand the datas distributions

By standardizing **Normal R.V** we have **Standard Normal R.V**, then we can find the probability

$$
\begin{aligned}
    & X = \texttt{Anything}
    \\
    \\
    & E[X] = \mu
    \\
    & Var(X) = \sigma^2
    \\
    \\
    & Y = \frac{X - \mu}{\sigma}
    \\
    \\
    & E[Y] = 0
    \\
    & Var(Y) = 1
\end{aligned}
$$

Now the **Y** is standardized

---

### PDF

$$
\begin{aligned}
    & f(x) = \frac{1}{\sigma \sqrt{2\pi}} e^{-\frac{(x - \mu)^2}{2 \sigma^2}}
    \\
    & x \in [-\infty, \infty]
\end{aligned}
$$

<!--prettier-ignore-->
!!! tip "Plot"
    Normal plot is like this

    ![Normal PDF](assets/normal_pdf.png)

---

### CDF

<!--prettier-ignore-->
!!! danger "Compute"
    There isn't any direct way to compute **CDF** of a random variable, instead, we must **Standardize** our variable into **Standard Normal R.V** the compute the **Probability** using **Standard Normal Distribution Table**

![Standard Normal Table](assets/standard_normal_table.png)

<!--prettier-ignore-->
!!! tip "Plot"
    Normal plot is like this

    ![Normal CDF](assets/normal_cdf.png)

<!--prettier-ignore-->
!!! example "Example"
    X is a random variable that similars to **N(6, 4)**, find the probability of x to be between **2** and **8**:

    $$
    \begin{aligned}
        & P(2 \leq X \leq 8) = P(2 - 6 \leq X - 6 \leq 8 - 6)
        \\
        & P(2 \leq X \leq 8) = P(\frac{2 - 6}{2} \leq \frac{X - 6}{2} \leq \frac{8 - 6}{2})
        \\
        & P(2 \leq X \leq 8) = P(-2 \leq \frac{X - 6}{2} \leq 1)
        \\
        \\
        & Y = \frac{X - 6}{2}
        \\
        \\
        & P(2 \leq X \leq 8) = P(-2 \leq Y \leq 1)
    \end{aligned}
    $$

    Now find the probability of **Y** using the **Standard Normal CDF Table**:

    $$
    \begin{aligned}
        & P(-2 \leq Y \leq 1) = P(Y \leq 1) - P(Y \leq -2)
        \\
        & P(-2 \leq Y \leq 1) = F(1) - F(-2)
    \end{aligned}
    $$

---

### MGF

$$
\begin{aligned}
    & M(t) = exp(\mu t + \frac{\sigma^2 t^2}{2})
\end{aligned}
$$

---

### Expectation

$$
\begin{aligned}
    & E(X) = \mu
\end{aligned}
$$

---

### Variance

$$
\begin{aligned}
    & Var(X) = \sigma^2
\end{aligned}
$$

---

## Gamma Random Variables

<!--TODO-->

---

## Chi-Square Random Variables (Sum Square Normal)

A random variable that is related to **Standard Normal Distribution**

If a R.V named **Z** has **Standard Normal Distribution**, then **Z^2** has the **Chi-Square Distribution** with **One degree of freedom**

$$
\begin{aligned}
    & Z_i \sim N(0,1)
    \\
    & X = Z_1^2 + Z_2^2 + \dots + Z_k^2
    \\
    \\
    & X \sim \chi^2(k)
    \\
    \\
    & k = \texttt{Degree of freedom}
\end{aligned}
$$

<!--prettier-ignore-->
!!! tip "Degree Of Freedom"
    Is the number of standard normal random variables that summated

    $$
    \begin{aligned}
        & k = 4
        \\
        \\
        & X \sim \chi^2(4) = Z_1^2 + Z_2^2 + Z_3^2 + Z_4^2
    \end{aligned}
    $$

---

### PDF

$$
\begin{aligned}
    & f(x) = \frac{x^{\frac{k}{2} - 1} . e^{-\frac{x}{2}}}{2^{\frac{k}{2}} . \Gamma(\frac{k}{2})}
    \\
    & x \geq 0
\end{aligned}
$$

<!--prettier-ignore-->
!!! tip "Plot"
    Chi-Square plot is like this

    ![Chi-Square PDF](assets/chi_square_pdf.png)

<!--prettier-ignore-->
!!! danger "Plot Shape"
    For **k > 2** the plot shape will be, similar to **Normal Distribution**

---

### CDF

<!--prettier-ignore-->
!!! danger "Compute"
    There isn't any direct way to compute **CDF** of a chi-square random variable, instead, we must use **Chi-Square Table**

![Chi-Square Table](assets/chi_square_table.png)

<!--prettier-ignore-->
!!! tip "Plot"
    Chi-Square plot is like this

    ![Chi-Square CDF](assets/chi_square_cdf.png)

---

### Expectation

$$
\begin{aligned}
    & X \sim \chi^2(k)
    \\
    \\
    & E(X) = k
\end{aligned}
$$

---

### Variance

$$
\begin{aligned}
    & X \sim \chi^2(k)
    \\
    \\
    & Var(X) = 2k
\end{aligned}
$$

---

### Goodness of fit test

<!--TODO-->
<!--https://www.youtube.com/watch?v=ZNXso_riZag-->

---

### Test for independence

<!--TODO-->
<!--https://www.youtube.com/watch?v=NTHA9Qa81R8-->

---

## Fisher Random Variables

A random variable that is related to **Chi-Square Distribution**

$$
\begin{aligned}
    & Z_1 \sim \chi^2(d_1)
    \\
    & Z_2 \sim \chi^2(d_2)
    \\
    \\
    & X = \frac{\frac{Z_1}{d_1}}{\frac{Z_2}{d_2}}
    \\
    \\
    & X \sim F(d_1,d_2)
    \\
    \\
    & d_1,d_2 = \texttt{Degrees of freedom}
\end{aligned}
$$

---

### PDF

$$
\begin{aligned}
    & f(x) = \frac{ \Gamma(\frac{d_1 + d_2}{2})(\frac{d_1}{d_2})^{\frac{d_1}{2}} . x^{\frac{d_1}{2} - 1} }{ \Gamma(\frac{d_1}{2})\Gamma(\frac{d_2}{2})(1 + \frac{d_1}{d_2}x)^{\frac{d_1 + d_2}{2}} }
    \\
    & x \geq 0
\end{aligned}
$$

<!--prettier-ignore-->
!!! tip "Plot"
    Fisher plot is like this

    ![Fisher PDF](assets/fisher_pdf.png)

<!--prettier-ignore-->
!!! danger "Plot Shape"
    For **d1 > 2** the plot shape will be, similar to **Normal Distribution**

<!--prettier-ignore-->
!!! danger "Plot Shape"
    For **d1 = d2** mode is equals to **1**

---

### CDF

<!--prettier-ignore-->
!!! danger "Compute"
    There isn't any direct way to compute **CDF** of a fisher random variable, instead, we must use **Chi-Square Table**

![Chi-Square Table](assets/chi_square_table.png)

<!--prettier-ignore-->
!!! tip "Plot"
    Fisher plot is like this

    ![Fisher CDF](assets/fisher_cdf.png)

---

### Expectation

$$
\begin{aligned}
    & X \sim F(d_1,d_2)
    \\
    \\
    & E(X) = \frac{d_2}{d_2 - 2}
\end{aligned}
$$

---

### Variance

$$
\begin{aligned}
    & X \sim F(d_1,d_2)
    \\
    \\
    & Var(X) = \frac{2d_2^2 (d_1 + d_2 - 2)}{d_1 (d_2 - 2)^2 (d_2 - 4)}
\end{aligned}
$$

---

## Student-t Random Variables (Small Normal)

**Smaller** datasets that have normal distribution called **Student-T Distribution**

$$
\begin{aligned}
    & Z \sim N(0,1)
    \\
    & Y \sim \chi^2(k)
    \\
    \\
    & X = \frac{Z}{\sqrt(\frac{Y}{k})} \sim T(k)
    \\
    \\
    & k = \texttt{Degree of freedom}
\end{aligned}
$$

---

### PDF

$$
\begin{aligned}
    & f(x) = \frac{\Gamma(\frac{k+1}{2})}{\Gamma(\frac{k}{2}) \sqrt{k \pi}} (1 + \frac{x^2}{k})^{-\frac{k+1}{2}}
    \\
    & -\infty \lt x \lt +\infty
\end{aligned}
$$

<!--prettier-ignore-->
!!! tip "Plot"
    Student-T plot is like this

    ![Student-T PDF](assets/student_t_pdf.png)

---

### CDF

<!--prettier-ignore-->
!!! danger "Compute"
    There isn't any direct way to compute **CDF** of a student-t random variable, instead, we must use **Student-T Table**

![Student-T Table](assets/student_t_table.png)

<!--prettier-ignore-->
!!! tip "Plot"
    Studnet-T plot is like this

    ![Student-T CDF](assets/student_t_cdf.png)

---

### Expectation

$$
\begin{aligned}
    & X \sim T(k)
    \\
    \\
    & E(X) = 0
\end{aligned}
$$

---

### Variance

$$
\begin{aligned}
    & X \sim T(k)
    \\
    \\
    & Var(X) = \frac{k}{k - 2}
\end{aligned}
$$

---
