# Moment Principles (μ)

Means the **average distance from a point**

![Moment Distance](assets/moment_distance.png)

Is a specific quantitative measure of the shape of a function, $\mu$

-   We use moments to describe a function shape in mathematics
-   We use moments to describe an object in physics

For example we find **mean** or **variance** function to find out the **center** of data or **distribution** of data

<!--prettier-ignore-->
!!! tip "General Formula"
    Average distance from point **c** in power of **p**, called the **p-th** moment about **c**:

    $$
    \begin{aligned}
        & \mu_p = E[(X - c)^p] = \sum\limits_{i} \frac{(x_i - c)^p}{n}
    \end{aligned}
    $$

<!--prettier-ignore-->
!!! example "Example"
    There are many general examples of moments that we used before

    1. **Raw moment**
        1. **Order 1**: Mean
    2. **Central Moment**:
        1. **Order 1**: 0
        2. **Order 2**: Variance
    3. **Standardized Moment**:
        1. **Order 1**: 0
        2. **Order 2**: 1
        3. **Order 3**: Skewness
        4. **Order 4**: Kurtosis

---

## Raw Moment

When **c** is **0**

Average distance from **0**

$$
\begin{aligned}
    & \mu_p^{'} = E[X^p] = \sum\limits_{i} \frac{x_i^p}{n}
\end{aligned}
$$

---

### Mean

The **1-th raw moment** called **mean** that is the average of values

$$
\begin{aligned}
    & \mu_1^{'} = \mu = E[X] = \sum\limits_{i} \frac{x_i}{n}
\end{aligned}
$$

![Mean](assets/distribution_measure_distribution_mean.png)

---

## Central Moment

When **c** is **Mean** or $\mu$

Average distance from **mean**

$$
\begin{aligned}
    & \mu_p = E[(X - \mu)^p] = \sum\limits_{i} \frac{(x_i - \mu_x)^p}{n}
\end{aligned}
$$

---

### Variance

The **2-th central moment** called **variance** that is the average distance of values

$$
\begin{aligned}
    & \mu_2 = \sigma^2 = Var[X] = \sum\limits_{i} \frac{(x_i - \mu_x)^2}{n}
\end{aligned}
$$

![Variance](assets/distribution_measure_distribution_variance.png)

---

## Standardized Moment

Is a central moment that is **normalized**

Standard normalized average distance from **mean**

<!--prettier-ignore-->
!!! tip "Normalization (Scaling)"
    The **normalization** is adjusting values measured on different scales

    One type of normalization is **Standard Normalization** which maps our function into **[-1,1]** range with center of **0**

    ---

    **Example**:

    $$
    \begin{aligned}
        & Normalize(X) = \frac{X - \mu}{SD(X)}
    \end{aligned}
    $$

    ![Standard Normalization](assets/standard_normalization.jpeg)

$$
\begin{aligned}
    & \tilde{\mu}_p = \frac{\mu_p}{\sigma^p} = \frac{E[(X - \mu)^p]}{\sigma^p} = \frac{\sum\limits_{i} \frac{(x_i - \mu_x)^p}{n}}{\sigma^p}
\end{aligned}
$$

---

### Skewness

The **3-th standardized moment** called skewness that shows the symmetry of a function shape, in **Left-Right**

$$
\begin{aligned}
    & \tilde{\mu_3} = \frac{\mu_3}{\sigma^3} = \frac{E[(X - \mu)^3]}{(\sqrt{E[(X - \mu)^2]})^3}
\end{aligned}
$$

![Skewness](assets/distribution_measure_distribution_skewness.png)

---

### Kurtosis

The **4-th standardized moment** called skewness that shows the symmetry of a function shape, in **Top-Down**

$$
\begin{aligned}
    & \tilde{\mu_4} = \frac{\mu_4}{\sigma^4} = \frac{E[(X - \mu)^4]}{(\sqrt{E[(X - \mu)^2]})^4}
\end{aligned}
$$

![Kurtosis](assets/distribution_measure_distribution_kurtosis.png)

---

## Mixed Moment

Is a moment using **multiple random variables**, shows the **N-Dimentional** digram of random variables **shape**

These type of moments, used to determine the **relation** between two or more **variables**

$$
\begin{aligned}
    & E[X^p] = E[X X X \dots X] = \sum\limits_{i} \frac{x_i^p}{n} = \sum\limits_{i} \frac{x_i . x_i . \dots . x_i}{n}
    \\
    & E[X Y Z \dots M] = \sum\limits_{i} \frac{x_i . y_i . z_i . \dots . m_i}{n}
    \\
    \\
    & E[(X - \mu_X)^p] = E[(X - \mu_X).(X - \mu_X).\dots.(X - \mu_X)]
    \\
    & = \sum\limits_{i} \frac{(x_i - \mu_X)^p}{n}
    \\
    & = \sum\limits_{i} \frac{(x_i - \mu_X) . (x_i - \mu_X) . \dots . (x_i - \mu_X)}{n}
    \\
    \\
    & E[(X - \mu_X).(Y - \mu_Y).\dots.(M - \mu_M)] =
    \\
    & = \sum\limits_{i} \frac{(x_i - \mu_X) . (y_i - \mu_Y) . \dots . (m_i - \mu_M)}{n}
\end{aligned}
$$

<!--prettier-ignore-->
!!! tip "The cause-and-effect relationship"
    The relationship between two variable doesn't mean the **cause-and-effect relationship**, for example:

    1. Number of hospitals
    2. Number of accidents

    These variables are correlated positively, but the **casual** of them is:

    1. Number of people

    In fact, all of them are related to this **new variable**

---

### Covariance (Relation Type)

An important **central 2-th mixed moment** called covariance that is like variance but for two random variables

$$
\begin{aligned}
    & Cov(X, Y) = E[(X - \mu_X) . (Y - \mu_Y)] = E[X . Y] - \mu_X.\mu_Y
    \\
    \\
    & \texttt{X.Y: mean of dot product of X and Y}
\end{aligned}
$$

<!--prettier-ignore-->
!!! danger "Shape"
    This moment, show the relation type of two R.V:

    1. **Positive(+)**: If **X** goes up, **Y** goes up (forward)
    2. **Zero(0)**: **X** and **Y** are unrelated
    3. **Negative(-)**: If **X** goes up, **Y** goes down (reverse)

    ![Covariance](assets/covariance_1.png)
    ![Covariance](assets/covariance_2.png)

<!--prettier-ignore-->
!!! example "Example"
    We have a dataset with two R.V's, find the relation type between these two random variables

    ![Covariance Example](assets/covariance_example.png)

    $$
    \begin{aligned}
        & Cov(ABC, XYZ) = E[(X - E[X]).(Y - E[Y])]
        \\
        & E[X] = 1.30
        \\
        & E[Y] = 3.74
        \\
        \\
        & Cov(ABC, XYZ) = \frac{(1.1 - 1.30).(3 - 3.74) + (1.7 - 1.30).(4.2 - 3.74) + \dots}{5}
        \\
        & Cov(ABC, XYZ) = 0.665
    \end{aligned}
    $$

    So **ABC** and **XYZ** are related positively, if **ABC** goes up, **XYZ** goes up, and if **ABC** goes down, **XYZ** also goes down

<!--prettier-ignore-->
!!! tip "E[X.Y] - E[X].E(Y)"
    We can simplify covariance formula:

    $$
    \begin{aligned}
        & Cov(X, Y) = E[(X - E(X)) . (Y - E(Y))]
        \\
        & Cov(X, Y) = E[X.Y - X.E(Y) - Y.E(X) + E(X).E(Y)]
        \\
        & Cov(X, Y) = E[X.Y] - E[X.E(Y)] - E[Y.E(X)] + E[E(X).E(Y)]
        \\
        & Cov(X, Y) = E[X.Y] - E[X].E(Y) - E(X).E[Y] + E(X).E(Y)
        \\
        \\
        & Cov(X, Y) = E[X.Y] - E[X].E[Y]
    \end{aligned}
    $$

<!--prettier-ignore-->
!!! tip "Cov(X, X)"
    Is equals to **Var(X)**:

    $$
    \begin{aligned}
        & Cov(X, X) = E[(X - E(X)) . (X - E(X))]
        \\
        & Cov(X, X) = E[(X - E(X))^2]
        \\
        \\
        & Cov(X, X) = Var(X)
    \end{aligned}
    $$

<!--prettier-ignore-->
!!! tip "Cov(a.X + b, Y)"
    Is equals to **a.Cov(X, Y)**:

    $$
    \begin{aligned}
        & Cov(a.X + b, Y) = E[(a.X + b - E(a.X + b)) . (Y - E(Y))]
        \\
        & Cov(a.X + b, Y) = E[((a.X + b) - (a.E(X) + b)) . (Y - E(Y))]
        \\
        & Cov(a.X + b, Y) = E[(a.X - a.E(X)) . (Y - E(Y))]
        \\
        & Cov(a.X + b, Y) = E[a.(X - E(X)) . (Y - E(Y))]
        \\
        & Cov(a.X + b, Y) = a.E[(X - E(X)) . (Y - E(Y))]
        \\
        \\
        & Cov(a.X + b, Y) = a.Cov(X, Y)
    \end{aligned}
    $$

<!--prettier-ignore-->
!!! tip "Cov(X, Y + Z)"
    Is equals to **Cov(X, Y) + Cov(X, Z)**:

    $$
    \begin{aligned}
        & Cov(X, Y + Z) = E[(X - E(X)) . ((Y + Z) - E(Y + Z))]
        \\
        & Cov(X, Y + Z) = E[(X - E(X)) . ((Y + Z) - (E(Y) + E(Z)))]
        \\
        & Cov(X, Y + Z) = E[(X - E(X)) . ((Y - E(Y)) + (Z - E(Z)))]
        \\
        & Cov(X, Y + Z) = E[(X - E(X)) . (Y - E(Y))] + E[(X - E(X)) . (Z - E(Z))]
        \\
        \\
        & Cov(X, Y + Z) = Cov(X, Y) + Cov(X, Z)
    \end{aligned}
    $$

<!--prettier-ignore-->
!!! tip "Var(X + Y)"
    We can find the covariance formula using **Var(X + Y)**, that is the gap of equality of **Var(X + Y)** and **Var(X) + Var(Y)**:

    $$
    \begin{aligned}
        & Var(X + Y) = E[( (X + Y) - (E[X + Y]) )^2]
        \\
        & Var(X + Y) = E[( (X + Y) - (E[X] + E[Y]) )^2]
        \\
        & Var(X + Y) = E[( (X - E[X]) + (Y - E[Y]) )^2]
        \\
        & Var(X + Y) = E[(X - E[X])^2 + (Y - E[Y])^2 + 2.(X - E[X]).(Y - E[Y])]
        \\
        & Var(X + Y) = E[(X - E[X])^2] + E[(Y - E[Y])^2] + E[2.(X - E[X]).(Y - E[Y])]
        \\
        & Var(X + Y) = Var(X) + Var(Y) + 2.E[(X - E[X]).(Y - E[Y])]
        \\
        \\
        & Var(X + Y) = Var(X) + Var(Y) + 2.Cov(X, Y)
    \end{aligned}
    $$

---

### Correlation Coefficient (Relation Measure)

An important **standardized 2-th mixed moment** called correlation coefficient

This moment is the standardized version of **Covariance** that show **Relation Type** and **Relation Measure** of two R.V's

$$
\begin{aligned}
    & Corr(X, Y) = \rho(X, Y) = \frac{Cov(X, Y)}{\sigma_X \sigma_Y} = \frac{E[(X - \mu_X).(Y - \mu_Y)]}{\sigma_X \sigma_Y}
\end{aligned}
$$

<!--prettier-ignore-->
!!! danger "Relation Meaning"
    Assume **X** as the **Math Ability** and **Y** as the **Musical Ability** in a dataset of persons, in the first look, these R.V's are uncorrelated, but in fact they may have a hidden relation:

    $$
    \begin{aligned}
        & X = \texttt{Math Ability}
        \\
        & Y = \texttt{Musical Ability}
        \\
        \\
        & X = Z + V
        \\
        & Y = Z + W
        \\
        \\
        & Z = \texttt{Hidden Relation}
    \end{aligned}
    $$

    Using **Mixed Moments** we can find this **Hidden Relation** `existance`, `type`, `measure`

    ![Correlation](assets/correlation.jpg)

<!--prettier-ignore-->
!!! tip "-1 <= Corr(X, Y) <= 1"
    The correlation coefficient, is always between `-1` and `+1`:

    1. `-1, 1`: Strongly correlated
         * linearly related: **X = a.Y + b**
         * $(X - E[X]) = c(Y - E[Y])$
    2. `0`: Uncorrelated

---

## Basic Rules

### Exists

For a random variable **X** if $E[|X^p|]$ goes to infinity, the **n-th** moment will not exists

$$
\begin{aligned}
    E[|X^p|] = \int\limits_{-\infty}^{+\infty} |x^p| . p_X(x) dx =
    \begin{cases}
        \infty & \texttt{n-th moment not exists}
        \\
        Constant & \texttt{n-th moment exists}
    \end{cases}
\end{aligned}
$$

---

### Previous

If **n-th** moment of a random variable exists, moments **0, 1, ..., n-1** of that variable will also exists

---

### 0-th Moment

The **0-th central moment** of a random variable is always **1**

$$
\begin{aligned}
    & \mu_0^{'} = \sum\limits_x x^0 p_X(x) = \sum\limits_x p_X(x) = 1
\end{aligned}
$$

---

## Moment Generating Function (MGF)

Is an alternative specification of a random variable probability distribution.

It can be used to compute a distribution’s moments: the **nth moment about 0** is the nth **derivative** of the moment-generating function, **evaluated at 0**.

This function does not always exist.

$$
\begin{aligned}
    & M_X(t) = E[e^{tX}]
\end{aligned}
$$

We can expansiate this function using **e^x** series:

$$
\begin{aligned}
    & e^{tX} = \sum\limits_{i=0}^{\infty} \frac{t^i X^i}{i!} = 1 + tX + \frac{t^2 X^2}{2!} + \dots + \frac{t^n X^n}{n!} + \dots
\end{aligned}
$$

So now we can find $E(e^{tX})$:

$$
\begin{aligned}
    & E[e^{tX}] = E(1) + E(tX) + E\left(\frac{t^2 X^2}{2!}\right) + \dots + E\left(\frac{t^n X^n}{n!}\right) + \dots
    \\
    & E[e^{tX}] = 1 + t.E(X) + \frac{t^2 E(X^2)}{2!} + \dots + \frac{t^n E(X^n)}{n!} + \dots
\end{aligned}
$$

By differentiating $M_X(t)$ **i** times with respect to **t** and setting **t=0** we obtain the **i-th** raw moment.

$$
\begin{aligned}
    & M_X(0) = 1
    \\
    & M_X^{(1)}(0) = E(X)
    \\
    & M_X^{(2)}(0) = E(X^2)
    \\
    & M_X^{(3)}(0) = E(X^3)
    \\
    & \dots
\end{aligned}
$$

<!--prettier-ignore-->
!!! example "Example"
    MGF of **bernouli distribution** is $1 - p + pe^t$:

    $$
    \begin{aligned}
        & X = Bernouli(p)
        \\
        \\
        & MGF(X) = 1 - p + pe^t
        \\
        & MGF^{(1)}(X) = pe^t
        \\
        & MGF^{(2)}(X) = pe^t
        \\
        & \dots
        \\
        & MGF^{(n)}(X) = pe^t
        \\
        \\
        & E(X) = MGF^{(1)}(0) = p
        \\
        & E(X^2) = MGF^{(2)}(0) = p
        \\
        & E(X^3) = MGF^{(3)}(0) = p
    \end{aligned}
    $$

<!--prettier-ignore-->
!!! tip "Tip"
    We can find **Central Moments** using **Raw Moments** like Variance formula, so using **MGF** we can find any **Moments**

    $$
    \begin{aligned}
        & Var(X) = E(X^2) = E(X)^2
    \end{aligned}
    $$

---
