# Visualize

There are two ways to visualize datasets,

1. Frequency Table
2. Frequency Chart

---

## Frequency Table

Is a table contains **summary** of our dataset

### DataSet to FrequencyTable

1. [Sort dataset](#sort-dataset) (`Quantitative`)
2. [Classify dataset](#classify-dataset)
    1. Classes Range (**R**) (`Quantitative`)
    2. Class Count (**K**)
    3. Class Length (**L**) (`Quantitative`)
3. [Find classes](#find-classes)
    1. Class Number (**i**)
    2. Class Interval (**I**) (`Quantitative`)
    3. Class Agent (**X**)
4. [Count classes](#count-classes)
    1. Frequency (**f**)
    2. Relative Frequency (**r**)
    3. Cumulative Frequency (**F**)
    4. Relative Cumulative Frequency (**G**)

![Frequency Table Create](assets/frequency_table_create.png)

---

#### Sort DataSet

Sorting our `quantiative` dataset

**Example**:

Ages of some people sample:

```code
Raw:        15, 14, 61, 52, 14, 6, 22, 52, 11, 68, 11, 67, 8, 19, 20, 12

Sorted:     6, 8, 11, 11, 12, 14, 14, 15, 19, 20, 22, 52, 52, 61, 67, 68
```

---

#### Classify DataSet

<!--prettier-ignore-->
!!! tip "Range (R)"
    Range or the space of min and max data in dataset
    
    $$
    \begin{aligned}
    & R = max(\texttt{DataSet}) - min(\texttt{DataSet})
    \end{aligned}
    $$

<!--prettier-ignore-->
!!! tip "Count (K)"
    The number of classes we want to reduce dataset, called **K**:

    1. We can classify using our custom `K` when we have a **basic knowledge about dataset**
        $$
        \begin{aligned}
        & K = \texttt{My Count}
        \end{aligned}
        $$

    2. When we doesn't know the dataset we can use **Sturges Formula** and use **dataset count** to find a good class count to classify (**n** is **`dataset length`**)
        $$
        \begin{aligned}
        & K = 1 + 3.3log(n)
        \end{aligned}
        $$

    !!! warning "Qualitative DataSet"
        For qualitative datasets we will use **count** of data **labels** or we merge some labels together

<!--prettier-ignore-->
!!! tip "Length (L)"
    Length of every class
    
    $$
    \begin{aligned}
    & L = \frac{R}{K}
    \end{aligned}
    $$

**Example**:

```code
Sorted:     6, 8, 11, 11, 12, 14, 14, 15, 19, 20, 22, 52, 52, 61, 67, 68

R = 68 - 6
K = 1 + 3.3log(16) = 4.97
L = 62 / 4

R = 62
K = 4
L = 15.5
```

---

#### Find Classes

<!--prettier-ignore-->
!!! tip "Class Number (i)"
    Is a **counter** from **1** to **n** for every class in order of sorted dataset

<!--prettier-ignore-->
!!! tip "Class Interval (I)"
    We classified our dataset using **R**, **K**, **L**, now using these parameters and starting from first dataset item we can specify our classes

    $$
    \begin{aligned}
    & Class_{1} = [a&&, a+L)
    \\
    & Class_{2} = [a+L&&, a+2L)
    \\
    & \dots
    \\
    & Class_{n} = [a+(n-1)L&&, b]
    \end{aligned}
    $$

    <!--prettier-ignore-->
    !!! danger "Select Interval"
        Selecting the class interval is **the most important** part of drawing frequency chart, because the **histogram** plotted by frequency chart is completely depends on frequency chart class intervals, and it may effect on our decision

        ![Class Interval Select](assets/class_interval_select.png)

<!--prettier-ignore-->
!!! tip "Class Agent (X)"
    **Center** of class called it's **agent**

    !!! warning "Qualitative DataSet"
        For qualitative datasets we will use **label** of data as class **agent**

**Qualiative Example**:

Teachers ratings from students of a class:

```code
Raw: Good,Good,Bad,Normal,Good,Bad,Good,Bad,Bad,Bad,Normal,Good

Sorted: Bad,Bad,Bad,Bad,Bad,Normal,Normal,Good,Good,Good,Good,Good

K = 3 -> [Bad,Normal,Good]

Class1 = Bad
Class2 = Normal
Class3 = Good
```

| Number(**i**) | Rating(**X**) |
| ------------- | ------------- |
| 1             | Bad           |
| 2             | Normal        |
| 3             | Good          |

**Quantitative Example**:

Students degrees in a class:

```code
Raw: 17.45, 18.66, 19.5, 14.5, 17.8, 14.75, 18.6, 15.5, 17.78, 11.51, 14.5, 15.4, 18.6, 17.5, 14.8, 13.3, 14.4, 15.3, 16.7, 16.8

Sorted: 11.51, 13.3, 14.4, 14.5, 14.5, 14.75, 14.8, 15.3, 15.4, 15.5, 16.7, 16.8, 17.45, 17.5, 17.78, 17.8, 18.6, 18.6, 18.66, 19.5

R = 19.5 - 11.51 = 7.99 ~ 8
K = 4
L = 8/4 = 2

Class1 = [11.51, 13.51) -> Agent: (11.51 + 13.51)/2 = 12.51
Class2 = [13.51, 15.51) -> Agent: (13.51 + 15.51)/2 = 14.51
Class3 = [15.51, 17.51) -> Agent: (15.51 + 17.51)/2 = 16.51
Class4 = [17.51, 19.51] -> Agent: (17.51 + 19.51)/2 = 18.51
```

| Number(**i**) | Interval(**I**)    | Agent(**X**) |
| ------------- | ------------------ | ------------ |
| 1             | **[11.51, 13.51)** | **12.51**    |
| 2             | **[13.51, 15.51)** | **14.51**    |
| 3             | **[15.51, 17.51)** | **16.51**    |
| 4             | **[17.51, 19.51]** | **18.51**    |

---

#### Count Classes

<!--prettier-ignore-->
!!! tip "Frequency (f)"
    Number of datas in a class i called $f_i$

    $$
    \begin{aligned}
    & f_{i} = \texttt{Frequency Class i}
    \end{aligned}
    $$

    !!! warning "Tip"
        Summation of all frequencies is equal to **n** (Number of datas)

        $$
        \begin{aligned}
        & f_{1} + f_{2} + \dots + f_{n} = n
        \end{aligned}
        $$

<!--prettier-ignore-->
!!! tip "Relative Frequency (r)"
    Percent of frequency $f_i$ called $r_i$

    $$
    \begin{aligned}
    & r_{i} = \frac{f_{i}}{n}
    \end{aligned}
    $$

    !!! warning "Tip"
        Summation of all relative frequencies is equal to **1**

        $$
        \begin{aligned}
        & r_{1} + r_{2} + \dots + r_{n} = 1
        \end{aligned}
        $$

<!--prettier-ignore-->
!!! tip "Cumulative Frequency (F)"
    Summation of number of datas in classes 1 to i called $F_i$

    $$
    \begin{aligned}
    & F_{i} = f_{1} + f_{2} + \dots + f_{i}
    \end{aligned}
    $$

    !!! warning "Tip"
        Last cumulative frequency is equal to **n**

        $$
        \begin{aligned}
        & F_{n} = n
        \end{aligned}
        $$

<!--prettier-ignore-->
!!! tip "Relative Cumulative Frequency (G)"
    Percent of cumulative frequency $F_i$ called $G_i$

    $$
    \begin{aligned}
    & G_{i} = \frac{F_{i}}{n}
    \end{aligned}
    $$

    !!! warning "Tip"
        Last relative cumulative frequency is equal to **1**

        $$
        \begin{aligned}
        & G_{n} = 1
        \end{aligned}
        $$

---

#### Frequency Table Example

Students degrees in a class:

```code
// Sort DataSet
Raw: 17.45, 18.66, 19.5, 14.5, 17.8, 14.75, 18.6, 15.5, 17.78, 11.51, 14.5, 15.4, 18.6, 17.5, 14.8, 13.3, 14.4, 15.3, 16.7, 16.8

Sorted: 11.51, 13.3, 14.4, 14.5, 14.5, 14.75, 14.8, 15.3, 15.4, 15.5, 16.7, 16.8, 17.45, 17.5, 17.78, 17.8, 18.6, 18.6, 18.66, 19.5

// Classify DataSet
R = 19.5 - 11.51 = 7.99 ~ 8
K = 4
L = 8/4 = 2

// Find Classes
Class1 = [11.51, 13.51) -> Agent: (11.51 + 13.51)/2 = 12.51
Class2 = [13.51, 15.51) -> Agent: (13.51 + 15.51)/2 = 14.51
Class3 = [15.51, 17.51) -> Agent: (15.51 + 17.51)/2 = 16.51
Class4 = [17.51, 19.51] -> Agent: (17.51 + 19.51)/2 = 18.51

// Count Classes
n = 20

// Count Classes - Frequency
f1 = length([11.51, 13.3]) = 2
f2 = length([14.4, 14.5, 14.5, 14.75, 14.8, 15.3, 15.4, 15.5]) = 8
f3 = length([16.7, 16.8, 17.45, 17.5]) = 4
f4 = length([17.78, 17.8, 18.6, 18.6, 18.66, 19.5]) = 6

f1 + f2 + f3 + f4 = 20


// Count Classes - Relative Frequency
r1 = 2/20 = 0.1
r2 = 8/20 = 0.4
r3 = 4/20 = 0.2
r4 = 6/20 = 0.3

r1 + r2 + r3 + r4 = 1


// Count Classes - Cumulative Frequency
F1 = f1 = 2
F2 = f1 + f2 = 2 + 8 = 10
F3 = f1 + f2 + f3 = 2 + 8 + 4 = 14
F4 = f1 + f2 + f3 + f4 = 2 + 8 + 4 + 6 = 20

F4 = 20


// Count Classes - Relative Cumulative Frequency
G1 = F1/20 = 0.1
G2 = F2/20 = 0.5
G3 = F3/20 = 0.7
G4 = F4/20 = 1.0

G4 = 1
```

| Number(**i**) | Interval(**I**)    | Agent(**X**) | $f_i$ | $r_i$   | $F_i$  | $G_i$   |
| ------------- | ------------------ | ------------ | ----- | ------- | ------ | ------- |
| 1             | **[11.51, 13.51)** | **12.51**    | **2** | **0.1** | **2**  | **0.1** |
| 2             | **[13.51, 15.51)** | **14.51**    | **8** | **0.2** | **10** | **0.5** |
| 3             | **[15.51, 17.51)** | **16.51**    | **4** | **0.4** | **14** | **0.7** |
| 4             | **[17.51, 19.51]** | **18.51**    | **6** | **0.3** | **20** | **1.0** |

---

### FrequencyTable to DataSet

1. Insert $\textbf{X}_i$ with $\textbf{f}_i$

#### Example

Students degrees in a class:

| Number(**i**) | Interval(**I**)    | Agent(**X**) | $f_i$ | $r_i$   | $F_i$  | $G_i$   |
| ------------- | ------------------ | ------------ | ----- | ------- | ------ | ------- |
| 1             | **[11.51, 13.51)** | **12.51**    | **2** | **0.1** | **2**  | **0.1** |
| 2             | **[13.51, 15.51)** | **14.51**    | **8** | **0.2** | **10** | **0.5** |
| 3             | **[15.51, 17.51)** | **16.51**    | **4** | **0.4** | **14** | **0.7** |
| 4             | **[17.51, 19.51]** | **18.51**    | **6** | **0.3** | **20** | **1.0** |

```code
DataSet:
12.51, 12.51
14.51, 14.51, 14.51, 14.51, 14.51, 14.51, 14.51, 14.51
16.51, 16.51, 16.51, 16.51
18.51, 18.51, 18.51, 18.51, 18.51, 18.51
```

---

## Frequency Chart

Is a diagram from [**Frequency Table**](#frequency-table)

<!--prettier-ignore-->
!!! tip "Diagram"
    Diagrams will help to learn **better** and **simpler** for people

There are multiple charts for **qualitative** or **quantitative** data:

1. [**Bar Chart**](#bar-chart)
2. [**Pie Chart**](#pie-chart)
3. [**Pareto Chart**](#pareto-chart)
4. [**Histogram**](#histogram-rectangle-chart)
5. [**Polygon**](#polygon)
6. [**Cumulative Histogram**](#cumulative-histogram)
7. [**Bell Curve**](#bell-curve)
8. [**Stem and Leaf**](#stem-and-leaf)
9. [**Box Chart**](#box-chart)

---

### Bar Chart

-   **X's**: Class Agent ($\textbf{x}_i$)
-   **Y's**: Frequency ($\textbf{f}_i$)

![Bar Chart](assets/bar_chart.png)

---

### Pie Chart

-   **Label's**: Class Agent ($\textbf{x}_i$)
-   **Thetta's**: Relative Frequency \* 360 ($\textbf{r}_i * 360$)

![Pie Chart](assets/pie_chart.png)

---

### Pareto Chart

1. **Reorder** from **highest to lowest** frequencies (Sort $\textbf{x}_i$ by $\textbf{f}_i$)
2. Plot **bar chart**
3. Add a line with a value of **Cumulative Frequency** ($\textbf{F}_i$)

![Pareto Chart](assets/pareto_chart.png)

---

### Histogram (Rectangle Chart)

-   **X's**: Class Interval ($\textbf{Class}_i$ - $\textbf{I}$)
-   **Y's**: Frequency ($\textbf{f}_i$) or Cumulative Frequency ($\textbf{F}_i$)

![Histogram](assets/histogram.png)

<!--prettier-ignore-->
!!! danger "Rectangle"
    In histogram or rectangle chart, rectangles **width** are based on **interval length**

---

### Polygon

1. Plot **histogram**
2. Connect **center** of rectangles together (**X**)

![Polygon](assets/polygon.png)

---

### Cumulative Histogram

1. **Reorder** from **highest to lowest** cumulative frequencies
2. Plot **Polygon** for **cumulative frequency** ($\textbf{F}_i$)

![Cumulative Histogram](assets/cumulative_histogram.png)

---

### Bell Curve

1. Plot **Histogram**
2. Connect **center** of rectangles together (**X**) using **curves**

![Bell Curve](assets/bell_curve.png)

---

### Stem and Leaf

1. **Sort** dataset
2. Save last digit of number as leaf and other digits as stem

![Stem And Leaf](assets/stem-and-leaf.svg)

<!--prettier-ignore-->
!!! tip "Tip"
    1. Data in this chart will **not distroyed**
    2. If data was **floating point** **multiply** them **by 10**

---

### Box Plot (Five Number Summary)

Find **min**, **Q1**, **Q2**, **Q3**, **max** and plot them using quartile in box

1. **Sort** dataset
2. Find DataSet **Range**
3. Find DataSet **Quartiles** (q1, q2(Median), q3)
4. Draw line from **X1** to **Xn**
5. Draw box from **q1** to **q3**
6. Draw cutpoint in **q2**

![Box Chart](assets/box_chart.png)

<!--prettier-ignore-->
!!! tip "Tip"
    We can find these informations from the chart:

    1. Range: R
    2. Interquintile Range: Q
    3. Quartile: q1, q2, q3
    4. Median: m
    5. Max, Min
    6. Skewness:
        1. If **q2** is closer to **q3**: Right skewness
        2. If **q2** is closer to **q1**: Left skewness

---

### Quantile Plot

1. **Sort** dataset by $\textbf{x}_i$ ascending
2. Find **n** quantiles for dataset
3. Plot chart with **(qi, xi)** points
4. (Optional) Normalize **xi** point into **qi** ranges in **[0,1]**

<!--prettier-ignore-->
!!! tip "Tip"
    Now we can get results based on **jumping** or **curvature** of the chart

---

**Example**:

$$
\begin{aligned}
    & DataSet: [1, 10, 12, 15, 17, 20, 5, 9, 3, 1]
    \\
    & SortDataSet: [1, 1, 3, 5, 9, 10, 12, 15, 17, 20]
    \\
    & n: 10
\end{aligned}
$$

| $\textbf{i}$ | $\textbf{x}_i$ | $\textbf{q}_i$ (n = 10)                        |
| ------------ | -------------- | ---------------------------------------------- |
| 1            | 1              | $q_{0.1} = \frac{1}{10} . 11 = x_{1.1} = 1$    |
| 2            | 1              | $q_{0.2} = \frac{2}{10} . 11 = x_{2.2} = 1.4$  |
| 3            | 3              | $q_{0.3} = \frac{3}{10} . 11 = x_{3.3} = 3.6$  |
| 4            | 5              | $q_{0.4} = \frac{4}{10} . 11 = x_{4.4} = 6.6$  |
| 5            | 9              | $q_{0.5} = \frac{5}{10} . 11 = x_{5.5} = 9.5$  |
| 6            | 10             | $q_{0.6} = \frac{6}{10} . 11 = x_{6.6} = 11.2$ |
| 7            | 12             | $q_{0.7} = \frac{7}{10} . 11 = x_{7.7} = 14.1$ |
| 8            | 15             | $q_{0.8} = \frac{8}{10} . 11 = x_{8.8} = 16.6$ |
| 9            | 17             | $q_{0.9} = \frac{9}{10} . 11 = x_{9.9} = 19.7$ |
| 10           | 20             |                                                |

| **X** (Quantile Percents) | **Y** (Quantile Values) |
| ------------------------- | ----------------------- |
| 0.1                       | 1                       |
| 0.2                       | 1.4                     |
| 0.3                       | 3.6                     |
| 0.4                       | 6.6                     |
| 0.5                       | 9.5                     |
| 0.6                       | 11.2                    |
| 0.7                       | 14.1                    |
| 0.8                       | 16.6                    |
| 0.9                       | 19.7                    |

![Quantile](assets/quantile_plot.png)

---

### Quantile-Quantile Plot (Q-Q Plot)

Is two **Quantile Plots** for two datasets that points are merged using $q_A = q_B$

<!--prettier-ignore-->
!!! tip "Tip"
    **Q-Q Plot** is used to determine that two datasets has **equal distributions** or not, if the Q-Q plot result shape was a **Line** they have equal distributions

    ![Quantile Quantile Result](assets/qq_plot_result.png)

---

**Example**:

| **Xa** (Quantile Percents) | **Ya** (Quantile Values) | **Xb** (Quantile Percents) | **Yb** (Quantile Values) |
| -------------------------- | ------------------------ | -------------------------- | ------------------------ |
| 0.1                        | 1                        | 0.1                        | 10                       |
| 0.2                        | 1.4                      | 0.2                        | 10.4                     |
| 0.3                        | 3.6                      | 0.3                        | 13.6                     |
| 0.4                        | 6.6                      | 0.4                        | 16.6                     |
| 0.5                        | 9.5                      | 0.5                        | 19.5                     |
| 0.6                        | 11.2                     | 0.6                        | 21.2                     |
| 0.7                        | 14.1                     | 0.7                        | 24.1                     |
| 0.8                        | 16.6                     | 0.8                        | 26.6                     |
| 0.9                        | 19.7                     | 0.9                        | 29.7                     |

| **X** (Quantile Values A) | **Y** (Quantile Values B) |
| ------------------------- | ------------------------- |
| 1                         | 10                        |
| 1.4                       | 10.4                      |
| 3.6                       | 13.6                      |
| 6.6                       | 16.6                      |
| 9.5                       | 19.5                      |
| 11.2                      | 21.2                      |
| 14.1                      | 24.1                      |
| 16.6                      | 26.6                      |
| 19.7                      | 29.7                      |

![Quantile Quantile](assets/qq_plot.png)

---

### Scatter Plot

We draw points from two or many **Data Attributes** of one **Data Object** in **2D** or **3D** or etc diagram

<!--prettier-ignore-->
!!! tip "Tip"
    **Scatter Plot** is used to determine:

    1. **Correlation**
    2. **Noises**

    ![Correlation](assets/correlation.jpg)
    ![Uncorrelated](assets/uncorrelated.jpg)

![Scatter](assets/scatter_plot_1.png)
![Scatter](assets/scatter_plot_2.jpg)
![Scatter](assets/scatter_plot_3.png)
![Scatter](assets/scatter_plot_4.jpg)

---

### Pixel Based

Showing data values using **pixel colors**

![Pixel Based](assets/pixel_based.png)

---
