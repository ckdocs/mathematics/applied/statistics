# Basics

**Probability** is the likelihood or chance of an event occurring.

<!--prettier-ignore-->
!!! example "Example"
    The probability of flipping a coin and it being heads is $\frac{1}{2}$, because there is 1 way of getting a head and the total number of possible outcomes is 2 (a head or tail). We write $P(head) = \frac{1}{2}$.

## Types

There are three main types of probabilities

### Theoretical (Classical) (Actual)

$$
\begin{aligned}
    P(A) = Pr(A) = \frac{\texttt{Event size}}{\texttt{Sample Space size}} = \frac{n(A)}{n(S)}
\end{aligned}
$$

<!--prettier-ignore-->
!!! example "Example"
    **Experiment**: Flipping dice

    **Sample Space**: `{1, 2, 3, 4, 5, 6}`

    **Event**: Getting `2 or 3`

    $$
    \begin{aligned}
        P(E) = \frac{2, 3}{1, 2, 3, 4, 5, 6} = \frac{2}{6} = \frac{1}{3}
    \end{aligned}
    $$

<!--prettier-ignore-->
!!! example "Example"
    **Experiment**: Sitting 4 couples in 8 chairs

    **Sample Space**: $\{M_1W_1M_2W_2M_3W_3M_4W_4, M_1W_1M_2W_2M_3W_4M_4W_3, ...\}$ (any combination)

    **Event 1**: Couples sits next to each other

    **Event 2**: Men sits next to each other and Women sits next to each othe

    $$
    \begin{aligned}
        & P(E1) = \frac{4!(2!)^4}{8!} = \frac{1}{150}
        \\
        & P(E2) = \frac{4!4!}{8!} = \frac{1}{70}
    \end{aligned}
    $$

---

### Experimental (Empirical) (Statistical)

$$
\begin{aligned}
    P(A) = Pr(A) = \frac{\texttt{Event occurences samples size}}{\texttt{Total occurences}}
\end{aligned}
$$

<!--prettier-ignore-->
!!! example "Example"
    **Experiment**: Flipping dice

    **Sample Space**: `{1, 2, 3, 4, 5, 6}`

    **Event**: Getting `2 or 3`

    **Statistics**: We flipped dice for 100 times and get:

    * **1**: 30 times
    * **2**: 28 times
    * **3**: 12 times
    * **4**: 10 times
    * **5**: 4 times
    * **6**: 16 times

    $$
    \begin{aligned}
        P(E) = \frac{28 + 12}{100} = \frac{40}{100} = \frac{2}{5}
    \end{aligned}
    $$

---

### Subjective

Probability is guess or estimate

<!--prettier-ignore-->
!!! example "Example"
    **Experiment**: Going to school

    **Sample Space**: `{Go, Not Go}`

    **Event**: Getting `Go`

    $$
    \begin{aligned}
        P(E) = \frac{1}{100} = \texttt{I Guess}
    \end{aligned}
    $$

---

## Probability Function (P)

Is a function of type bellow that has some rules

$$
\begin{aligned}
    P = Pr :: \mathcal{F} \rightarrow [0, 1]
\end{aligned}
$$

### Rules

1. $P(E) \geq 0$
2. $P(S) = 1$
3. $P(E_1 \cup E_2 \cup \dots) = P(E_1) + P(E_2) + \dots$ where $E_i \cap E_j = \phi$

---

### Probability Space

When we define our custom $\textbf{P}$ per sample space $\Omega$ contains a set of events (Sigma-Algebra) $\mathcal{F}$

$$
\begin{aligned}
    & \texttt{Probability Space} = (\Omega, \mathcal{F}, P)
\end{aligned}
$$

![Probability Space](assets/probability_space.png)

---

## Mathematical Expectaion (E)

Weighted mean using probabilities called **Mathematical Expectation** or **Expected Value**

$$
\begin{aligned}
    & E = \sum_{i=1}^{n} a_i . P(a_i) = a_1.P(a_1) + a_2.P(a_2) + \dots + a_n.P(a_n)
\end{aligned}
$$

<!--prettier-ignore-->
!!! example "Example"
    We have a dice with numbers of 1 to 8, find expected value:

    $$
    \begin{aligned}
        & E = 1.\frac{1}{8} + 2.\frac{1}{8} + \dots + 8.\frac{1}{8} = \frac{1 + 2 + \dots + 8}{8} = 4.5
    \end{aligned}
    $$

---

## Visualize

### Probability Tree

It helps you to map out the probabilities of many possibilities graphically

We insert probabilities of selection or occurence an event in our sample space tree diagram

<!--prettier-ignore-->
!!! example "Example"
    We have a bag with 4 white and 3 yellow balls and we want to peek two balls

    ![Pribability Tree](assets/probability_tree.png)

---

### Probability Distribution

We devide our frequencies by number of **Samples** in **Sample Space Frequency Distribution**, then we get **Probability Frequency Distribution** or **Probability Distribution** with values of probabilities

<!--prettier-ignore-->
!!! example "Example 2D"
    We have a **dice** and a **coin**, now we flip them

    1. We flip **dice**, then flip **coin**
    2. We flip **dice** and **coin** together

    Now we have **12** possible outcomes

    ![Probability Frquency Distribution 1](assets/probability_frequency_distribution_1.jpg)

<!--prettier-ignore-->
!!! example "Example 3D"
    We have a **three childs**, now we want to know their genders

    1. We get **first child gender**, then **second child gender**, then **third child gender**
    2. We get **all children genders**

    Now we have **8** possible outcomes

    ![Probability Frquency Distribution 2](assets/probability_frequency_distribution_2.jpg)

---

## Rules

There are many rules based on probability function basic rules

<!--prettier-ignore-->
!!! example "Example"
    We want to select one of 4 persons, probability of selecting first is double of second, second is double of third

    $$
    \begin{aligned}
        & P(A) + P(B) + P(C) + P(D) = 1
        \\
        \\
        & P(A) = 2*P(B)
        \\
        & P(B) = 2*P(C)
        \\
        & P(C) = P(D)
        \\
        \\
        & P(A) = \frac{4}{8}
        \\
        & P(B) = \frac{2}{8}
        \\
        & P(C) = \frac{1}{8}
        \\
        & P(D) = \frac{1}{8}
        \\
        & Max(P(A \cup B)) = P(A) + P(B) = \frac{6}{8}
        \\
        & Min(P(A{'} \cap B{'})) = P(A{'}) + P(B{'}) - 1 = \frac{2}{8}
    \end{aligned}
    $$

---

### Rule 1 (Empty)

$$
\begin{aligned}
    & P(\phi) = 0
\end{aligned}
$$

---

### Rule 2 (Union - Disjoint)

$$
\begin{aligned}
    & P(E_1 \cup E_2 \cup \dots \cup E_n) = P(E_1) + P(E_2) + \dots + P(E_n)
    \\
    & E_i \cap E_j = \phi
\end{aligned}
$$

---

### Rule 3 (Complement)

$$
\begin{aligned}
    & P(E{'}) = 1 - P(E)
    \\
    & E{'} \cup E = S
\end{aligned}
$$

---

### Rule 4 (Subtract)

$$
\begin{aligned}
    & P(A - B) = P(A) - P(A \cap B)
\end{aligned}
$$

<!--prettier-ignore-->
!!! tip "Tip"
    If $B$ is subset of $A$ then

    $$
    \begin{aligned}
        & P(A - B) = P(A) - P(B)
        \\
        & B \subseteq A
    \end{aligned}
    $$

<!--prettier-ignore-->
!!! tip "Tip"
    $$
    \begin{aligned}
        & P(B) \leq P(A)
    \end{aligned}
    $$

---

### Rule 5 (Union - Joint) (Max - Min)

$$
\begin{aligned}
    & P(A \cup B) = P(A) + P(B) - P(A \cap B)
\end{aligned}
$$

<!--prettier-ignore-->
!!! tip "Tip"
    $$
    \begin{aligned}
        & P(A \cap B) = P(A) + P(B) - P(A \cup B)
    \end{aligned}
    $$

<!--prettier-ignore-->
!!! tip "Boole's inequality - Max Probability"
    $$
    \begin{aligned}
        & P(A \cup B) \leq P(A) + P(B)
    \end{aligned}
    $$

<!--prettier-ignore-->
!!! tip "Tip - Min Probability"
    $$
    \begin{aligned}
        & P(A \cap B) \geq P(A) + P(B) - 1
    \end{aligned}
    $$

---

## Odd (Chanse)

$$
\begin{aligned}
    & Odd = \frac{P(A)}{1 - P(A)}
\end{aligned}
$$

<!--prettier-ignore-->
!!! example "Example"
    **Probability**: 0.2

    **Odd**: $\frac{0.2}{1 - 0.2} = \frac{1}{4}$

---
