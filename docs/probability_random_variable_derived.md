# Derived Distributions

## Single R.V

We can derivate our custom distribution from other standard distributions using functions

Now we can find out the PMF or PDF of the new derived distribution

$$
\begin{aligned}
    & X \sim Unif(1, 10)
    \\
    & Y = g(X)
    \\
    & Y \sim \: ?
\end{aligned}
$$

Our goal is to find the probability function of **Y**:

<!--prettier-ignore-->
!!! example "Example"
    Let **X** to be a discrete R.V, and **Y = g(X)** where:

    ![Derived Distribution](assets/derived_distribution.png)

    $$
    \begin{aligned}
        & supp(X) = \{2, 3, 4, 5\}
        \\
        & supp(Y) = \{3, 4\}
    \end{aligned}
    $$

    Now we want to find the **P(Y = 4)**:

    $$
    \begin{aligned}
        & P(Y = 4) = P(g(X) = 4)
        \\
        & P(g(X) = 4) = P(X = g^{-1}(4))
        \\
        & P(X = g^{-1}(4)) = P(X = 4, 5)
        \\
        & P(X = 4, 5) = P(X = 4) + P(X = 5)
        \\
        \\
        & P(Y = 4) = P(X = 4) + P(X = 5) = 0.3 + 0.4 = 0.7
    \end{aligned}
    $$

---

### Linear Function

We know the distribution if **X** R.V, and we know **Y = a.X + b**, what is the distribution of **Y** R.V ?

We can find it, for two modes, discrete and continous random variables:

1. **PMF**
2. **PDF**

---

#### PMF

1. Find PMF:

$$
\begin{aligned}
    & X \sim known
    \\
    & Y = a.X + b
    \\
    & P(Y = y) = P(a.X + b = y)
    \\
    & P(Y = y) = P(a.X = y - b)
    \\
    \\
    & P(Y = y) = P(X = \frac{y - b}{a})
    \\
    & p_Y(y) = p_X(\frac{y - b}{a})
\end{aligned}
$$

<!--prettier-ignore-->
!!! example "Example"
    **X** is a discrete random variable, **Y = 2X + 3**, find the probability of **P(Y = 7)**:
    
    ![Derived Linear Function PMF](assets/derived_linear_function_pmf.png)

    $$
    \begin{aligned}
        & P(Y = 7) = P(2.X + 3 = 7)
        \\
        & P(Y = 7) = P(2.X = 4)
        \\
        & P(Y = 7) = P(X = 2)
        \\
        \\
        & P(Y = 7) = \frac{3}{6}
    \end{aligned}
    $$

---

#### PDF

1. Find CDF:

$$
\begin{aligned}
    & X \sim known
    \\
    & Y = a.X + b
    \\
    & P(Y \leq y) = P(a.X + b \leq y)
    \\
    & P(Y \leq y) = P(a.X \leq y - b)
    \\
    \\
    & P(Y \leq y) = P(X \leq \frac{y - b}{a})
    \\
    & F_Y(y) = F_X(\frac{y - b}{a})
\end{aligned}
$$

2. Convert CDF to PDF:

$$
\begin{aligned}
    & F_Y(y) = F_X(\frac{y - b}{a})
    \\
    \\
    & (F(u))'= f(u).u'
    \\
    \\
    & f_Y(y).(y)' = f_X(\frac{y - b}{a}) . (\frac{y - b}{a})'
    \\
    & f_Y(y).1 = f_X(\frac{y - b}{a}) . \frac{1}{a}
    \\
    \\
    & f_Y(y) = \frac{1}{|a|} . f_X(\frac{y - b}{a})
\end{aligned}
$$

<!--prettier-ignore-->
!!! example "Example"
    **X** is a continous random variable, **Y = 2X + 3**, find the probability of **P(3 <= Y <= 5)**:
    
    ![Derived Linear Function PDF](assets/derived_linear_function_pdf.png)

    $$
    \begin{aligned}
        & P(3 \leq Y \leq 5) = P(3 \leq 2.X + 3 \leq 5)
        \\
        & P(3 \leq Y \leq 5) = P(0 \leq 2.X \leq 2)
        \\
        & P(3 \leq Y \leq 5) = P(0 \leq X \leq 1)
        \\
        \\
        & P(3 \leq Y \leq 5) = \frac{2}{3}
    \end{aligned}
    $$

<!--prettier-ignore-->
!!! tip "Negative a"
    For negative `a` we must reverse the less that symbol:

    $$
    \begin{aligned}
        & P(Y \leq y) = P(a.X \leq y - b)
        \\
        & P(Y \leq y) = P(X \geq \frac{y - b}{a})
        \\
        & P(Y \leq y) = 1 - P(X \lt \frac{y - b}{a})
        \\
        & P(Y \leq y) = 1 - P(X \leq \frac{y - b}{a})
        \\
        \\
        & F_Y(y) = 1 - F_X(\frac{y - b}{a})
        \\
        \\
        & f_Y(y) = -\frac{1}{a} . f_X(\frac{y - b}{a})
    \end{aligned}
    $$

---

#### Linear of Normal R.V

The linear function of a **Normal R.V** is another **Normal R.V**:

$$
\begin{aligned}
    & X \sim N(\mu, \sigma^2)
    \\
    & Y = aX + b
    \\
    \\
    & f_X(x) = \frac{1}{\sqrt{2.\pi}.\sigma} . e^{-\frac{(x - \mu)^2}{2.\sigma^2}}
    \\
    & f_Y(y) = \frac{1}{|a|} . f_X(\frac{y - b}{a})
    \\
    & f_Y(y) = \frac{1}{\sqrt{2.\pi}.\sigma.|a|} . e^{-\frac{((\frac{y-b}{a}) - \mu)^2}{2.\sigma^2}}
    \\
    \\
    & f_Y(y) = \frac{1}{\sqrt{2.\pi}.\sigma.|a|} . e^{-\frac{(y - b - a.\mu)^2}{2.\sigma^2.a^2}}
    \\
    \\
    & E[Y] = a \mu + b
    \\
    & Var(Y) = a^2 \sigma^2
    \\
    \\
    & Y \sim N(a \mu + b, a^2 \sigma^2)
\end{aligned}
$$

---

### General Function

We know the distribution if **X** R.V, and we know **Y = g(X)**, what is the distribution of **Y** R.V ?

We can find it, for two modes, discrete and continous random variables:

1. **PMF**
2. **PDF**

---

#### PMF

1. Find PMF:

$$
\begin{aligned}
    & X \sim known
    \\
    & Y = g(X)
    \\
    & P(Y = y) = P(g(X) = y)
    \\
    \\
    & P(Y = y) = P(X = g^{-1}(y))
    \\
    \\
    & p_Y(y) = p_X(g^{-1}(y))
    \\
    \\
    & p_Y(y) = \sum_{x: g(x) = y} p_X(x)
\end{aligned}
$$

---

#### PDF

1. Find CDF:

$$
\begin{aligned}
    & X \sim known
    \\
    & Y = g(X)
    \\
    & P(Y \leq y) = P(g(X) \leq y)
    \\
    \\
    & P(Y \leq y) = P(X \leq g^{-1}(y))
    \\
    & F_Y(y) = F_X(g^{-1}(y))
\end{aligned}
$$

2. Convert CDF to PDF:

$$
\begin{aligned}
    & F_Y(y) = F_X(g^{-1}(y))
    \\
    \\
    & (F(u))'= f(u).u'
    \\
    \\
    & f_Y(y).(y)' = f_X(g^{-1}(y)) . (g^{-1}(y))'
    \\
    & f_Y(y).1 = f_X(g^{-1}(y)) . (g^{-1}(y))'
    \\
    \\
    & f_Y(y) = (g^{-1}(y))' . f_X(g^{-1}(y))
\end{aligned}
$$

<!--prettier-ignore-->
!!! example "Example"
    **X** is a continous random variable, **Y = a/X**, find the distribution of **Y**:
    
    ![Derived General Function PDF](assets/derived_general_function_pdf.png)

    $$
    \begin{aligned}
        & P(Y \leq y) = P(\frac{a}{X} \leq y)
        \\
        & P(Y \leq y) = P(\frac{X}{a} \geq \frac{1}{y})
        \\
        & P(Y \leq y) = P(X \geq \frac{a}{y})
        \\
        & P(Y \leq y) = 1 - P(X \lt \frac{a}{y})
        \\
        & P(Y \leq y) = 1 - P(X \leq \frac{a}{y})
        \\
        \\
        & F(y) = 1 - F(\frac{a}{y})
        \\
        & f(y) = - f(\frac{a}{y}) . \frac{-a}{y^2}
        \\
        \\
        & f(y) = \frac{a}{y^2} . f(\frac{a}{y})
    \end{aligned}
    $$

---

## Multiple R.V

We can derivate our custom distribution by operating multiple R.V's with each other

Now we can find out the PMF or PDF of the new derived distribution

$$
\begin{aligned}
    & X \sim Unif(1, 10)
    \\
    & Y \sim Exp(4)
    \\
    & Z = g(X, Y)
    \\
    & Z \sim \: ?
\end{aligned}
$$

Our goal is to find the probability function of **Z**

<!--prettier-ignore-->
!!! tip "Convolution"
    Means combining to each other, we will combine two random variables to each other and create a new random variable, called **Convolution Random Variable**

    ![Convolution Random Variable](assets/convolution_random_variable.png)

    For finding distribution of these R.V's we must **Summation** over one random variable, like **Joint Probability**

    $$
    \begin{aligned}
        & P(Z = z) = P(X + Y = z) = \sum_{x \in X} P(X = x) . P(Y = z - x | X = x)
        \\
        & P(Z = z) = P(X + Y = z) = \sum_{y \in Y} P(Y = y) . P(Z = z - y | Y = y)
    \end{aligned}
    $$

---

### PMF

1. Summation over one R.V:

$$
\begin{aligned}
    & X \sim known
    \\
    & Y \sim known
    \\
    & Z = g(X, Y)
    \\
    & P(Z = z) = P(g(X, Y) = z)
    \\
    \\
    & P(Z = z) = \sum_{x \in X} P(X = x) . P(g(x, Y) = z | X = x)
    \\
    \\
    & p_Z(z) = \sum_{x \in X} p_X(x) . p_Y(z - g^{-1}(x))
\end{aligned}
$$

<!--prettier-ignore-->
!!! example "Example"
    **X** and **Y** are `independent` discrete random variables and **Z = X + Y**, find the distribution of Z

    $$
    \begin{aligned}
        & X \sim known
        \\
        & Y \sim known
        \\
        & Z = X + Y
        \\
        \\
        & P(Z = z) = P(X + Y = z)
        \\
        & P(Z = z) = \sum_{x \in X} P(X = x).P(X + Y = z | X = x)
        \\
        & P(Z = z) = \sum_{x \in X} P(X = x).P(x + Y = z)
        \\
        & P(Z = z) = \sum_{x \in X} P(X = x).P(Y = z - x)
        \\
        \\
        & p_Z(z) = \sum_{x \in X} p_X(x).p_Y(z - x)
    \end{aligned}
    $$

---

### PDF

1. Integrate over one R.V:

$$
\begin{aligned}
    & X \sim known
    \\
    & Y \sim known
    \\
    & Z = g(X, Y)
    \\
    & P(Z = z) = P(g(X, Y) = z)
    \\
    \\
    & P(Z = z) = \int_{-\infty}^{+\infty} P(X = x) . P(g(x, Y) = z | X = x) dx
    \\
    \\
    & f_Z(z) = \int_{-\infty}^{+\infty} f_X(x) . f_Y(z - g^{-1}(x)) dx
\end{aligned}
$$

<!--prettier-ignore-->
!!! example "Example"
    **X** and **Y** are `independent` continous random variables and **Z = X + Y**, find the distribution of Z

    $$
    \begin{aligned}
        & X \sim known
        \\
        & Y \sim known
        \\
        & Z = X + Y
        \\
        \\
        & P(Z = z) = P(X + Y = z)
        \\
        & P(Z = z) = \int_{-\infty}^{+\infty} P(X = x).P(X + Y = z | X = x) dx
        \\
        & P(Z = z) = \int_{-\infty}^{+\infty} P(X = x).P(x + Y = z) dx
        \\
        & P(Z = z) = \int_{-\infty}^{+\infty} P(X = x).P(Y = z - x) dx
        \\
        \\
        & f_Z(z) = \int_{-\infty}^{+\infty} f_X(x).f_Y(z - x) dx
    \end{aligned}
    $$

---

### Multiple of Normal R.V

The addition of two **Normal R.V** is another **Normal R.V**, we can find it using **PDF** multiple R.V integration

$$
\begin{aligned}
    & X \sim N(\mu_x, \sigma_x^2)
    \\
    & Y \sim N(\mu_y, \sigma_y^2)
    \\
    & Z = X + Y
    \\
    \\
    & f_X(x) = \frac{1}{\sqrt{2.\pi}.\sigma_x} . e^{-\frac{(x - \mu_x)^2}{2.\sigma_x^2}}
    \\
    & f_Y(y) = \frac{1}{\sqrt{2.\pi}.\sigma_y} . e^{-\frac{(y - \mu_y)^2}{2.\sigma_y^2}}
    \\
    \\
    & f_Z(z) = \int_{-\infty}^{+\infty} f_X(x) . f_Y(z - y) dx
    \\
    & f_Z(z) = \frac{1}{\sqrt{2.\pi.(\sigma_x^2 + \sigma_y^2)}} . exp\left( -\frac{(z - \mu_x - \mu_y)^2}{2.(\sigma_x^2 + \sigma_y^2)} \right)
    \\
    \\
    & Z \sim N(\mu_x + \mu_y, \sigma_x^2 + \sigma_y^2)
\end{aligned}
$$

<!--prettier-ignore-->
!!! tip "Infinite Normal R.V"
    So we can add infinite number of normal random variables or any **linear combination** of them, and the result is a new normal random variable

---

## Simulation

Sometimes we want to generate random samples using a **Distribution**, we will create a **Random Generator** with our target R.V distribution

Assume we already have the **Unif(0,1)** continous random generator, then we can convert these random samples to our target distribution

For creating our custom random generator using **Unif(0,1)** random generator:

1. Init **Unif(0,1)** random generator
2. Find **CDF** our target distribution
3. Find **CDF^(-1)** our target distribution
4. Target random generator is **CDF(-1) (Unif(0,1))**

![Random Generator CDF](assets/random_generator_cdf.png)

<!--prettier-ignore-->
!!! example "Example"
    Create a random generator with distribution of **Exp(1)**:

    $$
    \begin{aligned}
        & X \sim exp(1)
        \\
        & F_X(x) = 1 - e^{-x}
        \\
        & - e^{-x} = F_X(x) - 1
        \\
        & e^{-x} = 1 - F_X(x)
        \\
        & -x = Ln(1 - F_X(x))
        \\
        & x = - Ln(1 - F_X(x))
        \\
        \\
        & RNG = - Ln(1 - Unif(0,1))
    \end{aligned}
    $$

---
