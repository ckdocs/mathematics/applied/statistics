# Data

<!--prettier-ignore-->
!!! warning "What is data ?"
    Data refers to **facts** and **statistics** collected together for `reference` or `analysis`

<!--prettier-ignore-->
!!! tip "Variable"
    A property of a population we are focusing for collecting called a variable
    
    **Example**: `Age` is a variable in the `class` population

**Example**:

1. Set of numbers of 100 peoples `age`:

```code
17,17,20,40,41,40,13,43,15,...
```

2. Set of `positions of clicks` on a phone `touch screen`:

```code
(10,10), (11,30), (10,30), (11,31), (50,1), ...
```

---

## Concepts

There are there main concepts in data that have many names

![Data Table](assets/data_table.png)

1. **Data Set**

    1. Samples
    2. Sample Set
    3. Database
    4. Collection
    5. Data Table

2. **Data Object**

    1. Record
    2. Object
    3. Sample
    4. Tuple
    5. Data
    6. Graph
    7. Ordered
    8. Spatial
    9. Data Point
    10. Instance
    11. Example

3. **Data Attribute**

    1. Field
    2. Column
    3. Attribute
    4. Dimension
    5. Feature
    6. Variable
    7. Random Variable

---

### Data Set (Database)

Is the collection of data objects or records

There are many names equals to data set:

1.  **Samples**
2.  **Sample Set**
3.  **Database**
4.  **Collection**
5.  **Data Table**
6.  **Matrix**

There are many types of data sets like:

1. **Record Based**: SQL Server
    - Data objects will store in **record**, **matrix**, **vector(text sequence)**, **transacttion**
2. **Graph Based**: Neo4j
    - Data objects will store in **node**, **relation**
3. **Order Based**: TimeDB, Redis
    - Data objects will store in **order series**, like video (ordered pictures)
4. **Spatial Based**: Pictures, Maps
    - Data objects will store in **binary**, **matrix**, like maps and pictures

---

### Data Object (Record)

One data in dataset called data object or record

There are many names equals to data object:

1.  **Record**
2.  **Object**
3.  **Sample**
4.  **Tuple**
5.  **Data**
6.  **Graph**
7.  **Ordered**
8.  **Spatial**
9.  **Data Point**
10. **Instance**
11. **Example**
12. **Vector**

---

#### Dimension

Every record has some **columns**

We must always try to **reduce** dimension of our data to:

1. **Lower space usage**
2. **More speed to process**
3. **Clear data**

---

#### Sparsity

Sparse means data with null fields

We must always try to remove **null** fields in our data to:

1. **Lower space usage**
2. **More speed to process**

---

#### Distribution

We must find the distribution of our data

---

### Data Attribute (Column/Field)

Every data has one or many columns or fields, or function of fields

There are many names equals to data attribute:

1.  **Field** (Database)
2.  **Column** (Database)
3.  **Attribute** (Data Mining)
4.  **Dimension** (Dataware House)
5.  **Feature** (Machine Learning)
6.  **Variable** (Statistics)
7.  **Random Variable** (Statistics)

Data attributes are **functions**

---

#### Label

Some data attributes which shows the data object **category name** called **labels**

Labels are used in **Supervised Learning** algorithms

<!--prettier-ignore-->
!!! danger "Supervisor"
    The **label** values must filled by a **Supervisor** like a human

<!--prettier-ignore-->
!!! example "Example"
    We have a table with this schema:

        ```sql
        CREATE TABLE Student(
            id: string
            name: string
            type: "good" | "middle" | "bad"
        )
        ```

    The **type** attribute is a **label** and we classify students based on **type** into three classes (good, middle, bad)

---

#### Types

Every table column has a type (int, float, char, ...), our dataset columns also has types

There are `two` main types of datas

![Data Types](assets/data_types.png)

---

##### Qualitative (String)

Qualitative data deals with **characteristics** and **descriptors** that can't be easily measured, but can be **observed** subjectively

---

###### Nominal (String)

Data without any `order` and `rank`

**Example**:

<!--prettier-ignore-->
!!! tip "Genders"
    * Male
    * Female

    ![Ordinal Data](assets/qualitative_nominal_data.png)

<!--prettier-ignore-->
!!! warning "Central Tendency"
    These types of data has these central tendencies:

    1. **Mean**: False
    2. **Median**: False
    3. **Mode**: True

---

###### Ordinal (Enumerate)

Data with an `order`

**Example**:

<!--prettier-ignore-->
!!! tip "Ratings"
    1. Good
    2. Average
    3. Bad

    ![Ordinal Data](assets/qualitative_ordinal_data.png)

<!--prettier-ignore-->
!!! warning "Central Tendency"
    These types of data has these central tendencies:

    1. **Mean**: False
    2. **Median**: True
    3. **Mode**: True

---

###### Binary (Bit)

Can hold `0` or `1`

**Example**:

<!--prettier-ignore-->
!!! tip "State of a lock"
    {0, 1}

    ![Binary Data](assets/quantitative_binary_data.png)

<!--prettier-ignore-->
!!! warning "Central Tendency"
    These types of data has these central tendencies:

    1. **Mean**: False
    2. **Median**: False
    3. **Mode**: True

---

##### Quantitative (Number)

Quantitative data deals with **numbers** and things you can **measure** objectively

<!--prettier-ignore-->
!!! tip "Ratio vs Interval"
    There are two types of numbers, **ratio** that has a **Zero Point** and **interval** that are **Positive or Negative**

    1. **Ratio**: $[0, +\infty)$
        1. Kelvin temperature
        2. Persons height
    2. **Interval**: $(-\infty, +\infty)$
        1. Celsius temperature
        2. Fahrenheit temperature
        3. Current date

    We can compare **ratio** and **interval** numbers, but we can multiplying ratio numbers (10 is 5 times to 2)

---

###### Discrete (Integer)

A `categorical` data that can hold `finite` number of possible values

We can find a **map** between our data category and **N** numbers

**Example**:

<!--prettier-ignore-->
!!! tip "Numbers of students in a class"
    [0,1,2,3,...,n]

    ![Discrete Data](assets/quantitative_discrete_data.png)

<!--prettier-ignore-->
!!! warning "Central Tendency"
    These types of data has these central tendencies:

    1. **Mean**: True
    2. **Median**: True
    3. **Mode**: True

---

###### Continous (Floating Point)

Can hold `infinine` number of possible values

**Example**:

<!--prettier-ignore-->
!!! tip "Weight of a person"
    [0-n]

    ![Discrete Data](assets/quantitative_continous_data.png)

<!--prettier-ignore-->
!!! warning "Central Tendency"
    These types of data has these central tendencies:

    1. **Mean**: True
    2. **Median**: True
    3. **Mode**: True

---

## Operations

![Data Operations](assets/data_operations.png)

### Collect

We can collect data from any source:

1. `Questionnaire` from people
2. `Crawling` a website
3. `Logs` from an application

### Store

We can store data to any destination:

1. `Database`
2. `Excel`
3. `Paper`

### Measure & Analyze

We can measure and analyze our data using **comparing** and **parameters** using these methods:

1. [`Frequency Table`](#frequency-table)

### Visualize

We can visualize our data using **charts**:

1. [`Frequency Chart`](#frequency-chart)

---
