# Intro

Descriptive statistics is a method used to describe and understand the features of a specific dataset by giving short summaries about the sample and measures of the data.

## Measures

Numbers showing the attributes of dataset, there are there main types of measures:

1. **Central Tendency**: Mean, Median, Mode, ...
2. **Dispersion Tendency**: Variance, StandardDeviation, ...
3. **Distribution Tendency**: Skewness, Kurtosis, ...

---
