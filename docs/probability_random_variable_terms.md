# Terms

There are many basic and important terms for every random variable, like statistic data

---

## Support (supp)

Subset of domain that **f(x)** is not zero:

$$
\begin{aligned}
    & supp(f) = \{x \in X | f(x) \neq 0\}
\end{aligned}
$$

<!--prettier-ignore-->
!!! tip "Function"
    We define functions in mathematics

    ![Function](assets/function.png)

<!--prettier-ignore-->
!!! example "Example"
    We have a random variable named **X** where:

    **X**: Sum of two dice that we toss

    $$
    \begin{aligned}
        & supp(X) = \{2, 3, \dots, 10, 11, 12\}
    \end{aligned}
    $$

---

## Mode (M)

The data with the most number of repetitions called **Mode**, if we have a random variable the max value of **PMF** called it's mode

$$
\begin{aligned}
    & M(PMF) = max(PMF)
\end{aligned}
$$

<!--prettier-ignore-->
!!! example "Example"
    1. Tossing two dices and getting sum of numbers
        * **Sample Space**: Any possible outcome of two dice object
        * **X**: Map **dice object** into **two number**
        * **Y**: Map **two number** into **sum of them**
        * **M(Y)**: 7

    ---

    ![Random Variable](assets/probability.png)

---

## Median (m)

The central data called **Median**, that the probabiliy of lower half equals the probability of higher half

$$
\begin{aligned}
    & P(X \geq m) = P(X \leq m)
\end{aligned}
$$

<!--prettier-ignore-->
!!! example "Example"
    1. Tossing two dices and getting sum of numbers
        * **Sample Space**: Any possible outcome of two dice object
        * **X**: Map **dice object** into **two number**
        * **Y**: Map **two number** into **sum of them**
        * **m(Y)**: 7

    ---

    ![Random Variable](assets/probability.png)

---

## Mean (Expected Value) (E)

Or **expectation** or **mean of random variable** ($\mu_{X}$), is the value that we want to occure in average

Is the weighted average of **Random Variable Values** and **PMF** or **PDF**, so we can apply weighted mean rules over expectation

$$
\begin{aligned}
    & E(X) = \mu_{X} =
    \begin{cases}
        \sum\limits_{x} x . p_X(x) & \texttt{Discrete}
        \\
        \int\limits_{-\infty}^{\infty} x . f_X(x) dx & \texttt{Continous}
    \end{cases}
\end{aligned}
$$

![Expected Value](assets/expected_value.png)

---

### Basic Rules

Mathematical expectation have three basic rules

<!--prettier-ignore-->
!!! tip "Non-Negative"
    $$
    \begin{aligned}
        & X \geq 0 \Rightarrow E(X) \geq 0
    \end{aligned}
    $$

    ---

    Proof:

    $$
    \begin{aligned}
        & X \geq 0
        \\
        & \Rightarrow \forall x \in X : x \geq 0
        \\
        & \Rightarrow P_X(x) \geq 0
        \\
        & \Rightarrow \sum\limits_{x} x . P_X(x) \geq 0
        \\
        & \Rightarrow E(X) \geq 0
    \end{aligned}
    $$

<!--prettier-ignore-->
!!! tip "Between"
    $$
    \begin{aligned}
        & a \geq X \geq b \Rightarrow a \geq E(X) \geq b
    \end{aligned}
    $$

    ---

    Proof:

    $$
    \begin{aligned}
        & a \geq X \geq b
        \\
        & \Rightarrow \forall x \in X : a \geq x \geq b
        \\
        & \Rightarrow 1 \geq P_X(x) \geq 0
        \\
        & \Rightarrow \sum\limits_{x} x . P_X(x) \geq 0
        \\
        & \Rightarrow a \geq E(X) \geq b
    \end{aligned}
    $$

<!--prettier-ignore-->
!!! tip "Constant"

    $$
    \begin{aligned}
        & X = c \Rightarrow E(X) = c
    \end{aligned}
    $$

---

### Mean Rule - E(E(X))

We have mean rule for means

$$
\begin{aligned}
    & E(E(X)) = E(X)
\end{aligned}
$$

Proof:

$$
\begin{aligned}
    & E(E(X)) = \sum\limits_{x} E(X) . p_X(x)
    \\
    & E(E(X)) = \sum\limits_{x} E(X) . \frac{1}{n}
    \\
    & E(E(X)) = \frac{n . E(X)}{n}
    \\
    & E(E(X)) = E(X)
\end{aligned}
$$

---

### Function Rule - E(g(X))

Is a rule for calculating $E(g(X))$ using $E(X)$

$$
\begin{aligned}
    & E(g(X)) =
    \begin{cases}
        \sum\limits_{x} g(x) . P_X(x) & \texttt{Discrete}
        \\
        \int\limits_{-\infty}^{\infty} g(x) . f_X(x) dx & \texttt{Continous}
    \end{cases}
\end{aligned}
$$

We define **Y** from **X** by applying a function over it

$$
\begin{aligned}
    & Y = g(X)
\end{aligned}
$$

![Function Rule](assets/expectation_function_rule_example.png)

We want to find $E(Y)$, we can use standard formula of expectation for random variable **Y**

$$
\begin{aligned}
    & E(Y) = \sum\limits_{y} y . P_Y(y)
    \\
    & E(Y) = 4 . (0.4 + 0.3) + 3 . (0.2 + 0.1) = 3.7
\end{aligned}
$$

But we can convert it to $E(X)$

$$
\begin{aligned}
    & E(g(X)) = \sum\limits_{x} g(x) . P_X(x)
    \\
    & E(g(X)) = E(Y) = g(5).0.4 + g(3).0.3 + g(3).0.2 + g(2).0.1 = 3.7
\end{aligned}
$$

---

### Linearity Rule - E(aX + b)

We have linearity rule for means

$$
\begin{aligned}
    & E(aX + b) = a.E(X) + b
\end{aligned}
$$

Proof:

$$
\begin{aligned}
    & Y = g(X) = aX + b
    \\
    & E(Y) = \sum\limits_{x} g(x) . P_X(x)
    \\
    & E(Y) = \sum\limits_{x} (a.x + b) . P_X(x)
    \\
    & E(Y) = \sum\limits_{x} (a.x) . P_X(x) + \sum\limits_{x} b . P_X(x)
    \\
    & E(Y) = a . \sum\limits_{x} x . P_X(x) + b . \sum\limits_{x} P_X(x)
    \\
    & E(Y) = a . E(X) + b
\end{aligned}
$$

---

### Abstract Conditional Rule - E(X | Y)

We can rewrite the conditional expectation formula in an abstract way, that generates values as a new random variable

$$
\begin{aligned}
    & E(X | Y = y) = value
    \\
    \\
    & E(X | Y) = [(value_1 | Y = y_1), (value_2 | Y = y_2), \dots]
\end{aligned}
$$

<!--prettier-ignore-->
!!! tip "Law Of Total Expectation"
    We can rewrite this formula in abstract way

    $$
    \begin{aligned}
        & E[E[X | Y]] = E[X]
    \end{aligned}
    $$

---

## Variance (Var)

Average distance from the mean called **Variance** ($X - \mu_{X}$) or $\sigma_{X}^{2}$

Is a measure of spread of a **PMF**

$$
\begin{aligned}
    & Var(X) = E[(X - \mu_{X})^2]
    \\
    & \mu_{X} = E(X)
\end{aligned}
$$

We can simplify this formula using expectation **function rule**:

$$
\begin{aligned}
    & Var(X) = \sigma_{X}^{2} = E(g(X)) = \sum\limits_{x} g(x) . P_X(x) = \sum\limits_{x} (x - \mu)^2 . P_X(x)
    \\
    & g(X) = (X - \mu) ^ 2
\end{aligned}
$$

---

### Mean Rule - Var(E(X))

We have mean rule for variances

$$
\begin{aligned}
    & Var(E(X)) = \frac{Var(X)}{n}
\end{aligned}
$$

Proof:

$$
\begin{aligned}
    & Var(E(X)) = Var(\frac{X_1 + X_2 + \dots + X_n}{n})
    \\
    & Var(E(X)) = \frac{n . Var(X)}{n^2}
    \\
    & Var(E(X)) = \frac{Var(X)}{n}
\end{aligned}
$$

---

### Linearity Rule - Var(aX + b)

We have linearity rule in this form for variances

$$
\begin{aligned}
    & Var(aX + b) = a^2.Var(X)
\end{aligned}
$$

Proof: Part-1:

$$
\begin{aligned}
    & Y = X + b
    \\
    & E(X) = \mu_X = \dots
    \\
    & E(Y) = \mu_Y = E(X + b) = E(X) + b = \mu_X + b
    \\
    \\
    & Var(X) = E[(X - \mu_X)^2]
    \\
    & Var(Y) = E[(Y - \mu_Y)^2]
    \\
    & Var(Y) = E[((X + b) - (\mu_X + b))^2]
    \\
    & Var(Y) = E[(X - \mu_X)^2]
    \\
    & Var(Y) = Var(X)
\end{aligned}
$$

Proof: Part-2:

$$
\begin{aligned}
    & Y = a.X
    \\
    & E(X) = \mu_X = \dots
    \\
    & E(Y) = \mu_Y = E(a.X) = a.E(X) = a.\mu_X
    \\
    \\
    & Var(X) = E[(X - \mu_X)^2]
    \\
    & Var(Y) = E[(Y - \mu_Y)^2]
    \\
    & Var(Y) = E[((a.X) - (a.\mu_X))^2]
    \\
    & Var(Y) = E[a^2.(X - \mu_X)^2]
    \\
    & Var(Y) = a^2.E[(X - \mu_X)^2]
    \\
    & Var(Y) = a^2.Var(X)
\end{aligned}
$$

---

### Expectation Rule - E(X^2) - E(X)^2

We can simplify variance formula using expectations

$$
\begin{aligned}
    & Var(X) = E(X^2) - E(X)^2
\end{aligned}
$$

Proof:

$$
\begin{aligned}
    & Var(X) = E[(X - \mu)^2] = E[X^2 - 2X\mu + \mu^2]
    \\
    & Var(X) = E(X^2) - E(2X\mu) + E(\mu^2)
    \\
    & Var(X) = E(X^2) - 2\mu.E(X) + \mu^2
    \\
    & Var(X) = E(X^2) - 2.E(X).E(X) + E(X)^2
    \\
    & Var(X) = E(X^2) - E(X)^2
\end{aligned}
$$

---

### Abstract Conditional Rule - Var(X | Y)

We can rewrite the conditional variance formula in an abstract way, that generates values as a new random variable

$$
\begin{aligned}
    & Var(X | Y = y) = value
    \\
    \\
    & Var(X | Y) = [(value_1 | Y = y_1), (value_2 | Y = y_2), \dots]
\end{aligned}
$$

<!--prettier-ignore-->
!!! tip "Law Of Total Variance"
    We can rewrite this formula in abstract way

    $$
    \begin{aligned}
        & Var(X) = E[Var(X | Y)] + Var(E[X | Y])
    \end{aligned}
    $$

---

## Standard Deviation (SD)

The variance unit is square, for example if **X** has unit of **Meter**, the **Var(X)** has unit of **Meter^2**, we can compute square root of variance called **Standard Deviation** or $\sigma_X$ that has unit of **Meter**

$$
\begin{aligned}
    & SD(X) = \sigma_X = \sqrt{Var(X)}
\end{aligned}
$$

---

## Schwarz Inequality

If **X** and **Y** has a finite 2-th moment then:

$$
\begin{aligned}
    & E[XY]^2 \leq E[X^2] . E[Y^2]
\end{aligned}
$$

This iniquality will be equality if and only if: **For existed `a`, P(X = aY) = 1**

$$
\begin{aligned}
    & E[XY]^2 = E[X^2] . E[Y^2]
    \\
    & P(X = aY) = 1
\end{aligned}
$$

---

## Chebyshev Inequality

Is an important inequality, used to recognize better the probability distributions

If a probability distribution of a random variable is **normal** we can easily analyze it, else we most try to convert that distribution to a normal distribution

![Normal Abnormal Distributions](assets/normalـabnormalـdistribution.png)
![Abnormal Distributions](assets/abnormal_distribution.png)

Chebyshev inequality can help us to recognize any probability distributions

It will make a relation between **Probability** and **Mean** and **Variance** to find a probability of a random variable to exist on a specific range from it's mean

![Six Sigma](assets/sixـsigma.png)

$$
\begin{aligned}
    & P(|X - \mu| < k\sigma) \geq 1 - \frac{1}{k^2}
    \\
    \\
    & P(|X - \mu| < k\sigma) = P(-k\sigma < X - \mu < +k\sigma)
    \\
    & P(|X - \mu| < k\sigma) = P(-k\sigma + \mu < X < +k\sigma + \mu)
\end{aligned}
$$

Probability of **X** to be in range $\textbf{k}\sigma$ from **Mean** or $\mu$

<!--prettier-ignore-->
!!! example "Example"
    We toss a coin 400 times, what is the probability of number of heads to be in range of 140 and 260 ?

    $$
    \begin{aligned}
        & X = Binomial(n = 400, p = \frac{1}{2})
        \\
        & \mu = np = 200
        \\
        & \sigma^2 = np(1-p) = 100
        \\
        & \sigma = 10
        \\
        \\
        & P(|X - \mu| < k\sigma) > 1 - \frac{1}{k^2}
        \\
        & k = 6
        \\
        & P(|X - 200| < 40) > 1 - \frac{1}{36}
        \\
        & P(160 < X < 240) > \frac{35}{36}
    \end{aligned}
    $$

---

## Cumulant (K)

<!--TODO-->

---

## Combinant (G)

<!--TODO-->

---

## Characteristic Function

<!--TODO-->

---
