# Central Tendency

Measure of **central** is used to describe the central point in a sample or population

1. **Qualitative** DataSet: `Compare Distribution Table`
2. **Quantitative** DataSet: `Measure of Central`

![Central Tendency](assets/distribution_measure_central_tendency.png)

<!--prettier-ignore-->
!!! warning "Central Tendency using Frequency Table"
    For computing measures using frequency tables, we most convert our **Frequency Table To DataSet** using $\textbf{f}_i$ and $\textbf{X}_i$, then use that dataset

![Centeral Tendency Data Types](assets/centeral_tendency_data_types.png)

## Mean (Qunatitative)

Or **Expected Value** or **Mathematical Expectation**

Measure of average of all the values in a sample

<!--prettier-ignore-->
!!! tip "Compare Means"
    If values were **positive**, we can compare our means:

    $$
    \begin{aligned}
    & \overline{\textbf{X}} > \textbf{G} > \textbf{H}
    \end{aligned}
    $$

    ---

    **Example**:

    $$
    \begin{aligned}
    & \textbf{DataSet}: 1,2,3,\dots,19,20
    \newline
    \newline
    & \overline{\textbf{X}}: 10.5
    \newline
    & \textbf{G}: 8.3
    \newline
    & \textbf{H}: 5.56
    \end{aligned}
    $$

<!--prettier-ignore-->
!!! danger "Dependency"
    Mean measure is highly dependent on outlier datas, so if we have a **one big** or **one small** data in our dataset, mean is not an accurate measure for central tendency.

---

### Arithmetic (**X**)

Arithmetic mean or $\overline{\textbf{X}}$

$$
\begin{aligned}
& \overline{X} = E[X] = \frac{x_1 + x_2 + \dots + x_n}{n} = \frac{ \sum\limits_{i=1}^n x_i }{n}
\end{aligned}
$$

<!--prettier-ignore-->
!!! example "Example"
    $$
    \begin{aligned}
    & \textbf{DataSet}: 1,5,1,5,6,1,4,2,6,7,7,12
    \newline
    \newline
    & \textbf{Mean}: \frac{1 + 5 + 1 + 5 + 6 + 1 + 4 + 2 + 6 + 7 + 7 + 12}{12} = 4.75
    \end{aligned}
    $$

<!--prettier-ignore-->
!!! warning "Mean of Mean"
    Mean of series **A** with number of **m** is $\overline{\textbf{X}}$ and mean of series **B** with number of **n** is $\overline{\textbf{Y}}$, mean of the sum of these series equals:

    $$
    \begin{aligned}
    & \textbf{Mean}: \frac{m\overline{X} + n\overline{Y}}{m + n}
    \end{aligned}
    $$

---

### Trimmed (**X**)

Trimmed mean or $\overline{\textbf{X}}$

1. Remove **big** or **small** values from dataset
2. **Arithmetic** mean

<!--prettier-ignore-->
!!! tip "Algorithm"
    Algorithm:

    1. Sort dataset
    2. Remove top, bottom **2%** of datas (clear outlier data)
    3. Mean

<!--prettier-ignore-->
!!! example "Example"
    $$
    \begin{aligned}
    & \textbf{DataSet}: 1,5,1,5,6,1,4,2,6,7,7,12,100,310
    \newline
    \newline
    & \textbf{Trimmed DataSet}: 1,5,1,5,6,1,4,2,6,7,7,12
    \newline
    \newline
    & \textbf{Mean}: \frac{1 + 5 + 1 + 5 + 6 + 1 + 4 + 2 + 6 + 7 + 7 + 12}{12} = 4.75
    \end{aligned}
    $$

---

### Weighted (**Xw**)

Weighted mean or $\overline{\textbf{X}}_w$

We have a weight $w_i$ per every value $x_i$

$$
\begin{aligned}
& \overline{X}_w = \frac{w_1x_1 + w_2x_2 + \dots w_nx_n}{w_1 + w_2 + \dots + w_n} = \frac{ \sum\limits_{i=1}^n w_ix_i }{ \sum\limits_{i=1}^n w_i }
\end{aligned}
$$

<!--prettier-ignore-->
!!! example "Example"
    $$
    \begin{aligned}
    &(x_i,w_i)
    \newline
    \newline
    & \textbf{DataSet}: (1,2),(2,2),(5,1),(3,1)
    \newline
    \newline
    & \textbf{Mean}: \frac{1*2 + 2*2 + 5*1}{2+2+1} = 2.2
    \end{aligned}
    $$

---

### Geometric (**G**)

Geometric mean or $\textbf{G}$

$$
\begin{aligned}
& G = \sqrt[n]{x_1 . x_2 \dots x_n} = \sqrt[n]{ \prod\limits_{i=1}^n x_i }
\end{aligned}
$$

<!--prettier-ignore-->
!!! example "Example"
    $$
    \begin{aligned}
    & \textbf{DataSet}: 0.1, 0.2, 0.3
    \newline
    \newline
    & \textbf{Mean}: \sqrt[3]{0.1 . 0.2 . 0.3} = 0.1817
    \end{aligned}
    $$

<!--prettier-ignore-->
!!! warning "Geometric vs Arithmetic"
    Relation between $\textbf{G}$ and $\overline{\textbf{X}}$ is:

    $$
    \begin{aligned}
    & ln(G) = \frac{1}{n} \sum\limits_{i=1}^n ln(x_i) = \overline{ln(x_i)}
    \end{aligned}
    $$

---

### Harmonic (**H**)

Harmonic mean or $\textbf{H}$ is for **Positive** values

$$
\begin{aligned}
& H = \frac{n}{ \frac{1}{x_1} + \frac{1}{x_2} + \dots + \frac{1}{x_n} } = \frac{n}{ \sum\limits_{i=1}^n \frac{1}{x_i} }
\end{aligned}
$$

<!--prettier-ignore-->
!!! example "Example"
    $$
    \begin{aligned}
    & \textbf{DataSet}: 10, 20, 25
    \newline
    \newline
    & \textbf{Mean}: \frac{3}{ \frac{1}{10} + \frac{1}{20} + \frac{1}{25} } = 15.79
    \end{aligned}
    $$

<!--prettier-ignore-->
!!! warning "Harmonic vs Arithmetic"
    Relation between $\textbf{H}$ and $\overline{\textbf{X}}$ is:

    $$
    \begin{aligned}
    & H = \left(\frac{ \frac{1}{x_1} + \frac{1}{x_2} + \dots + \frac{1}{x_n} }{n}\right)^{-1} = \left(\frac{ \sum\limits_{i=1}^n \frac{1}{x_i} }{n}\right)^{-1} = \left(\overline{\frac{1}{X}}\right)^{-1}
    \end{aligned}
    $$

---

## Median (**m**)

Measure of the central value of the sample set called **median** or $\textbf{m}$

-   For **Quantitative** and **Qualitative** values
-   **Sort** DataSet: $x_1, x_2, \dots, x_n$

$$
\begin{aligned}
& \textbf{m} =  \begin{cases}
                    x_{ \frac{(n+1)}{2} } & n = 2k + 1
                    \\
                    \frac{x_{ \frac{n}{2} } + x_{ \frac{n}{2} + 1 }}{2} & n = 2k
                \end{cases}
\end{aligned}
$$

**Example**:

```code
DataSet: 5, 23, 2, 1, 17
Sorted: 1, 2, 5, 17, 23

Median: 5
```

```code
DataSet: 5, 23, 2, 1, 17, 26
Sorted: 1, 2, 5, 17, 23, 26

Median: (5+17)/2 = 11
```

<!--prettier-ignore-->
!!! tip "Frequency Table Formula"
    We can find median using frequency table with this formula:

    $$
    \begin{aligned}
        & Median = L_1 + \frac{\frac{n}{2} - (\sum freq)l}{freq_{median}}.width
    \end{aligned}
    $$

---

## Quantile (**q**)

An specific **cut point** of the sample set called **quantile** or $\textbf{q}_p$ where **p** percent of values are **less or equals** of that point

-   For **Quantitative** and **Qualitative** values
-   **Sort** DataSet: $x_1, x_2, \dots, x_n$
-   $\textbf{q}_p = (n+1) * p$ is **Quantile Index**
-   Find $\textbf{X}(\textbf{q}_p)$ value using **Weighted Mean**

<!--prettier-ignore-->
!!! danger "Tip"
    When our data attribute is **Discrete** every two data values have some gap, the quantile value can be any point in this gap:

    ![Quantile Gap](assets/quantile_gap.png)

<!--prettier-ignore-->
!!! example "Example"
    Find these quantiles:

    1. **0.75**
    2. **63%**
    3. **3th decile**

    $$
    \begin{aligned}
    & \textbf{DataSet}: 6, 1, 5, 5, 2, 2, 5, 5, 25, 5, 5, 2, 2, 2, 24, 7, 3, 3, 6, 42, 51
    \\
    & \textbf{Sorted}: 1, 2, 2, 2, 2, 2, 3, 3, 5, 5, 5, 5, 5, 5, 6, 6, 7, 24, 25, 42, 51
    \\
    & \textbf{n}: 21
    \end{aligned}
    $$

    ---

    -   **0.75**

    $$
    \begin{aligned}
    & \textbf{p}_q = (21 + 1) * 0.75 = 16.5
    \\
    \\
    & X(16) = 6
    \\
    & X(17) = 7
    \\
    \\
    & X(16.5) = \frac{X(16) + X(17)}{2} = 6.5
    \end{aligned}
    $$

    ---

    -   **63%**

    $$
    \begin{aligned}
    & \textbf{p}_q = (21 + 1) * 0.63 = 13.86
    \\
    \\
    & X(13) = 5
    \\
    & X(14) = 5
    \\
    \\
    & X(13.86) = \frac{14*X(16) + 86*X(17)}{100} = 5
    \end{aligned}
    $$

    ---

    -   **3th decile**

    $$
    \begin{aligned}
    & \textbf{p}_q = (21 + 1) * 0.3 = 6.6
    \\
    \\
    & X(6) = 2
    \\
    & X(7) = 3
    \\
    \\
    & X(6.6) = \frac{4*X(6) + 6*X(7)}{10} = 2.6
    \end{aligned}
    $$

<!--prettier-ignore-->
!!! warning "Tip"
    These quantiles are equal:

    * **1th median**
    * **2th quartile**
    * **5th decile**
    * **50th percentile**

<!--prettier-ignore-->
!!! tip "5 Main Measures"
    1. Min
    2. q1
    3. q2
    4. q3
    5. Max

### Median

Cut points with base of **50%**

$$
\begin{aligned}
& 50\%
\end{aligned}
$$

### Quartile

Cut points with base of **25%**

$$
\begin{aligned}
& 25\%, 50\%, 75\%
\end{aligned}
$$

### Decile

Cut points with base of **10%**

$$
\begin{aligned}
& 10\%, 20\%, \dots, 80\%, 90\%
\end{aligned}
$$

### Precentile

Cut points with base of **1%**

$$
\begin{aligned}
& 1\%, 2\%, \dots, 98\%, 99\%
\end{aligned}
$$

---

## Mode (**M**)

The value most recurrent in the sample set

<!--prettier-ignore-->
!!! example "Example"
    $$
    \begin{aligned}
    & \textbf{DataSet}: 4, 12, 6, 2, 3, 4, 4, 1
    \newline
    \newline
    & \textbf{Mode}: 4 => 3-recurrent
    \end{aligned}
    $$

<!--prettier-ignore-->
!!! warning "Tip"
    Sometimes our dataset have **multiple modes**, when 2 or more values have the **equal** and **most** recurrent number, we call these datasets:

    1. **Unimodal**: One mode
    2. **Bimodal**: Two modes
    3. **Trimodal**: Three modes
    4. **Nomode**: Unique data (like ID)

<!--prettier-ignore-->
!!! tip "Empirical Formula"
    We have an empirical formula that establishes a connection between **Mean**, **Median**, **Mode**:

    $$
    \begin{aligned}
        & 3.|Mean - Median| = |Mean - Mode|
    \end{aligned}
    $$

    ![Empirical Formula](assets/mean-median-mode.png)

---

## Midrange

The mean of **max** and **min** values called midrange

<!--prettier-ignore-->
!!! example "Example"
    $$
    \begin{aligned}
    & \textbf{DataSet}: 4, 12, 6, 2, 3, 4, 4, 1
    \newline
    \newline
    & \textbf{Midrange}: \frac{12 + 1}{2} = 6.5
    \end{aligned}
    $$

---

## Five Number Summary

We call **Min**, **Q1**, **Q2**, **Q3**, **Max** of dataset **five number summary** and we can draw the box plot using these numbers

![BoxPlot](assets/boxplot4.bmp)

---
