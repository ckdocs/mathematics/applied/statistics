# Intro

We must deeply undestand **Trial**, **Sample Space**, **Random Variable**, **Probability**, **Probability Distribution**

---

## Trial (Application)

Trial or experiment, is a **definition** of a experiment

Is like an application we define: **Begin**, **End**

<!--prettier-ignore-->
!!! example "Example"
    1. Tossing two dices
    2. Tossing a dice until getting a 6

    ![Trial](assets/trial.jpg)

---

## Sample Space (Possible Objects)

Any possible outcomes of experiment

Is like recording application objects: **Record**

<!--prettier-ignore-->
!!! example "Example"
    1. Tossing two dices

    ![Sample Spacce](assets/sample_space.png)

---

## Random Variable (Function)

Any functions that maps objects into another objects or numbers

<!--prettier-ignore-->
!!! example "Example"
    1. Tossing two dices and getting sum of numbers
        * **Sample Space**: Any possible outcome of two dice object
        * **X**: Map **dice object** into **two number**
        * **Y**: Map **two number** into **sum of them**
    2. Getting a lamp age
        * **Sample Space**: Any possible outcome of lamp object
        * **X**: Map **lamp object** into **age of it**
    3. Flipping a coin until get a head and getting number of coins
        * **Sample Space**: Any number of coins that last coin is head
        * **X**: Map **coin object** into **Head|Tail state**
        * **Y**: Map **Head|Tail state** into **number of coins**

---

## Probability

We can define probability using **random variables** by dividing it on **sample space**

<!--prettier-ignore-->
!!! example "Example"
    1. Tossing two dices and getting sum of numbers
        * **Sample Space**: Any possible outcome of two dice object
        * **X**: Map **dice object** into **two number**
        * **Y**: Map **two number** into **sum of them**
        * **P(3)**: {12, 21} = $\frac{2}{36}$

    ---

    ![Random Variable](assets/probability.png)

---

## Probability Distribution

A distribution of **probabilities** of all possible **ranges** of a random variable

-   **X**: Range of random variable
-   **Y**: P(X)

<!--prettier-ignore-->
!!! tip "Categorize"
    We can categorize probability distributions into some categories based on their shapes:

    1. Bernouli
    2. Uniform
    3. Binomial
    4. Geometric
    3. etc

<!--prettier-ignore-->
!!! tip "Function"
    We define functions in mathematics

    ![Function](assets/function.png)

<!--prettier-ignore-->
!!! example "Example"
    1. Tossing two dices and getting sum of numbers
        * **Sample Space**: Any possible outcome of two dice object
        * **X**: Map **dice object** into **two number**
        * **Y**: Map **two number** into **sum of them**
        * **Y Probability Distribution**

    ---

    ![Probability Distribution](assets/probability_distribution.png)

---

## Random Variable (X)

A numerical quantity that takes values based on outcomes

Is a function from **Sample Space** into **Real Numbers**

$$
\begin{aligned}
    & X :: \Omega \rightarrow R
\end{aligned}
$$

<!--prettier-ignore-->
!!! example "Example"
    * **Trial**: Selecting a random person from set bellow
    * **Sample Space**: {a, b, c, d}
    * **W**: Weight of person
    * **H**: Height of person
    * **B**: Body mass = $B = \frac{W}{H^2}$

    ![Random Variable Example](assets/random_variable_example.png)

<!--prettier-ignore-->
!!! tip "Tip"
    * **Random Variable**: $X$
    * **Numerical Value**: $x$

    $$
    \begin{aligned}
        & W: \texttt{Weight of person}
        \\
        \\
        & w_1: 178
        \\
        & w_2: 175
        \\
        & w_3: 160
        \\
        & \dots
    \end{aligned}
    $$

<!--prettier-ignore-->
!!! tip "Tip"
    Random variables are functions, so we can combine them together and create new random variables

    $$
    \begin{aligned}
        & Z = X + Y
    \end{aligned}
    $$

---

### Types

There are two types of random variables based on their values, **discrete** and **continous**

1. Discrete (Countable)
    1. Finine
    2. Infinite
2. Continous

---

#### Discrete

Values of random variable can be any **finite** or **infinite** discrete random variables

<!--prettier-ignore-->
!!! example "Example"
    1. Dice number: **Discrete - Finite**: `{1,2,3,4,5,6}`
    2. Coin flip count to get head: **Discrete - Infinite**: `{1,2,...}`

---

#### Continous

Values of random variable can be any continous random variables

<!--prettier-ignore-->
!!! example "Example"
    1. Lamp age: **Continous**: `(0,inf)`
    2. Person height: **Continous**: `(100,200)`

---

### Inverse

Random variable is a function from $\Omega$ to $R$, we can find it's inverse from $R$ to $[\Omega]$

$$
\begin{aligned}
    & X^{-1} :: R \rightarrow \{\Omega\}
\end{aligned}
$$

We can describe inverse using **Operations** like `=`, `>`, `<`, `>=`, `<=`

$$
\begin{aligned}
    & X = x:: \{w : X(w) = x\}
    \\
    & X > x:: \{w : X(w) > x\}
    \\
    & X < x:: \{w : X(w) < x\}
    \\
    & X \geq x:: \{w : X(w) \geq x\}
    \\
    & X \leq x:: \{w : X(w) \leq x\}
\end{aligned}
$$

<!--prettier-ignore-->
!!! example "Example"
    1. Tossing two dices and getting sum of numbers
        * **Sample Space**: Any possible outcome of two dice object
        * **X**: Map **dice object** into **two number**
        * **Y**: Map **two number** into **sum of them**

    $$
    \begin{aligned}
        & (Y = 4): \{13,31,22\}
        \\
        & (Y = 5): \{14,41,23,32\}
    \end{aligned}
    $$

---
