# Statistics

Statistics is an area of applied mathematics concerned with the data **collection**, **presentation**, **analysis**, **interpretation**

1. **Collection**: `Questionnaire`, `Crawler`, `Logs`
2. **Presentation**: `Table`, `Chart`
3. **Analysis**: `Mean`, `Sum`, `Varians`
4. **Interpretation**: `Result`

**Example**:

1. You launch an application, you want to get feedback from your users: `Collection`
2. Your boss want a visualized version of feedbacks: `Presentation, Analysis`

---

## Statistics Terminologies

![Statistics Terminologies](assets/statistics_terminologies.png)

---

### Population

A collection or set of individuals or objects or events whos properties are to be analyzed

<!--prettier-ignore-->
!!! tip "Population types"
    There are two main types of populations:

    1. **Infinite**
    2. **Finine**

---

### Sample

A subset of population called `Sample`

<!--prettier-ignore-->
!!! tip "Good Sample"
    A **well choosen sample** will contain `most` of the `information` about a particular population parameter

---

### Sampling

Population is too big to collect data from, so we select a subset of population and collecting data from it

Sampling is the most important part of statistics, because the selected sample must include entire attributes of entire population.

![Sampling Methods](assets/sampling_methods.png)

---

#### Probability Sampling

This type of sampling is based on `equal` or `non equal` **chance** probability of every select people of population

---

##### Random Sampling

Randomly select sample (equal chance for selecting every one) (1,4,6,52,100,...)

![Random Sampling](assets/random_sampling.png)

---

##### Systematic Sampling

Selecting with a static formula (2n, 2n+4, 2n^2, ...)

![Systematic Sampling](assets/systematic_sampling.png)

---

##### Stratified Sampling

Categorize then random select (males, females)

![Stratified Sampling](assets/stratified_sampling.png)

---

## Statistics Types

![Descriptive VS Inferential](assets/descriptive_vs_inferential.jpg)

### Descriptive Statistics

Descriptive statistics uses the data to provide `descriptions` of the `population`, either through numerical **calculations** or **graphs** or **tables**

<!--prettier-ignore-->
!!! tip "Descriptive Statistics"
    Descriptive Statistics is mainly focused upon the main characteristics of data. It provides `graphical` summary of the data.

> Descriptive Statistics is a method which is used to describe an specific feature of specific dataset by giving a short summary of data

**Example**:

You want to buy shirts for a class, you get size of all students, find `Minimum`, `Maximum`, `Average` size, then buy some shirts

![Descriptive Example](assets/descriptive_example.png)

---

### Inferential Statistics

Inferential statistics makes `inferences` and `predictions` about a population based on a **sample** of data taken from the **population** in question

<!--prettier-ignore-->
!!! tip "Inferential Statistics"
    Inferential Statistics, generalizes a large dataset and applies `probability` to draw a conclusion. It allows us to infer `data parameters` based on a statistical model using a sample data.

**Example**:

Get sample people from class room, then expand your model from the sample to class and categorize them into the groups: `Large`, `Medium`, `Small`

![Inferential Example](assets/inferential_example.png)

---
